<?php
return array(

	'_404_'   => 'welcome/404',    // The main 404 route
	'_root_'  => 'pages/frontend/home',  // The default route

    // Installation
    'backend/installation' => 'backend/install/setup',

    // Backend routes
    'backend' => 'backend/dashboard', // The default route for backend
    'backend/sign-in' => 'backend/dashboard/sign_in',
    'backend/sign-out' => 'backend/dashboard/sign_out',
    'backend/change-password' => 'backend/dashboard/change_current_password',
    'backend/my-profile' => 'backend/dashboard/my_profile_form',
    'backend/no-permission' => 'error/no_permission/backend',

    // Admin Management //
    // Admin User
    'backend/admin-user' => 'adminmanagement/adminuser',
    'backend/admin-user/add' => 'adminmanagement/adminuser/form/0',
    'backend/admin-user/edit/(:num)' => 'adminmanagement/adminuser/form/$1',
    'backend/admin-user/delete/(:num)' => 'adminmanagement/adminuser/delete/$1',
    'backend/admin-user/reset-password/(:num)' => 'adminmanagement/adminuser/reset_password/$1',
    
    // Admin Role Permission
    'backend/admin-role-permission' => 'adminmanagement/adminrolepermission',
    'backend/admin-role-permission/add' => 'adminmanagement/adminrolepermission/form/0',
    'backend/admin-role-permission/edit/(:num)' => 'adminmanagement/adminrolepermission/form/$1',
    'backend/admin-role-permission/delete/(:num)' => 'adminmanagement/adminrolepermission/delete/$1',
    'backend/admin-role-permission/assign-admin/(:num)' => 'adminmanagement/adminrolepermission/assign_admin/$1',
    'backend/admin-role-permission/do-assign-admin/(:num)/(:num)' => 'adminmanagement/adminrolepermission/do_assign_admin/$1/$2/1',
    'backend/admin-role-permission/do-unassign-admin/(:num)/(:num)' => 'adminmanagement/adminrolepermission/do_assign_admin/$1/$2/0',
    'backend/admin-role-permission/set-permission/(:num)' => 'adminmanagement/adminrolepermission/set_permission/$1',
    
    // Basic Setting
    'backend/setting' => 'adminmanagement/setting',

    // Facebook Management //
    'backend/fb-participant' => 'facebookmanagement/participant',
    'backend/fb-setting' => 'facebookmanagement/setting',
    
    
    
    // Banner Management //
    'backend/banner-management' => 'bannermanagement/backend/banner',
    'backend/banner-management/add' => 'bannermanagement/backend/banner/form/0',
    'backend/banner-management/edit/(:num)' => 'bannermanagement/backend/banner/form/$1',
    'backend/banner-management/delete/(:num)' => 'bannermanagement/backend/banner/delete/$1',
    
    
    
    // CSR Management //
    // List
    'backend/csr-management' => 'csrmanagement/backend',
    'backend/csr-management/add' => 'csrmanagement/backend/form/0',
    'backend/csr-management/edit/(:num)' => 'csrmanagement/backend/form/$1',
    'backend/csr-management/delete/(:num)' => 'csrmanagement/backend/delete/$1',
    
    // Image
    'backend/csr-management/image' => 'csrmanagement/backend/image',
    'backend/csr-management/image/add' => 'csrmanagement/backend/image/form/0',
    'backend/csr-management/image/edit/(:num)' => 'csrmanagement/backend/image/form/$1',
    'backend/csr-management/image/delete/(:num)' => 'csrmanagement/backend/image/delete/$1',
    
    
    // Career Management //
    // List
    'backend/career-management' => 'careermanagement/backend',
    'backend/career-management/add' => 'careermanagement/backend/career/form/0',
    'backend/career-management/edit/(:num)' => 'careermanagement/backend/career/form/$1',
    'backend/career-management/delete/(:num)' => 'careermanagement/backend/career/delete/$1',
    
    // General Req
    'backend/career-management/general' => 'careermanagement/backend/general',
    'backend/career-management/general/add' => 'careermanagement/backend/general/form/0',
    'backend/career-management/general/edit/(:num)' => 'careermanagement/backend/general/form/$1',
    'backend/career-management/general/delete/(:num)' => 'careermanagement/backend/general/delete/$1',
    
    // Specific Req
    'backend/career-management/specific' => 'careermanagement/backend/specific',
    'backend/career-management/specific/add' => 'careermanagement/backend/specific/form/0',
    'backend/career-management/specific/edit/(:num)' => 'careermanagement/backend/specific/form/$1',
    'backend/career-management/specific/delete/(:num)' => 'careermanagement/backend/specific/delete/$1',
      
    // Vacancy
    'backend/career-management/vacancy' => 'careermanagement/backend/vacancy',
    'backend/career-management/vacancy/add' => 'careermanagement/backend/vacancy/form/0',
    'backend/career-management/vacancy/edit/(:num)' => 'careermanagement/backend/vacancy/form/$1',
    'backend/career-management/vacancy/delete/(:num)' => 'careermanagement/backend/vacancy/delete/$1',
    
    
    // News Management //
    // List
    'backend/news' => 'news/backend',
    'backend/news/add' => 'news/backend/form/0',
    'backend/news/edit/(:num)' => 'news/backend/form/$1',
    'backend/news/delete/(:num)' => 'news/backend/delete/$1',
    
    // Image
    'backend/news/image' => 'news/backend/image',
    'backend/news/image/add' => 'news/backend/image/form/0',
    'backend/news/image/edit/(:num)' => 'news/backend/image/form/$1',
    'backend/news/image/delete/(:num)' => 'news/backend/image/delete/$1',
    
    // Doc
    'backend/news/doc' => 'news/backend/doc',
    'backend/news/doc/add' => 'news/backend/doc/form/0',
    'backend/news/doc/edit/(:num)' => 'news/backend/doc/form/$1',
    'backend/news/doc/delete/(:num)' => 'news/backend/doc/delete/$1',
    
    // Doc ID
    'backend/news/docid' => 'news/backend/docid',
    'backend/news/docid/add' => 'news/backend/docid/form/0',
    'backend/news/docid/edit/(:num)' => 'news/backend/docid/form/$1',
    'backend/news/docid/delete/(:num)' => 'news/backend/docid/delete/$1', 

    // Pages //
    'backend/pages' => 'pages/backend',
    'backend/pages/add' => 'pages/backend/form/0',
    'backend/pages/edit/(:num)' => 'pages/backend/form/$1',
    'backend/pages/delete/(:num)' => 'pages/backend/delete/$1',
    
    // Project //
    // List
    'backend/project' => 'project/backend',
    'backend/project/add' => 'project/backend/form/0',
    'backend/project/edit/(:num)' => 'project/backend/form/$1',
    'backend/project/delete/(:num)' => 'project/backend/delete/$1',
    
    // Image
    'backend/project/image' => 'project/backend/image',
    'backend/project/image/add' => 'project/backend/image/form/0',
    'backend/project/image/edit/(:num)' => 'project/backend/image/form/$1',
    'backend/project/image/delete/(:num)' => 'project/backend/image/delete/$1',
    
    // Office
    'backend/project/office' => 'project/backend/office',
    'backend/project/office/add' => 'project/backend/office/form/0',
    'backend/project/office/edit/(:num)' => 'project/backend/office/form/$1',
    'backend/project/office/delete/(:num)' => 'project/backend/office/delete/$1',
      
    // City
    'backend/project/city' => 'project/backend/city',
    'backend/project/city/add' => 'project/backend/city/form/0',
    'backend/project/city/edit/(:num)' => 'project/backend/city/form/$1',
    'backend/project/city/delete/(:num)' => 'project/backend/city/delete/$1',
    
    
    // Media Uploader //
    // Banner 
    'backend/media-uploader/banner' => 'mediauploader/banner',
    'backend/media-uploader/banner/upload' => 'mediauploader/banner/upload',
    
    // Slide
    'backend/media-uploader/slide' => 'mediauploader/slide',
    'backend/media-uploader/slide/upload' => 'mediauploader/slide/upload',
    
    // Project Image 
    'backend/media-uploader/projectimage' => 'mediauploader/projectimage',
    'backend/media-uploader/projectimage/upload' => 'mediauploader/projectimage/upload',
    
    // News Image 
    'backend/media-uploader/newsimage' => 'mediauploader/newsimage',
    'backend/media-uploader/newsimage/upload' => 'mediauploader/newsimage/upload',
    
    // News Doc 
    'backend/media-uploader/newsdoc' => 'mediauploader/newsdoc',
    'backend/media-uploader/newsdoc/upload' => 'mediauploader/newsdoc/upload',
    
    // News Doc Id
    'backend/media-uploader/newsdocid' => 'mediauploader/newsdocid',
    'backend/media-uploader/newsdocid/upload' => 'mediauploader/newsdocid/upload',
    
    // CSR Image 
    'backend/media-uploader/csrimage' => 'mediauploader/csrimage',
    'backend/media-uploader/csrimage/upload' => 'mediauploader/csrimage/upload',
    
    // Footer Image 
    'backend/media-uploader/footerimage' => 'mediauploader/footerimage',
    'backend/media-uploader/footerimage/upload' => 'mediauploader/footerimage/upload',
    
    
    // Slide Management //
    'backend/slide-management' => 'slidemanagement/backend/slide',
    'backend/slide-management/add' => 'slidemanagement/backend/slide/form/0',
    'backend/slide-management/edit/(:num)' => 'slidemanagement/backend/slide/form/$1',
    'backend/slide-management/delete/(:num)' => 'slidemanagement/backend/slide/delete/$1',
	
    // Footer Banner Management //
    'backend/footer-management' => 'footermanagement/backend',
    'backend/footer-management/add' => 'footermanagement/backend/form/0',
    'backend/footer-management/edit/(:num)' => 'footermanagement/backend/form/$1',
    'backend/footer-management/delete/(:num)' => 'footermanagement/backend/delete/$1',
	
	
	//frontend
	':lang_code/home'                           => 'pages/frontend/home',
	':lang_code/philosophy'                     => 'pages/frontend/philosophy',
	':lang_code/project'                        => 'project/frontend/home',
	':lang_code/about-group'                    => 'pages/frontend/about/group',
	':lang_code/about-us/president'             => 'pages/frontend/about/president',
	':lang_code/about-us/history'               => 'pages/frontend/about/history',
	':lang_code/about-us/business'              => 'pages/frontend/about/business',
	':lang_code/about-us/company'               => 'pages/frontend/about/company',
	':lang_code/csr'                            => 'csrmanagement/frontend/home',
	':lang_code/career'                         => 'careermanagement/frontend/home',
	':lang_code/career/submit/(:vacancy_id)'    => 'careermanagement/frontend/home/submit',
	':lang_code/career/(:vacancy_id)/thanks'    => 'careermanagement/frontend/home/thank',
    ':lang_code/news'                           => 'news/frontend/home',
	':lang_code/news/archive/:selected_year'    => 'news/frontend/home',
	':lang_code/contact-us'                     => 'pages/frontend/contact',
    ':lang_code/contact-us/thanks'              => 'pages/frontend/contact/thank',
	'get-submit-contact'                        => 'pages/api/submitcontact/getsubmitcontact',
    ':lang_code/privacy'                        => 'pages/frontend/home/privacy',
    ':lang_code/terms'                          => 'pages/frontend/home/terms',
);