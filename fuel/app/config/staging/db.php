<?php
/**
 * The staging database settings. These get merged with the global settings.
 */

// Check Global Config
$_development_db_global_config_filepath = '';
$_db_host_global = '127.0.0.1';
$_db_name_global = 'microad_tokyuland';
// Check if filepath is stored on server variable
if (isset($_SERVER['FUELDB_GLOBAL'])) {
	$_development_db_global_config_filepath = $_SERVER['FUELDB_GLOBAL'];
}

// Check if there is a global config
if (is_file($_development_db_global_config_filepath)) {
	return require_once $_development_db_global_config_filepath;
} else {
	return array(
		'default' => array(
			'connection'  => array(
				'dsn'        => 'mysql:host=127.0.0.1;dbname=fuel_staging',
				'username'   => 'fuel_app',//'microad_dev',//
				'password'   => 'super_secret_password',//'umqt88n2',//
			),
		),
	);
}
