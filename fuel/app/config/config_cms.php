<?php

return array(
	'cms_name' => 'TokyuLand Indonesia',
	'max_lock_count' => 5,
	'cms_session_name' => array(
		'admin_id' => 'TokyuLandIndonesia'
	),
	'admin_default_password' => 'tokyuland',
	'max_email_sent_daily' => 500,
	'max_email_sent_per_task' => 10,
	
	'menus' => array(
		'dashboard' => array(
			'label' => 'Dashboard',
			'route' => 'backend',
			'icon_class' => 'fa fa-dashboard',
			'permission' => false,
		),
            'admin_management' => array(
			'label' => 'Admin Management',
			'icon_class' => 'fa fa-columns',
			'permission' => false,
			'submenus' => array(
				'admin_user' => array(
					'label' => 'Admin User',
					'route' => 'backend/admin-user',
					'icon_class' => 'fa fa-users',
					'permission' => true,
				),
				'admin_role_permission' => array(
					'label' => 'Admin Role Permission',
					'route' => 'backend/admin-role-permission',
					'icon_class' => 'fa fa-shield',
					'permission' => true,
				),
                'admin_setting' => array(
                    'label' => 'Basic Setting',
                    'route' => 'backend/setting',
                    'icon_class' => 'fa fa-gear',
                    'permission' => true,
                ),
                'admin_role_permission' => array(
                    'label' => 'Admin Role Permission',
                    'route' => 'backend/admin-role-permission',
                    'icon_class' => 'fa fa-shield',
                    'permission' => true,
                )
			)
		),
                'banner_management' => array(
			'label' => 'Banner Management',
			'icon_class' => 'fa fa-pagelines',
			'permission' => false,
			'route' => 'backend/banner-management'
		),
                'csr_management' => array(
                    'label' => 'CSR Management',
                    'icon_class' => 'fa fa-street-view',
                    'permission' => false,
                    'submenus' => array(
                        'csr_organizer' => array(
                            'label' => 'Organize',
                            'route' => 'backend/csr-management/',
                            'icon_class' => 'fa fa-puzzle-piece',
                            'permission' => true,
                        ),
                    )     
                ),
                'career_management' => array(
                    'label' => 'Career Management',
                    'icon_class' => 'fa fa-briefcase',
                    'permission' => false,
                    'submenus' => array(
                    'career_vacancy' => array(
                            'label' => 'Vacancy',
                            'route' => 'backend/career-management/vacancy',
                            'icon_class' => 'fa fa-user-plus',
                            'permission' => true,
                        ),
                    )     
                ),
                'media_uploader' => array(
			'label' => 'Media Uploader',
			'icon_class' => 'fa fa-upload',
			'permission' => false,
			'submenus' => array(
				'media_uploader_banner' => array(
                                'label' => 'Home Banner',
                                'route' => 'backend/media-uploader/banner',
                                'icon_class' => 'fa fa-upload',
                                'permission' => true,
                            ),
                                'media_uploader_slide' => array(
                                'label' => 'Home Slide',
                                'route' => 'backend/media-uploader/slide',
                                'icon_class' => 'fa fa-upload',
                                'permission' => true,
                            ),
                                'media_uploader_projectimage' => array(
                                'label' => 'Project Image',
                                'route' => 'backend/media-uploader/projectimage',
                                'icon_class' => 'fa fa-upload',
                                'permission' => true,
                            ),
                                'media_uploader_newsimage' => array(
                                'label' => 'News Image',
                                'route' => 'backend/media-uploader/newsimage',
                                'icon_class' => 'fa fa-upload',
                                'permission' => true,
                            ),
                                'media_uploader_newsdoc' => array(
                                'label' => 'News Document',
                                'route' => 'backend/media-uploader/newsdoc',
                                'icon_class' => 'fa fa-upload',
                                'permission' => true,
                            ),
                                'media_uploader_newsdoc_id' => array(
                                'label' => 'News Document (id)',
                                'route' => 'backend/media-uploader/newsdocid',
                                'icon_class' => 'fa fa-upload',
                                'permission' => true,
                            ),
                                'media_uploader_footerimage' => array(
                                'label' => 'Footer Image',
                                'route' => 'backend/media-uploader/footerimage',
                                'icon_class' => 'fa fa-upload',
                                'permission' => true,
                            ),
			)
		),
		'pages' => array(
			'label' => 'Pages',
			'route' => 'backend/pages',
			'icon_class' => 'fa fa-sitemap',
			'permission' => true,
		),
		'project' => array(
                    'label' => 'Project Management',
                    'icon_class' => 'fa fa-cubes',
                    'permission' => false,
                    'submenus' => array(
                        'project_organizer' => array(
                            'label' => 'Organize',
                            'route' => 'backend/project',
                            'icon_class' => 'fa fa-cube',
                            'permission' => true,
                        ),
                        'project_image' => array(
                            'label' => 'Image',
                            'route' => 'backend/project/image',
                            'icon_class' => 'fa fa-camera-retro',
                            'permission' => true,
                         ),
                        'project_office' => array(
                            'label' => 'Office',
                            'route' => 'backend/project/office',
                            'icon_class' => 'fa fa-building',
                            'permission' => true,
                         ),  
                        'project_city' => array(
                            'label' => 'City',
                            'route' => 'backend/project/city',
                            'icon_class' => 'fa fa-map-marker',
                            'permission' => true,
                        ),
                    )     
                ),
                'news' => array(
                    'label' => 'News Management',
                    'icon_class' => 'fa fa-newspaper-o',
                    'permission' => false,
                    'submenus' => array(
                        'news_organizer' => array(
                            'label' => 'Organize',
                            'route' => 'backend/news/',
                            'icon_class' => 'fa fa-puzzle-piece',
                            'permission' => true,
                        ),
                        'news_image' => array(
                            'label' => 'Image',
                            'route' => 'backend/news/image',
                            'icon_class' => 'fa fa-camera-retro',
                            'permission' => true,
                        ),
                        'news_doc' => array(
                            'label' => 'Doc',
                            'route' => 'backend/news/doc',
                            'icon_class' => 'fa fa-file',
                            'permission' => true,
                        ),
                        'news_docid' => array(
                            'label' => 'Doc-id',
                            'route' => 'backend/news/docid',
                            'icon_class' => 'fa fa-file',
                            'permission' => true,
                        )
                    )     
                ),
                'slide_management' => array(
			'label' => 'Slide Management',
			'icon_class' => 'fa fa-image',
			'permission' => false,
			'route' => 'backend/slide-management'
		),
                'footer_management' => array(
			'label' => 'Footer Banner Management',
			'icon_class' => 'fa fa-image',
			'permission' => false,
			'route' => 'backend/footer-management'
		),
		
	),
);
