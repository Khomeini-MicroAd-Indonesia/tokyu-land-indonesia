<?php

/* group.twig */
class __TwigTemplate_e051df2dea0378bdb089e2bbf7ca7fa93bb22d0c77405d2c8b3884a4007b49d1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("pages/views/template_frontend.twig");

        $this->blocks = array(
            'frontend_content' => array($this, 'block_frontend_content'),
            'frontend_js' => array($this, 'block_frontend_js'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "pages/views/template_frontend.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_frontend_content($context, array $blocks = array())
    {
        // line 4
        echo "    <div class=\"tokyuland-group\" id=\"top-group\">
        <div class=\"row\">
            <ul class=\"breadcrumbs\">
                <li><a href=\"";
        // line 7
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/home\">Home</a></li>
                <li class=\"current\">About Group</li>
            </ul>
        </div>
        <div class=\"row\">
            <h1>ABOUT GROUP</h1>
            <div>
                <ul class=\"button-group\">
                    <li><a id=\"fodusanClick\" class=\"button\">Tokyu Fudosan Holding</a></li>
                    <li><a id=\"landClick\" class=\"button\">Tokyu Land Corporation</a></li>
                    <li><a id=\"groupClick\" class=\"button second\">Tokyu Group Companies</a></li>
                </ul>
            </div>
        </div>
        <div class=\"row\">
            <div id=\"fodusan\" class=\"group-box\">
                <div>
                    <img src=\"";
        // line 24
        echo Uri::base();
        echo "/assets/css/images/fodusan-new.png\"/>
                </div>
                <div style=\"height: 20px;\"></div>
                <div class=\"holding\">
                    <h2>Tokyu Fudosan Holding's Data</h2>
                    <p>
                        Tokyu Fudosan Holdings—Continuing to create new value for living, business, and culture in Japan
                    </p>
                </div>
                <div class=\"holding-data\" data-equalizer>
                    <div class=\"small-4 columns\" data-equalizer-watch>
                        <div class=\"img-holding\">
                            <img src=\"";
        // line 36
        echo Uri::base();
        echo "/assets/css/images/100.jpg\"/>
                        </div>
                        <div class=\"text-holding\">
                            ";
        // line 39
        echo Lang::get("fodusan-100");
        echo "
                        </div>
                    </div>
                    <div class=\"small-4 columns\" data-equalizer-watch>
                        <div class=\"img-holding\">
                            <img src=\"";
        // line 44
        echo Uri::base();
        echo "/assets/css/images/17000.jpg\"/>
                        </div>
                        <div class=\"text-holding\">
                            ";
        // line 47
        echo Lang::get("fodusan-17000");
        echo "
                        </div>
                    </div>
                    <div class=\"small-4 columns\" data-equalizer-watch>
                        <div class=\"img-holding\">
                            <img src=\"";
        // line 52
        echo Uri::base();
        echo "/assets/css/images/2500.jpg\"/>
                        </div>
                        <div class=\"text-holding\">
                            ";
        // line 55
        echo Lang::get("fodusan-2500");
        echo "
                        </div>
                    </div>
                    <div style=\"clear: both\"></div>
                </div>
                <div class=\"holding-data\" data-equalizer>
                    <div class=\"small-4 columns\" data-equalizer-watch>
                        <div class=\"img-holding\">
                            <img src=\"";
        // line 63
        echo Uri::base();
        echo "/assets/css/images/60.jpg\"/>
                        </div>
                        <div class=\"text-holding\">
                            ";
        // line 66
        echo Lang::get("fodusan-60");
        echo "
                        </div>
                    </div>
                    <div class=\"small-4 columns\" data-equalizer-watch>
                        <div class=\"img-holding\">
                            <img src=\"";
        // line 71
        echo Uri::base();
        echo "/assets/css/images/669000.jpg\"/>
                        </div>
                        <div class=\"text-holding\">
                            ";
        // line 74
        echo Lang::get("fodusan-669");
        echo "
                        </div>
                    </div>
                    <div class=\"small-4 columns\" data-equalizer-watch>
                        <div class=\"img-holding\">
                            <img src=\"";
        // line 79
        echo Uri::base();
        echo "/assets/css/images/6billion.jpg\"/>
                        </div>
                        <div class=\"text-holding\">
                            ";
        // line 82
        echo Lang::get("fodusan-6");
        echo "
                        </div>
                    </div>
                    <div style=\"clear: both\"></div>
                </div>
                <div class=\"row\">
                    <div class=\"link-group\">
                        <a href=\"http://www.tokyu-fudosan-hd.co.jp/english/\" target=\"_blank\"><strong>Visit  TOKYU FUDOSAN HOLDINGS Web site</strong></a>
                    </div>
                </div>
            </div>
            <div style=\"height: 50px;\"></div>
            <div id=\"land\" class=\"group-box\">
                <div class=\"tokyuland-land\">
                    <div class=\"img-land\">
                        <img src=\"";
        // line 97
        echo Uri::base();
        echo "/assets/css/images/title-land.jpg\"/>
                    </div>
                    <div style=\"height: 20px;\"></div>
                    <div class=\"sub-land\">
                        <p>Creating urban landscapes, supporting lifestyles, and opening doors to the future. Tokyu Fudosan Holdings’ business ties in to all aspects of life. </p>
                    </div>
                    <div class=\"land-data\" data-equalizer>
                        <div class=\"small-4 columns\" data-equalizer-watch>
                            <div class=\"wrap-land-corp\">
                                <div class=\"title-land-corp\">
                                    <p class=\"big-one\">C</p><p class=\"small-one\">omfortable offices that</p>
                                    <p class=\"small-one\">speed up business development</p>
                                </div>
                                <div class=\"img-land-corp\">
                                    <img src=\"";
        // line 111
        echo Uri::base();
        echo "/assets/css/images/comfortable.jpg\"/>
                                </div>
                                <div class=\"content-land-corp\">
                                    ";
        // line 114
        echo Lang::get("land-comfort");
        echo "
                                </div>
                            </div>

                        </div>
                        <div class=\"small-4 columns\" data-equalizer-watch>
                            <div class=\"wrap-land-corp mid-wrap-land\">
                                <div class=\"title-land-corp\">
                                    <p class=\"big-one\">P</p><p class=\"small-one\">roviding support for secure and healthy senior living</p>
                                </div>
                                <div class=\"img-land-corp\">
                                    <img src=\"";
        // line 125
        echo Uri::base();
        echo "/assets/css/images/providing.jpg\"/>
                                </div>
                                <div class=\"content-land-corp\">
                                    ";
        // line 128
        echo Lang::get("land-prov");
        echo "
                                </div>
                            </div>
                        </div>
                        <div class=\"small-4 columns\" data-equalizer-watch>
                            <div class=\"wrap-land-corp last-wrap-land\">
                                <div class=\"title-land-corp\">
                                    <p class=\"big-one\">R</p><p class=\"small-one\">esorts that heal people, and are kind to the environment</p>
                                </div>
                                <div class=\"img-land-corp\">
                                    <img src=\"";
        // line 138
        echo Uri::base();
        echo "/assets/css/images/resorts.jpg\"/>
                                </div>
                                <div class=\"content-land-corp\">
                                    ";
        // line 141
        echo Lang::get("land-resorts");
        echo "
                                </div>
                            </div>
                        </div>
                        <div style=\"clear: both;\"></div>
                    </div>
                    <div class=\"land-data\" data-equalizer>
                        <div class=\"small-4 columns\" data-equalizer-watch>
                            <div class=\"wrap-land-corp\">
                                <div class=\"title-land-corp\">
                                    <p class=\"big-one\">B</p><p class=\"small-one\">oldly proposing a new style of working</p>
                                </div>
                                <div class=\"img-land-corp\">
                                    <img src=\"";
        // line 154
        echo Uri::base();
        echo "/assets/css/images/boldly.jpg\"/>
                                </div>
                                <div class=\"content-land-corp\">
                                    ";
        // line 157
        echo Lang::get("land-bold");
        echo "
                                </div>
                            </div>

                        </div>
                        <div class=\"small-4 columns\" data-equalizer-watch>
                            <div class=\"wrap-land-corp mid-wrap-land\">
                                <div class=\"title-land-corp\">
                                    <p class=\"big-one\">C</p><p class=\"small-one\">ommercial facilities that create new value for the city</p>
                                </div>
                                <div class=\"img-land-corp\">
                                    <img src=\"";
        // line 168
        echo Uri::base();
        echo "/assets/css/images/commercial.jpg\"/>
                                </div>
                                <div class=\"content-land-corp\">
                                    ";
        // line 171
        echo Lang::get("land-commercial");
        echo "
                                </div>
                            </div>
                        </div>
                        <div class=\"small-4 columns\" data-equalizer-watch>
                            <div class=\"wrap-land-corp last-wrap-land\">
                                <div class=\"title-land-corp\">
                                    <p class=\"big-one\">H</p><p class=\"small-one\">omes that fulfill dreams and desires</p>
                                </div>
                                <div class=\"img-land-corp\">
                                    <img src=\"";
        // line 181
        echo Uri::base();
        echo "/assets/css/images/homes.jpg\"/>
                                </div>
                                <div class=\"content-land-corp\">
                                    ";
        // line 184
        echo Lang::get("land-homes");
        echo "
                                </div>
                            </div>
                        </div>
                        <div style=\"clear: both;\"></div>
                    </div>
                    <div class=\"land-data\" data-equalizer>
                        <div class=\"small-4 columns\" data-equalizer-watch>
                            <div class=\"wrap-land-corp\">
                                <div class=\"title-land-corp\">
                                    <p class=\"big-one\">O</p><p class=\"small-one\" style=\"margin-top:10px; \">verseas projects</p>
                                </div>
                                <div class=\"img-land-corp\">
                                    <img src=\"";
        // line 197
        echo Uri::base();
        echo "/assets/css/images/overseas.jpg\"/>
                                </div>
                                <div class=\"content-land-corp\">
                                    ";
        // line 200
        echo Lang::get("land-overseas");
        echo "
                                </div>
                            </div>
                        </div>
                        <div style=\"clear: both;\"></div>
                </div>
            </div>
            <div class=\"row\">
                <div class=\"link-group\">
                    <a href=\"http://www.tokyu-land.co.jp/english/\" target=\"_blank\"><strong>Visit  TOKYU LAND CORPORATION Web site</strong></a>
                </div>
            </div>
        </div>
        <div style=\"height: 50px;\"></div>
        <div id=\"group\" class=\"group-box\">
            <div class=\"tokyuland-land\">
                <h2>Tokyu Group Companies</h2>
                <div class=\"sub-land\">
                    <p>Creating urban landscapes, supporting lifestyles, and opening doors to the future. Tokyu Fudosan Holdings’ business ties in to all aspects of life. </p>
                </div>
                <div class=\"land-data\" data-equalizer>
                    <div class=\"small-4 columns\" data-equalizer-watch>
                        <div class=\"wrap-land-corp\">
                            <div class=\"title-group-comp\">
                                <img src=\"";
        // line 224
        echo Uri::base();
        echo "/assets/css/images/hands.png\"/>
                            </div>
                            <div class=\"img-land-corp\">
                                <img src=\"";
        // line 227
        echo Uri::base();
        echo "/assets/css/images/hands-img.jpg\"/>
                            </div>
                            <div class=\"content-land-corp\">
                                ";
        // line 230
        echo Lang::get("group-hands");
        echo "
                            </div>
                        </div>

                    </div>
                    <div class=\"small-4 columns\" data-equalizer-watch>
                        <div class=\"wrap-land-corp\">
                            <div class=\"title-group-comp\">
                                <img src=\"";
        // line 238
        echo Uri::base();
        echo "/assets/css/images/livable.png\"/>
                            </div>
                            <div class=\"img-land-corp\">
                                <img src=\"";
        // line 241
        echo Uri::base();
        echo "/assets/css/images/livable-img.jpg\"/>
                            </div>
                            <div class=\"content-land-corp\">
                                ";
        // line 244
        echo Lang::get("group-livable");
        echo "
                            </div>
                        </div>
                    </div>
                    <div class=\"small-4 columns\" data-equalizer-watch>
                        <div class=\"wrap-land-corp\">
                            <div class=\"title-group-comp\">
                                <img src=\"";
        // line 251
        echo Uri::base();
        echo "/assets/css/images/community.png\"/>
                            </div>
                            <div class=\"img-land-corp\">
                                <img src=\"";
        // line 254
        echo Uri::base();
        echo "/assets/css/images/comm-img.jpg\"/>
                            </div>
                            <div class=\"content-land-corp\">
                                ";
        // line 257
        echo Lang::get("group-comm");
        echo "
                            </div>
                        </div>
                    </div>
                    <div style=\"clear: both;\"></div>
                </div>
                <div class=\"land-data\" data-equalizer>
                    <div class=\"small-4 columns\" data-equalizer-watch>
                        <div class=\"wrap-land-corp\">
                            <div class=\"title-group-comp\">
                                <img src=\"";
        // line 267
        echo Uri::base();
        echo "/assets/css/images/oasis.png\"/>
                            </div>
                            <div class=\"img-land-corp\">
                                <img src=\"";
        // line 270
        echo Uri::base();
        echo "/assets/css/images/oasis-img.jpg\"/>
                            </div>
                            <div class=\"content-land-corp\">
                                ";
        // line 273
        echo Lang::get("group-oasis");
        echo "
                            </div>
                        </div>
                    </div>
                    <div class=\"small-4 columns\" data-equalizer-watch>
                        <div class=\"wrap-land-corp\">
                            <div class=\"title-group-comp\">
                                <img src=\"";
        // line 280
        echo Uri::base();
        echo "/assets/css/images/stay.png\"/>
                            </div>
                            <div class=\"img-land-corp\">
                                <img src=\"";
        // line 283
        echo Uri::base();
        echo "/assets/css/images/stay-img.jpg\"/>
                            </div>
                            <div class=\"content-land-corp\">
                                ";
        // line 286
        echo Lang::get("group-stay");
        echo "
                            </div>
                        </div>
                    </div>
                    <div class=\"small-4 columns\" data-equalizer-watch>
                        <div class=\"wrap-land-corp\">
                            <div class=\"title-group-comp\">
                                <img src=\"";
        // line 293
        echo Uri::base();
        echo "/assets/css/images/homes.png\"/>
                            </div>
                            <div class=\"img-land-corp\">
                                <img src=\"";
        // line 296
        echo Uri::base();
        echo "/assets/css/images/homes-img.jpg\"/>
                            </div>
                            <div class=\"content-land-corp\">
                                ";
        // line 299
        echo Lang::get("group-homes");
        echo "
                            </div>
                        </div>
                    </div>
                    <div style=\"clear: both;\"></div>
                </div>
            </div>
        </div>
    </div>
    <div class=\"scroll-top-wrapper \">
        <img src=\"";
        // line 309
        echo Uri::base();
        echo "/assets/css/images/top-land.png\"/>
    </div>
    <div style=\"height: 50px;\"></div>
";
    }

    // line 314
    public function block_frontend_js($context, array $blocks = array())
    {
        // line 315
        echo "    ";
        $this->displayParentBlock("frontend_js", $context, $blocks);
        echo "
    ";
        // line 316
        echo Asset::js("foundation/foundation.equalizer.js");
        echo "

    <script>
        \$(\"#fodusanClick\").click(function (){
            //\$(this).animate(function(){
            \$('html, body').animate({
                scrollTop: \$(\"#fodusan\").offset().top
            }, 1000);
            //});
        });

        \$(\"#landClick\").click(function (){
            //\$(this).animate(function(){
            \$('html, body').animate({
                scrollTop: \$(\"#land\").offset().top
            }, 1700);
            //});
        });

        \$(\"#groupClick\").click(function (){
            //\$(this).animate(function(){
            \$('html, body').animate({
                scrollTop: \$(\"#group\").offset().top
            }, 1700);
            //});
        });

        \$(function(){

            \$(document).on( 'scroll', function(){

                if (\$(window).scrollTop() > 500) {
                    \$('.scroll-top-wrapper').addClass('show');
                } else {
                    \$('.scroll-top-wrapper').removeClass('show');
                }
            });

            \$('.scroll-top-wrapper').on('click', scrollToTop);
        });

        function scrollToTop() {
            verticalOffset = typeof(verticalOffset) != 'undefined' ? verticalOffset : 0;
            element = \$('body');
            offset = element.offset();
            offsetTop = offset.top;
            \$('html, body').animate({scrollTop: offsetTop}, 500, 'linear');
        }
    </script>
";
    }

    public function getTemplateName()
    {
        return "group.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  497 => 316,  492 => 315,  489 => 314,  481 => 309,  468 => 299,  462 => 296,  456 => 293,  446 => 286,  440 => 283,  434 => 280,  424 => 273,  418 => 270,  412 => 267,  399 => 257,  393 => 254,  387 => 251,  377 => 244,  371 => 241,  365 => 238,  354 => 230,  348 => 227,  342 => 224,  315 => 200,  309 => 197,  293 => 184,  287 => 181,  274 => 171,  268 => 168,  254 => 157,  248 => 154,  232 => 141,  226 => 138,  213 => 128,  207 => 125,  193 => 114,  187 => 111,  170 => 97,  152 => 82,  146 => 79,  138 => 74,  132 => 71,  124 => 66,  118 => 63,  107 => 55,  101 => 52,  93 => 47,  87 => 44,  79 => 39,  73 => 36,  58 => 24,  37 => 7,  32 => 4,  29 => 3,);
    }
}
