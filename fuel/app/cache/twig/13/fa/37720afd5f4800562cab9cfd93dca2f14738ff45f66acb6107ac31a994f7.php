<?php

/* submit.twig */
class __TwigTemplate_13fa37720afd5f4800562cab9cfd93dca2f14738ff45f66acb6107ac31a994f7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("pages/views/template_frontend.twig");

        $this->blocks = array(
            'frontend_content' => array($this, 'block_frontend_content'),
            'frontend_js' => array($this, 'block_frontend_js'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "pages/views/template_frontend.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_frontend_content($context, array $blocks = array())
    {
        // line 4
        echo "<div class=\"tokyuland-career\">
    <div class=\"row\">
        <ul class=\"breadcrumbs\">
            <li><a href=\"";
        // line 7
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/home\">Home</a></li>
            <li class=\"current\">Career</li>
        </ul>
    </div>
    <div class=\"row\">
        <h1>Career</h1>
    </div>

    <div class=\"row\">
        <h3>Apply for ";
        // line 16
        echo $this->getAttribute((isset($context["vacancy_model"]) ? $context["vacancy_model"] : null), "name");
        echo " (";
        echo $this->getAttribute((isset($context["vacancy_model"]) ? $context["vacancy_model"] : null), "code");
        echo ")</h3>
        <form data-abide action=\"\" method=\"post\" enctype=\"multipart/form-data\">
            <div class=\"collapse\">
                <div class=\"custom-file-upload\">
                    <input type=\"file\" id=\"file\" name=\"cv_file\" accept=\"application/pdf, application/msword\"/>
                </div>
                <div style=\"clear: both;\"></div>
            </div>
            <div style=\"font-size: 10px; padding: 0 0 5px 0;\">*upload only pdf or doc/docx document</div>
            <div class=\"apply-button\">
                <a href=\"";
        // line 26
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/career\" class=\"button\">Back</a>
                <input type=\"submit\" value=\"Submit\" class=\"button\"/>
            </div>
        </form>

    </div>
    <div style=\"height: 50px;\"></div>
</div>
    ";
    }

    // line 36
    public function block_frontend_js($context, array $blocks = array())
    {
        // line 37
        echo "    ";
        $this->displayParentBlock("frontend_js", $context, $blocks);
        echo "
    ";
        // line 38
        echo Asset::js("foundation/foundation.abide.js");
        echo "
<script>
    
    \$(function() {
        \$('form').submit(function(){
          
            window.location.href = 'http://google.com';
          //return false;
        });
      });
    
    ";
        // line 53
        echo "    
    ";
        // line 59
        echo "    
        //Reference:
        //http://www.onextrapixel.com/2012/12/10/how-to-create-a-custom-file-input-with-jquery-css3-and-php/
        (function(\$) {

            // Browser supports HTML5 multiple file?
            var multipleSupport = typeof \$('<input/>')[0].multiple !== 'undefined',
                    isIE = /msie/i.test( navigator.userAgent );

            \$.fn.customFile = function() {

                return this.each(function() {

                    var \$file = \$(this).addClass('custom-file-upload-hidden'), // the original file input
                            \$wrap = \$('<div class=\"file-upload-wrapper\">'),
                            \$input = \$('<input type=\"text\" class=\"file-upload-input\" />'),
                    // Button that will be used in non-IE browsers
                            \$button = \$('<button type=\"button\" class=\"file-upload-button\">Browse</button>'),
                    // Hack for IE
                            \$label = \$('<label class=\"file-upload-button\" for=\"'+ \$file[0].id +'\">Select a File</label>');

                    // Hide by shifting to the left so we
                    // can still trigger events
                    \$file.css({
                        position: 'absolute',
                        left: '-9999px'
                    });

                    \$wrap.insertAfter( \$file )
                            .append( \$file, \$input, ( isIE ? \$label : \$button ) );

                    // Prevent focus
                    \$file.attr('tabIndex', -1);
                    \$button.attr('tabIndex', -1);

                    \$button.click(function () {
                        \$file.focus().click(); // Open dialog
                    });

                    \$file.change(function() {

                        var files = [], fileArr, filename;

                        // If multiple is supported then extract
                        // all filenames from the file array
                        if ( multipleSupport ) {
                            fileArr = \$file[0].files;
                            for ( var i = 0, len = fileArr.length; i < len; i++ ) {
                                files.push( fileArr[i].name );
                            }
                            filename = files.join(', ');

                            // If not supported then just take the value
                            // and remove the path to just show the filename
                        } else {
                            filename = \$file.val().split('\\\\').pop();
                        }

                        \$input.val( filename ) // Set the value
                                .attr('title', filename) // Show filename in title tootlip
                                .focus(); // Regain focus

                    });

                    \$input.on({
                        blur: function() { \$file.trigger('blur'); },
                        keydown: function( e ) {
                            if ( e.which === 13 ) { // Enter
                                if ( !isIE ) { \$file.trigger('click'); }
                            } else if ( e.which === 8 || e.which === 46 ) { // Backspace & Del
                                // On some browsers the value is read-only
                                // with this trick we remove the old input and add
                                // a clean clone with all the original events attached
                                \$file.replaceWith( \$file = \$file.clone( true ) );
                                \$file.trigger('change');
                                \$input.val('');
                            } else if ( e.which === 9 ){ // TAB
                                return;
                            } else { // All other keys
                                return false;
                            }
                        }
                    });

                });

            };

            // Old browser fallback
            if ( !multipleSupport ) {
                \$( document ).on('change', 'input.customfile', function() {

                    var \$this = \$(this),
                    // Create a unique ID so we
                    // can attach the label to the input
                            uniqId = 'customfile_'+ (new Date()).getTime(),
                            \$wrap = \$this.parent(),

                    // Filter empty input
                            \$inputs = \$wrap.siblings().find('.file-upload-input')
                                    .filter(function(){ return !this.value }),

                            \$file = \$('<input type=\"file\" id=\"'+ uniqId +'\" name=\"'+ \$this.attr('name') +'\"/>');

                    // 1ms timeout so it runs after all other events
                    // that modify the value have triggered
                    setTimeout(function() {
                        // Add a new input
                        if ( \$this.val() ) {
                            // Check for empty fields to prevent
                            // creating new inputs when changing files
                            if ( !\$inputs.length ) {
                                \$wrap.after( \$file );
                                \$file.customFile();
                            }
                            // Remove and reorganize inputs
                        } else {
                            \$inputs.parent().remove();
                            // Move the input so it's always last on the list
                            \$wrap.appendTo( \$wrap.parent() );
                            \$wrap.find('input').focus();
                        }
                    }, 1);

                });
            }

        }(jQuery));

        \$('input[type=file]').customFile();
    </script>
    ";
    }

    public function getTemplateName()
    {
        return "submit.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  104 => 59,  101 => 53,  87 => 38,  82 => 37,  79 => 36,  65 => 26,  50 => 16,  37 => 7,  32 => 4,  29 => 3,);
    }
}
