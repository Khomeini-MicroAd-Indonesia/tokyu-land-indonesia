<?php

/* business.twig */
class __TwigTemplate_b621e149b414935b874378c5da5f43c034060e787c78c6cd438e8ea8d783b1eb extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("pages/views/template_frontend.twig");

        $this->blocks = array(
            'frontend_content' => array($this, 'block_frontend_content'),
            'frontend_js' => array($this, 'block_frontend_js'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "pages/views/template_frontend.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_frontend_content($context, array $blocks = array())
    {
        // line 4
        echo "    <div class=\"tokyuland-business\">
        <div class=\"row\">
            <ul class=\"breadcrumbs\">
                <li><a href=\"";
        // line 7
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/home\">Home</a></li>
                <li><a href=\"";
        // line 8
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/about-us/president\">About Us</a></li>
                <li class=\"current\">History in Indonesia</li>
            </ul>
        </div>
        <div class=\"row about-sub-menu\">
            <div class=\"small-2 columns\">
                About us |
            </div>
            <div class=\"small-10 columns\">
                <ul class=\"inline-list\">
                    <li><a href=\"";
        // line 18
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/about-us/president\">President Director’s Message</a></li>
                    <li><a href=\"";
        // line 19
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/about-us/history\">History in Indonesia</a></li>
                    <li><a class=\"active\" href=\"";
        // line 20
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/about-us/business\">Business Development</a></li>
                    <li><a href=\"";
        // line 21
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/about-us/company\">Company Profile</a></li>
                </ul>
            </div>
        </div>
        <div class=\"row\">
            <div class=\"small-12 columns\">
                <h3>
                    <strong>Business Development</strong>
                </h3>
                <div class=\"business-slogan\">
                    TOKYU LAND INDONESIA: <span>Creating new urban landscapes, creating the future.</span>
                </div>
                <div class=\"business-subtitle\">
                    Businesses we aim to expand:
                </div>
                <div class=\"small-text-center\">
                    <img src=\"";
        // line 37
        echo Uri::base();
        echo "/assets/css/images/business-chart-";
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo ".png\"/>
                </div>
            </div>
        </div>
    </div>

    <div style=\"height: 50px;\"></div>
";
    }

    // line 46
    public function block_frontend_js($context, array $blocks = array())
    {
        // line 47
        echo "    ";
        $this->displayParentBlock("frontend_js", $context, $blocks);
        echo "
";
    }

    public function getTemplateName()
    {
        return "business.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  108 => 47,  105 => 46,  91 => 37,  71 => 21,  66 => 20,  61 => 19,  56 => 18,  42 => 8,  37 => 7,  32 => 4,  29 => 3,);
    }
}
