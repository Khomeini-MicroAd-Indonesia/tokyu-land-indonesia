<?php

/* thanks.twig */
class __TwigTemplate_8f896ce9f86a414c4850e163de609a1bc74db2da06e7c75ce4bb8bccbbdc743d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("pages/views/template_frontend.twig");

        $this->blocks = array(
            'frontend_content' => array($this, 'block_frontend_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "pages/views/template_frontend.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_frontend_content($context, array $blocks = array())
    {
        // line 4
        echo "<div class=\"tokyuland-career\">
    <div class=\"row\">
        <ul class=\"breadcrumbs\">
            <li><a href=\"";
        // line 7
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "\">Home</a></li>
            <li class=\"current\">Career</li>
        </ul>
    </div>
    <div class=\"row\">
        <h1>Career</h1>
    </div>

    <div class=\"row\">
        <h3>Apply for ";
        // line 16
        echo $this->getAttribute((isset($context["vacancy_model"]) ? $context["vacancy_model"] : null), "name");
        echo " (";
        echo $this->getAttribute((isset($context["vacancy_model"]) ? $context["vacancy_model"] : null), "code");
        echo ")</h3>
        <div class=\"foot-note\" style=\"font-weight: normal;\">
            Thank you for your application for this position at our company.<br/>
            We will contact you in only the case you pass examinations of Documents(CV).<br/><br/>
        </div>

        <div class=\"apply-button\">
            <a href=\"";
        // line 23
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/career\" class=\"button\">Career</a>
            <a href=\"";
        // line 24
        echo Uri::base();
        echo "\" class=\"button\">Home</a>
        </div>


    </div>
    <div style=\"height: 50px;\"></div>
    ";
    }

    public function getTemplateName()
    {
        return "thanks.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  66 => 24,  61 => 23,  49 => 16,  36 => 7,  31 => 4,  28 => 3,);
    }
}
