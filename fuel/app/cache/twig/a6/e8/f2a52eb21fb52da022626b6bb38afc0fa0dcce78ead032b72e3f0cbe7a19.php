<?php

/* m_home.twig */
class __TwigTemplate_a6e8f2a52eb21fb52da022626b6bb38afc0fa0dcce78ead032b72e3f0cbe7a19 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("pages/views/mobile_template.twig");

        $this->blocks = array(
            'frontend_content' => array($this, 'block_frontend_content'),
            'frontend_js' => array($this, 'block_frontend_js'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "pages/views/mobile_template.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_frontend_content($context, array $blocks = array())
    {
        // line 4
        echo "<div>
    <ul class=\"example-orbit\" data-orbit data-options=\"animation:slide; pause_on_hover:true; resume_on_mouseout: true; timer_speed: 5000; slide_number: false; animation_speed:500; navigation_arrows:false; bullets:false;\">
        ";
        // line 6
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["slide_img"]) ? $context["slide_img"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["filename"]) {
            // line 7
            echo "            <li>
                <img src=\"";
            // line 8
            echo Uri::base();
            echo "media/slide/";
            echo (isset($context["filename"]) ? $context["filename"] : null);
            echo "\" class=\"img-responsive\"/>
            </li>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['filename'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 11
        echo "    </ul>
    <img class=\"branz-banner\" src=\"";
        // line 12
        echo Uri::base();
        echo "/assets/css/images/Branz-Banner.jpg\"/>
</div>
<div style=\"height: 50px;\"></div>
<div>
    <div class=\"project-home-title\">
        <div class=\"small-12 columns\">
            Project
        </div>
        <div style=\"clear: both\"></div>

    </div>

        <div class=\"row\">
            ";
        // line 25
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["project_data"]) ? $context["project_data"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["project"]) {
            // line 26
            echo "            <div class=\"project-home\">
                <div class=\"project-wrap\">
                    <div class=\"small-12 columns\">
                        <img src=\"";
            // line 29
            echo Uri::base();
            echo "media/project/thumbnail/";
            echo $this->getAttribute((isset($context["project"]) ? $context["project"] : null), "image");
            echo "\" class=\"img-responsive\"/>
                    </div>
                    <div style=\"clear: both\"></div>
                    <div class=\"small-12 columns project-desc\">
                        <h5>";
            // line 33
            echo $this->getAttribute((isset($context["project"]) ? $context["project"] : null), "name");
            echo "</h5>
                        <h5>( ";
            // line 34
            echo $this->getAttribute((isset($context["project"]) ? $context["project"] : null), "type");
            echo " )</h5>
                    </div>
                    <div style=\"clear: both\"></div>
                </div>
            </div>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['project'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 40
        echo "            <div class=\"text-center project-button\">
                <a href=\"";
        // line 41
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/project\">
                    <div>
                       All Projects
                    </div>
                </a>
            </div>
        </div>

</div>
<div style=\"height: 50px;\"></div>
<div>
    <div class=\"project-home-title\">
        <div class=\"small-12 columns\">
            News Release
        </div>
        <div style=\"clear: both\"></div>

    </div>
    <div class=\"row\">
        ";
        // line 60
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["news_data"]) ? $context["news_data"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["news"]) {
            // line 61
            echo "        <div class=\"project-home\">
            <div class=\"project-wrap\">
                <div class=\"small-12 columns\">
                    <img src=\"";
            // line 64
            echo Uri::base();
            echo "media/news/thumbnail/";
            echo $this->getAttribute((isset($context["news"]) ? $context["news"] : null), "image");
            echo "\" class=\"img-responsive\"/>
                </div>
                <div style=\"clear: both\"></div>
                <div class=\"small-12 columns project-desc\">
                    <h5>";
            // line 68
            echo $this->getAttribute((isset($context["news"]) ? $context["news"] : null), "title");
            echo "</h5>
                </div>
                <div style=\"clear: both\"></div>
            </div>
        </div>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['news'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 74
        echo "        <div class=\"text-center project-button\">
            <a href=\"";
        // line 75
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/news\">
                <div>
                    All News
                </div>
            </a>
        </div>
    </div>

</div>

<div style=\"height: 50px;\"></div>
";
    }

    // line 88
    public function block_frontend_js($context, array $blocks = array())
    {
        // line 89
        echo "    ";
        $this->displayParentBlock("frontend_js", $context, $blocks);
        echo "
    ";
        // line 90
        echo Asset::js("foundation/foundation.orbit.js");
        echo "
";
    }

    public function getTemplateName()
    {
        return "m_home.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  191 => 90,  186 => 89,  183 => 88,  166 => 75,  163 => 74,  151 => 68,  142 => 64,  137 => 61,  133 => 60,  110 => 41,  107 => 40,  95 => 34,  91 => 33,  82 => 29,  77 => 26,  73 => 25,  57 => 12,  54 => 11,  43 => 8,  40 => 7,  36 => 6,  32 => 4,  29 => 3,);
    }
}
