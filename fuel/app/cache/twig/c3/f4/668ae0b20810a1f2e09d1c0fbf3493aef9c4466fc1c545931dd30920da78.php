<?php

/* home.twig */
class __TwigTemplate_c3f4668ae0b20810a1f2e09d1c0fbf3493aef9c4466fc1c545931dd30920da78 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("pages/views/template_frontend.twig");

        $this->blocks = array(
            'frontend_content' => array($this, 'block_frontend_content'),
            'frontend_js' => array($this, 'block_frontend_js'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "pages/views/template_frontend.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_frontend_content($context, array $blocks = array())
    {
        // line 4
        echo "    <div class=\"tokyuland-home\">
        <div class=\"row\">
            <ul class=\"example-orbit\" data-orbit data-options=\"animation:slide; pause_on_hover:true; resume_on_mouseout: true; timer_speed: 5000; slide_number: false; animation_speed:2000; navigation_arrows:false; bullets:false;\">

                ";
        // line 8
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["slide_img"]) ? $context["slide_img"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["filename"]) {
            // line 9
            echo "                    <li>
                        <img src=\"";
            // line 10
            echo Uri::base();
            echo "media/slide/";
            echo (isset($context["filename"]) ? $context["filename"] : null);
            echo "\" class=\"img-responsive\"/>
                    </li>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['filename'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 13
        echo "            </ul>
            <a href=\"http://branz-bsd.com/\" target=\"_blank\"><img src=\"";
        // line 14
        echo Uri::base();
        echo "/assets/css/images/Branz-Banner.jpg\"/></a>
        </div>
        <div class=\"row\" data-equalizer = \"mother\">
            <div class=\"small-6 columns\" data-equalizer-watch = \"mother\">
                <div class=\"project-home-title\">
                    <div class=\"small-8 columns\">
                        Project
                    </div>
                    <div class=\"small-4 columns\">
                        <span> > </span> <a href=\"";
        // line 23
        echo (isset($context["base_url"]) ? $context["base_url"] : null);
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/project\">All Projects</a>
                    </div>
                    <div style=\"clear: both\"></div>

                </div>
                ";
        // line 28
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["project_data"]) ? $context["project_data"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["project"]) {
            // line 29
            echo "                <div class=\"row\" data-equalizer=\"project\">
                    <div class=\"project-home\" data-equalizer-watch=\"project\">
                        <div class=\"small-6 columns\">
                            <a href=\"";
            // line 32
            echo $this->getAttribute((isset($context["project"]) ? $context["project"] : null), "web");
            echo "\" target=\"_blank\">
                                <img src=\"";
            // line 33
            echo Uri::base();
            echo "media/project/thumbnail/";
            echo $this->getAttribute((isset($context["project"]) ? $context["project"] : null), "image");
            echo "\" class=\"img-responsive\"/>
                            </a>
                        </div>
                        <div class=\"small-6 columns\">
                            <h5>";
            // line 37
            echo $this->getAttribute((isset($context["project"]) ? $context["project"] : null), "name");
            echo "</h5>
                            <h6><b>( ";
            // line 38
            echo $this->getAttribute((isset($context["project"]) ? $context["project"] : null), "type");
            echo " )</b></h6>
                            <p>";
            // line 39
            echo $this->getAttribute((isset($context["project"]) ? $context["project"] : null), "detail");
            echo "</p>
                            <p><b>Web: </b><a href=\"";
            // line 40
            echo $this->getAttribute((isset($context["project"]) ? $context["project"] : null), "web");
            echo "\" target=\"_blank\">";
            echo $this->getAttribute((isset($context["project"]) ? $context["project"] : null), "web");
            echo "</a></p>
                        </div>
                        <div style=\"clear: both;\"></div>
                    </div>
                        
                </div>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['project'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 47
        echo "            </div>
            <div class=\"small-6 columns\" data-equalizer-watch = \"mother\">
                <div class=\"news-home-title\">
                    <div class=\"small-8 columns\">
                        News Release
                    </div>
                    <div class=\"small-4 columns\">
                        <span> > </span> <a href=\"";
        // line 54
        echo (isset($context["base_url"]) ? $context["base_url"] : null);
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/news\">All News</a>
                    </div>
                    <div style=\"clear: both\"></div>
                </div>
                ";
        // line 58
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["news_data"]) ? $context["news_data"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["news"]) {
            // line 59
            echo "                <div class=\"row\" data-equalizer=\"news\">
                    <div class=\"news-home\" data-equalizer-watch=\"news\">
                        <div class=\"small-6 columns\">
                            <a href=\"";
            // line 62
            echo Uri::base();
            echo "media/news/doc/";
            echo $this->getAttribute((isset($context["news"]) ? $context["news"] : null), "doc_filename");
            echo "\" target=\"_blank\">
                                <img src=\"";
            // line 63
            echo Uri::base();
            echo "media/news/thumbnail/";
            echo $this->getAttribute((isset($context["news"]) ? $context["news"] : null), "image");
            echo "\" class=\"img-responsive\"/>
                            </a>
                        </div>
                        <div class=\"small-6 columns\">
                            <h5>";
            // line 67
            echo $this->getAttribute((isset($context["news"]) ? $context["news"] : null), "date");
            echo "</h5>
                            <p>";
            // line 68
            echo $this->getAttribute((isset($context["news"]) ? $context["news"] : null), "doc");
            echo "</p>
                            <p><a href=\"";
            // line 69
            echo Uri::base();
            echo "media/news/doc/";
            echo $this->getAttribute((isset($context["news"]) ? $context["news"] : null), "doc_filename");
            echo "\" target=\"_blank\"><img src=\"";
            echo Uri::base();
            echo "assets/img/pdf-icon-smallest.png\" class=\"img-responsive\"/></a></p>
                        </div>
                        <div style=\"clear: both\"></div>
                    </div>
                </div>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['news'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 75
        echo "            </div>
        </div>
    </div>

    <div style=\"height: 50px;\"></div>
";
    }

    // line 82
    public function block_frontend_js($context, array $blocks = array())
    {
        // line 83
        echo "    ";
        $this->displayParentBlock("frontend_js", $context, $blocks);
        echo "
    ";
        // line 84
        echo Asset::js("foundation/foundation.orbit.js");
        echo "
    ";
        // line 85
        echo Asset::js("foundation/foundation.equalizer.js");
        echo "
";
    }

    public function getTemplateName()
    {
        return "home.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  215 => 85,  211 => 84,  206 => 83,  203 => 82,  194 => 75,  178 => 69,  174 => 68,  170 => 67,  161 => 63,  155 => 62,  150 => 59,  146 => 58,  138 => 54,  129 => 47,  114 => 40,  110 => 39,  106 => 38,  102 => 37,  93 => 33,  89 => 32,  84 => 29,  80 => 28,  71 => 23,  59 => 14,  56 => 13,  45 => 10,  42 => 9,  38 => 8,  32 => 4,  29 => 3,);
    }
}
