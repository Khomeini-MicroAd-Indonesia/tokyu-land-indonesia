<?php

/* privacy.twig */
class __TwigTemplate_047afdbf64437f1572f2a78442ccdcaffe4db1c65becb5374ac5f0eff31d1481 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("pages/views/template_frontend.twig");

        $this->blocks = array(
            'frontend_content' => array($this, 'block_frontend_content'),
            'frontend_js' => array($this, 'block_frontend_js'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "pages/views/template_frontend.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_frontend_content($context, array $blocks = array())
    {
        // line 4
        echo "    <div class=\"tokyuland-philosophy\">
        <div class=\"row\">
            <ul class=\"breadcrumbs\">
                <li><a href=\"";
        // line 7
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/home\">Home</a></li>
                <li class=\"current\">Privacy Policy</li>
            </ul>
        </div>
        <div class=\"extra-content row\">
            <h1>Privacy Policy</h1>
            <div>
                ";
        // line 14
        echo Lang::get("privacy");
        echo "
            </div>
        </div>
    </div>

    <div style=\"height: 50px;\"></div>
";
    }

    // line 22
    public function block_frontend_js($context, array $blocks = array())
    {
        // line 23
        echo "    ";
        $this->displayParentBlock("frontend_js", $context, $blocks);
        echo "
";
    }

    public function getTemplateName()
    {
        return "privacy.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  62 => 23,  59 => 22,  48 => 14,  37 => 7,  32 => 4,  29 => 3,);
    }
}
