<?php

/* no_permission.twig */
class __TwigTemplate_bed5768faaef1862f86b15cfec19baf861a8560d0573aeb99f9928c440e3e0c5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("backend/base.twig");

        $this->blocks = array(
            'backend_content' => array($this, 'block_backend_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "backend/base.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_backend_content($context, array $blocks = array())
    {
        // line 4
        echo "<div class=\"wrapper row-offcanvas row-offcanvas-left\">
\t<!-- Main content -->
\t<section class=\"content\">

\t\t<div class=\"error-page\">
\t\t\t<h2 class=\"headline text-info\"> No Permission</h2>
\t\t\t<div class=\"error-content\">
\t\t\t\t<h3><i class=\"fa fa-warning text-yellow\"></i> You don't have a permission.</h3>
\t\t\t\t<p>
\t\t\t\t\tThe page you were looking for is need a permission, please contact administrator to grant the permission.
\t\t\t\t</p>
\t\t\t\t<p>
\t\t\t\t\t<a href=\"";
        // line 16
        echo (isset($context["redirect_to"]) ? $context["redirect_to"] : null);
        echo "\">
\t\t\t\t\t\tBack
\t\t\t\t\t</a>
\t\t\t\t</p>
\t\t\t</div><!-- /.error-content -->
\t\t</div><!-- /.error-page -->

\t</section><!-- /.content -->
</div><!-- ./wrapper -->
";
    }

    public function getTemplateName()
    {
        return "no_permission.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  45 => 16,  31 => 4,  28 => 3,);
    }
}
