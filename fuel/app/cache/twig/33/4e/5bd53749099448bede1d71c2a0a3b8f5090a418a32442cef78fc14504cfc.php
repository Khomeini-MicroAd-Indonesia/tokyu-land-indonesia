<?php

/* jquery_doc_uploader.twig */
class __TwigTemplate_334e5bd53749099448bede1d71c2a0a3b8f5090a418a32442cef78fc14504cfc extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("backend/template.twig");

        $this->blocks = array(
            'backend_css' => array($this, 'block_backend_css'),
            'backend_content_header' => array($this, 'block_backend_content_header'),
            'backend_content' => array($this, 'block_backend_content'),
            'backend_js' => array($this, 'block_backend_js'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "backend/template.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_backend_css($context, array $blocks = array())
    {
        // line 4
        echo "\t";
        $this->displayParentBlock("backend_css", $context, $blocks);
        echo "
\t
\t<!-- Bootstrap styles -->
\t<link rel=\"stylesheet\" href=\"//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css\">
\t
\t<!-- blueimp Gallery styles -->
\t<link rel=\"stylesheet\" href=\"//blueimp.github.io/Gallery/css/blueimp-gallery.min.css\">
\t
\t<!-- CSS to style the file input field as button and adjust the Bootstrap progress bars -->
\t";
        // line 13
        echo Asset::css("jquery.fileupload.css");
        echo "
\t";
        // line 14
        echo Asset::css("jquery.fileupload-ui.css");
        echo "
\t
\t<!-- CSS adjustments for browsers with JavaScript disabled -->
\t<noscript>";
        // line 17
        echo Asset::css("jquery.fileupload-noscript.css");
        echo "</noscript>
\t<noscript>";
        // line 18
        echo Asset::css("jquery.fileupload-ui-noscript.css");
        echo "</noscript>
";
    }

    // line 21
    public function block_backend_content_header($context, array $blocks = array())
    {
        // line 22
        echo "<!-- Content Header (Page header) -->
<section class=\"content-header\">
\t<h1>";
        // line 25
        echo "            Media Uploader
            <small>";
        // line 26
        echo (isset($context["media_uploader_subtitle"]) ? $context["media_uploader_subtitle"] : null);
        echo "</small>
\t</h1>
\t";
        // line 29
        echo "\t<ol class=\"breadcrumb\">
            <li><a href=\"";
        // line 30
        echo Uri::base();
        echo "backend\">Home</a></li>
            <li>Media Uploader</li>
            <li class=\"active\">";
        // line 32
        echo (isset($context["media_uploader_subtitle"]) ? $context["media_uploader_subtitle"] : null);
        echo "</li>
\t</ol>
</section>
";
    }

    // line 37
    public function block_backend_content($context, array $blocks = array())
    {
        // line 38
        echo "
";
        // line 39
        if ((twig_length_filter($this->env, (isset($context["success_message"]) ? $context["success_message"] : null)) > 0)) {
            // line 40
            echo "<div class=\"alert alert-success alert-dismissable\">
\t<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>
\t";
            // line 42
            echo (isset($context["success_message"]) ? $context["success_message"] : null);
            echo "
</div>
";
        }
        // line 45
        echo "
";
        // line 46
        if ((twig_length_filter($this->env, (isset($context["error_message"]) ? $context["error_message"] : null)) > 0)) {
            // line 47
            echo "<div class=\"alert alert-danger alert-dismissable\">
\t<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>
\t";
            // line 49
            echo (isset($context["error_message"]) ? $context["error_message"] : null);
            echo "
</div>
";
        }
        // line 52
        echo "
\t<div class=\"box box-solid\">
\t\t<div class=\"box-header\">
                    <i class=\"fa fa-text-width\"></i>
                    <h3 class=\"box-title\">Information</h3>
\t\t</div><!-- /.box-header -->
\t\t<div class=\"box-body\">
                    ";
        // line 59
        echo (isset($context["information_text"]) ? $context["information_text"] : null);
        echo "
\t\t</div><!-- /.box-body -->
\t</div>
\t
\t<!-- The file upload form used as target for the file upload widget -->
\t<form id=\"fileupload\" action=\"";
        // line 64
        echo (isset($context["form_action_url"]) ? $context["form_action_url"] : null);
        echo "\" method=\"post\" enctype=\"multipart/form-data\">
\t\t<!-- Redirect browsers with JavaScript disabled to the origin page -->
\t\t<noscript><input type=\"hidden\" name=\"redirect\" value=\"";
        // line 66
        echo Uri::base();
        echo "backend\"></noscript>
\t\t
\t\t<!-- The fileupload-buttonbar contains buttons to add/delete files and start/cancel the upload -->
\t\t<div class=\"row fileupload-buttonbar\">
                    <div class=\"col-lg-7\">
                            <!-- The fileinput-button span is used to style the file input field as button -->
                            <span class=\"btn btn-success fileinput-button\">
                                <i class=\"glyphicon glyphicon-plus\"></i>
                                <span>Add files...</span>
                                <input type=\"file\" name=\"files[]\" multiple>
                            </span>
                            <button type=\"submit\" class=\"btn btn-primary start\">
                                <i class=\"glyphicon glyphicon-upload\"></i>
                                <span>Start upload</span>
                            </button>
                            <button type=\"reset\" class=\"btn btn-warning cancel\">
                                <i class=\"glyphicon glyphicon-ban-circle\"></i>
                                <span>Cancel upload</span>
                            </button>
                            <button type=\"button\" class=\"btn btn-danger delete\">
                                <i class=\"glyphicon glyphicon-trash\"></i>
                                <span>Delete</span>
                            </button>
                            <!-- The global file processing state -->
                            <span class=\"fileupload-process\"></span>
                    </div>
                    <!-- The global progress state -->
                    <div class=\"col-lg-5 fileupload-progress fade\">
                        <!-- The global progress bar -->
                        <div class=\"progress progress-striped active\" role=\"progressbar\" aria-valuemin=\"0\" aria-valuemax=\"100\">
                            <div class=\"progress-bar progress-bar-success\" style=\"width:0%;\"></div>
                        </div>
                        <!-- The extended global progress state -->
                        <div class=\"progress-extended\">&nbsp;</div>
                    </div>
\t\t</div>
\t\t<!-- The table listing the files available for upload/download -->
\t\t<table role=\"presentation\" class=\"table table-striped\"><tbody class=\"files\"></tbody></table>
\t</form>
\t</div>
\t<!-- The blueimp Gallery widget -->
\t<div id=\"blueimp-gallery\" class=\"blueimp-gallery blueimp-gallery-controls\" data-filter=\":even\">
            <div class=\"slides\"></div>
            <h3 class=\"title\"></h3>
            <a class=\"prev\">‹</a>
            <a class=\"next\">›</a>
            <a class=\"close\">×</a>
            <a class=\"play-pause\"></a>
            <ol class=\"indicator\"></ol>
\t</div>
";
    }

    // line 118
    public function block_backend_js($context, array $blocks = array())
    {
        // line 119
        echo "\t";
        $this->displayParentBlock("backend_js", $context, $blocks);
        echo "
\t
\t<!-- DATA TABES SCRIPT -->
\t";
        // line 122
        echo Asset::js("plugins/datatables/jquery.dataTables.js");
        echo "
\t";
        // line 123
        echo Asset::js("plugins/datatables/dataTables.bootstrap.js");
        echo "
\t
\t<!-- The template to display files available for upload -->
\t<script id=\"template-upload\" type=\"text/x-tmpl\">
\t\t";
        // line 157
        echo "
\t\t{% for (var i=0, file; file=o.files[i]; i++) { %}
\t\t\t<tr class=\"template-upload fade\">
\t\t\t\t<td>
\t\t\t\t\t<span class=\"preview\"></span>
\t\t\t\t</td>
\t\t\t\t<td>
\t\t\t\t\t<p class=\"name\">{%=file.name%}</p>
\t\t\t\t\t<strong class=\"error text-danger\"></strong>
\t\t\t\t</td>
\t\t\t\t<td>
\t\t\t\t\t<p class=\"size\">Processing...</p>
\t\t\t\t\t<div class=\"progress progress-striped active\" role=\"progressbar\" aria-valuemin=\"0\" aria-valuemax=\"100\" aria-valuenow=\"0\"><div class=\"progress-bar progress-bar-success\" style=\"width:0%;\"></div></div>
\t\t\t\t</td>
\t\t\t\t<td>
\t\t\t\t\t{% if (!i && !o.options.autoUpload) { %}
\t\t\t\t\t\t<button class=\"btn btn-primary start\" disabled>
\t\t\t\t\t\t\t<i class=\"glyphicon glyphicon-upload\"></i>
\t\t\t\t\t\t\t<span>Start</span>
\t\t\t\t\t\t</button>
\t\t\t\t\t{% } %}
\t\t\t\t\t{% if (!i) { %}
\t\t\t\t\t\t<button class=\"btn btn-warning cancel\">
\t\t\t\t\t\t\t<i class=\"glyphicon glyphicon-ban-circle\"></i>
\t\t\t\t\t\t\t<span>Cancel</span>
\t\t\t\t\t\t</button>
\t\t\t\t\t{% } %}
\t\t\t\t</td>
\t\t\t</tr>
\t\t{% } %}
\t\t";
        echo "
\t</script>
\t
\t<!-- The template to display files available for download -->
\t<script id=\"template-download\" type=\"text/x-tmpl\">
\t\t";
        // line 201
        echo "
\t\t{% for (var i=0, file; file=o.files[i]; i++) { %}
\t\t\t<tr class=\"template-download fade\">
\t\t\t\t<td>
                                    <span class=\"preview\">
                                        <a href=\"{%=file.url%}\" title=\"{%=file.name%}\" download=\"{%=file.name%}\" data-gallery><img src=\"http://localhost:8888/MicroAd/tokyuland/assets/img/pdf-icon.png\"></a>
                                    </span>
\t\t\t\t</td>
\t\t\t\t<td>
\t\t\t\t\t<p class=\"name\">
\t\t\t\t\t\t{% if (file.url) { %}
\t\t\t\t\t\t\t<a href=\"{%=file.url%}\" title=\"{%=file.name%}\" download=\"{%=file.name%}\" {%=file.thumbnailUrl?'data-gallery':''%}>{%=file.name%}</a>
\t\t\t\t\t\t{% } else { %}
\t\t\t\t\t\t\t<span>{%=file.name%}</span>
\t\t\t\t\t\t{% } %}
\t\t\t\t\t</p>
\t\t\t\t\t{% if (file.error) { %}
\t\t\t\t\t\t<div><span class=\"label label-danger\">Error</span> {%=file.error%}</div>
\t\t\t\t\t{% } %}
\t\t\t\t</td>
\t\t\t\t<td>
\t\t\t\t\t<span class=\"size\">{%=o.formatFileSize(file.size)%}</span>
\t\t\t\t</td>
\t\t\t\t<td>
\t\t\t\t\t{% if (file.deleteUrl) { %}
\t\t\t\t\t\t<button class=\"btn btn-danger delete\" data-type=\"{%=file.deleteType%}\" data-url=\"{%=file.deleteUrl%}\"{% if (file.deleteWithCredentials) { %} data-xhr-fields='{\"withCredentials\":true}'{% } %}>
\t\t\t\t\t\t\t<i class=\"glyphicon glyphicon-trash\"></i>
\t\t\t\t\t\t\t<span>Delete</span>
\t\t\t\t\t\t</button>
\t\t\t\t\t\t<input type=\"checkbox\" name=\"delete\" value=\"1\" class=\"toggle\">
\t\t\t\t\t{% } else { %}
\t\t\t\t\t\t<button class=\"btn btn-warning cancel\">
\t\t\t\t\t\t\t<i class=\"glyphicon glyphicon-ban-circle\"></i>
\t\t\t\t\t\t\t<span>Cancel</span>
\t\t\t\t\t\t</button>
\t\t\t\t\t{% } %}
\t\t\t\t</td>
\t\t\t</tr>
\t\t{% } %}
\t\t";
        echo "
\t</script>
\t
\t<!-- The jQuery UI -->
\t";
        // line 205
        echo Asset::js("jquery-ui-1.11.0.min.js");
        echo "
\t
\t<!-- The Templates plugin is included to render the upload/download listings -->
\t";
        // line 208
        echo Asset::js("blueimp/tmpl.min.js");
        echo "
\t
\t<!-- The Load Image plugin is included for the preview images and image resizing functionality -->
\t";
        // line 211
        echo Asset::js("blueimp/load-image.all.min.js");
        echo "
\t
\t<!-- The Canvas to Blob plugin is included for image resizing functionality -->
\t";
        // line 214
        echo Asset::js("blueimp/canvas-to-blob.min.js");
        echo "
\t
\t<!-- blueimp Gallery script -->
\t";
        // line 217
        echo Asset::js("blueimp/jquery.blueimp-gallery.min.js");
        echo "
\t
\t<!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
\t";
        // line 220
        echo Asset::js("jquery-fileupload/jquery.iframe-transport.js");
        echo "
\t
\t<!-- The basic File Upload plugin -->
\t";
        // line 223
        echo Asset::js("jquery-fileupload/jquery.fileupload.js");
        echo "
\t
\t<!-- The File Upload processing plugin -->
\t";
        // line 226
        echo Asset::js("jquery-fileupload/jquery.fileupload-process.js");
        echo "
\t
\t<!-- The File Upload image preview & resize plugin -->
\t";
        // line 229
        echo Asset::js("jquery-fileupload/jquery.fileupload-image.js");
        echo "
\t
\t<!-- The File Upload audio preview plugin -->
\t";
        // line 232
        echo Asset::js("jquery-fileupload/jquery.fileupload-audio.js");
        echo "
\t
\t<!-- The File Upload video preview plugin -->
\t";
        // line 235
        echo Asset::js("jquery-fileupload/jquery.fileupload-video.js");
        echo "
\t
\t<!-- The File Upload validation plugin -->
\t";
        // line 238
        echo Asset::js("jquery-fileupload/jquery.fileupload-validate.js");
        echo "
\t
\t<!-- The File Upload user interface plugin -->
\t";
        // line 241
        echo Asset::js("jquery-fileupload/jquery.fileupload-ui.js");
        echo "
\t
\t<!-- The main application script -->
\t<script type=\"text/javascript\">
\t\t\$(function () {
\t\t\t'use strict';

\t\t\t// Initialize the jQuery File Upload widget:
\t\t\t\$('#fileupload').fileupload({
\t\t\t\t// Uncomment the following to send cross-domain cookies:
\t\t\t\t//xhrFields: {withCredentials: true},
\t\t\t\turl: \$('#fileupload').attr('action')
\t\t\t});

\t\t\t// Enable iframe cross-domain access via redirect option:
\t\t\t\$('#fileupload').fileupload(
\t\t\t\t'option',
\t\t\t\t'redirect',
\t\t\t\twindow.location.href.replace(
\t\t\t\t\t/\\/[^\\/]*\$/,
\t\t\t\t\t'/cors/result.html?%s'
\t\t\t\t)
\t\t\t);

\t\t\t// Load existing files:
\t\t\t\$('#fileupload').addClass('fileupload-processing');
\t\t\t\$.ajax({
\t\t\t\t// Uncomment the following to send cross-domain cookies:
\t\t\t\t//xhrFields: {withCredentials: true},
\t\t\t\turl: \$('#fileupload').fileupload('option', 'url'),
\t\t\t\tdataType: 'json',
\t\t\t\tcontext: \$('#fileupload')[0]
\t\t\t}).always(function () {
\t\t\t\t\$(this).removeClass('fileupload-processing');
\t\t\t}).done(function (result) {
\t\t\t\t\$(this).fileupload('option', 'done')
\t\t\t\t\t.call(this, \$.Event('done'), {result: result});
\t\t\t});
\t\t});
\t</script>
\t
\t<!-- The XDomainRequest Transport is included for cross-domain file deletion for IE 8 and IE 9 -->
\t<!--[if (gte IE 8)&(lt IE 10)]>
\t";
        // line 284
        echo Asset::js("jquery-fileupload/cors/jquery.xdr-transport.js");
        echo "
\t<![endif]-->
";
    }

    public function getTemplateName()
    {
        return "jquery_doc_uploader.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  431 => 284,  385 => 241,  379 => 238,  373 => 235,  367 => 232,  361 => 229,  355 => 226,  349 => 223,  343 => 220,  337 => 217,  331 => 214,  325 => 211,  319 => 208,  313 => 205,  267 => 201,  229 => 157,  222 => 123,  218 => 122,  211 => 119,  208 => 118,  153 => 66,  148 => 64,  140 => 59,  131 => 52,  125 => 49,  121 => 47,  119 => 46,  116 => 45,  110 => 42,  106 => 40,  104 => 39,  101 => 38,  98 => 37,  90 => 32,  85 => 30,  82 => 29,  77 => 26,  74 => 25,  70 => 22,  67 => 21,  61 => 18,  57 => 17,  51 => 14,  47 => 13,  34 => 4,  31 => 3,);
    }
}
