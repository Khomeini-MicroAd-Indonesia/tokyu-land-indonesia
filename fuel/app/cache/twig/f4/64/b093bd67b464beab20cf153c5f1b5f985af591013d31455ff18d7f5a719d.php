<?php

/* news.twig */
class __TwigTemplate_f464b093bd67b464beab20cf153c5f1b5f985af591013d31455ff18d7f5a719d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("pages/views/template_frontend.twig");

        $this->blocks = array(
            'frontend_content' => array($this, 'block_frontend_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "pages/views/template_frontend.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_frontend_content($context, array $blocks = array())
    {
        // line 4
        echo "    <div class=\"tokyuland-news\">
    <div class=\"row\">
        <ul class=\"breadcrumbs\">
            <li><a href=\"";
        // line 7
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/home\">Home</a></li>
            <li class=\"current\">News Release</li>
        </ul>
    </div>
    <div class=\"row\">
        <h1>NEWS RELEASE</h1>
    </div>
    <div class=\"row\">
        <div class=\"small-3 columns\">
            <ul class=\"side-nav\">
                ";
        // line 17
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["news_data"]) ? $context["news_data"] : null));
        foreach ($context['_seq'] as $context["news_year"] => $context["news_item"]) {
            // line 18
            echo "                    ";
            $context["news_year_url"] = (((Uri::base() . (isset($context["current_lang"]) ? $context["current_lang"] : null)) . "/news/archive/") . (isset($context["news_year"]) ? $context["news_year"] : null));
            // line 19
            echo "                    ";
            if (((isset($context["news_year"]) ? $context["news_year"] : null) == (isset($context["news_latest_year"]) ? $context["news_latest_year"] : null))) {
                // line 20
                echo "                        ";
                $context["news_year_url"] = ((Uri::base() . (isset($context["current_lang"]) ? $context["current_lang"] : null)) . "/news");
                // line 21
                echo "                    ";
            }
            // line 22
            echo "                    <li>
                        <a href=\"";
            // line 23
            echo ((((isset($context["news_year"]) ? $context["news_year"] : null) == (isset($context["news_selected_year"]) ? $context["news_selected_year"] : null))) ? ("javascript:") : ((isset($context["news_year_url"]) ? $context["news_year_url"] : null)));
            echo "\" class=\"";
            echo ((((isset($context["news_year"]) ? $context["news_year"] : null) == (isset($context["news_selected_year"]) ? $context["news_selected_year"] : null))) ? ("active") : (""));
            echo "\">
                            ";
            // line 24
            echo (isset($context["news_year"]) ? $context["news_year"] : null);
            echo "
                        </a>
                    </li>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['news_year'], $context['news_item'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 28
        echo "            </ul>
        </div>
        <div class=\"small-9 columns\">
            ";
        // line 31
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["news_data"]) ? $context["news_data"] : null));
        foreach ($context['_seq'] as $context["news_year"] => $context["news_item"]) {
            if (((isset($context["news_year"]) ? $context["news_year"] : null) == (isset($context["news_selected_year"]) ? $context["news_selected_year"] : null))) {
                // line 32
                echo "                ";
                $context['_parent'] = (array) $context;
                $context['_seq'] = twig_ensure_traversable((isset($context["news_item"]) ? $context["news_item"] : null));
                foreach ($context['_seq'] as $context["_key"] => $context["_item"]) {
                    // line 33
                    echo "                <div class=\"news-list\">
                    <div class=\"small-3 columns\">
                        ";
                    // line 35
                    echo $this->getAttribute((isset($context["_item"]) ? $context["_item"] : null), "date");
                    echo "
                        <hr/>
                    </div>
                    <div class=\"small-9 columns\">
                        <a href=\"";
                    // line 39
                    echo Uri::base();
                    echo "media/news/doc/";
                    echo $this->getAttribute((isset($context["_item"]) ? $context["_item"] : null), "doc_filename");
                    echo "\" target=\"_blank\">
                            ";
                    // line 40
                    echo $this->getAttribute((isset($context["_item"]) ? $context["_item"] : null), "doc");
                    echo "
                        </a>
                    </div>
                    <div style=\"clear: both;\"></div>
                </div>
                ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['_item'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 46
                echo "            ";
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['news_year'], $context['news_item'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 47
        echo "        </div>
    </div>
    <div style=\"height: 50px;\"></div>
    </div>
";
    }

    public function getTemplateName()
    {
        return "news.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  136 => 47,  129 => 46,  117 => 40,  111 => 39,  104 => 35,  100 => 33,  95 => 32,  90 => 31,  85 => 28,  75 => 24,  69 => 23,  66 => 22,  63 => 21,  60 => 20,  57 => 19,  54 => 18,  50 => 17,  36 => 7,  31 => 4,  28 => 3,);
    }
}
