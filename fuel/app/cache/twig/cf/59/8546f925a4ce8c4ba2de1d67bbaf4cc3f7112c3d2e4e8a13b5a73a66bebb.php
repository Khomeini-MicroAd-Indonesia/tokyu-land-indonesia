<?php

/* pages/views/template_frontend.twig */
class __TwigTemplate_cf598546f925a4ce8c4ba2de1d67bbaf4cc3f7112c3d2e4e8a13b5a73a66bebb extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'fb_head_prefix' => array($this, 'block_fb_head_prefix'),
            'fb_meta_data' => array($this, 'block_fb_meta_data'),
            'frontend_css' => array($this, 'block_frontend_css'),
            'fb_js_sdk' => array($this, 'block_fb_js_sdk'),
            'frontend_content' => array($this, 'block_frontend_content'),
            'frontend_js' => array($this, 'block_frontend_js'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html class=\"no-js\" lang=\"en\">
<head ";
        // line 3
        $this->displayBlock('fb_head_prefix', $context, $blocks);
        echo ">
    <base href=\"";
        // line 4
        echo Uri::base();
        echo "\" />

    ";
        // line 6
        $this->displayBlock('fb_meta_data', $context, $blocks);
        // line 7
        echo "
    <meta charset=\"utf-8\" />
    <meta name=\"viewport\" content=\"width=1100, initial-scale=1.0\" />

    <title>";
        // line 11
        echo (isset($context["meta_title"]) ? $context["meta_title"] : null);
        echo "</title>
    <link rel=\"icon\" type=\"image/ico\" href=\"";
        // line 12
        echo Uri::base();
        echo "assets/img/favicon.png\" />
    <meta name=\"description\" content=\"";
        // line 13
        echo (isset($context["meta_desc"]) ? $context["meta_desc"] : null);
        echo "\">

    ";
        // line 15
        $this->displayBlock('frontend_css', $context, $blocks);
        // line 21
        echo "    
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', '";
        // line 28
        echo Config::get("config_basic.google_analytic_code");
        echo "', 'auto');
        ga('send', 'pageview');
    </script>
</head>
<body>
    ";
        // line 33
        $this->displayBlock('fb_js_sdk', $context, $blocks);
        // line 34
        echo "    <div class=\"header\">
        <div class=\"row\" style=\"position: relative; width: 1024px;\">
            <div class=\"season-image\" id=\"video-tag\" style=\"display:none;\">
               ";
        // line 37
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["banner"]) ? $context["banner"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["filename"]) {
            // line 38
            echo "                    <video id=\"videoId\" poster=\"";
            echo Uri::base();
            echo "assets/img/poster.gif\" width=\"1022\" height=\"280\" autoplay loop>
                        <source src=\"";
            // line 39
            echo Uri::base();
            echo "media/banner/";
            echo (isset($context["filename"]) ? $context["filename"] : null);
            echo "\" type=\"video/mp4\">
                        Your browser does not support the video tag.
                    </video>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['filename'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 43
        echo "            </div>
            
        </div>
        <div class=\"row top-web\">
            <div class=\"large-4 columns title-web\">
                Hospitality Quality of Japan
            </div>
            <div class=\"large-8 columns\">
                <ul class=\"top-menu inline-list\">
                    <li class=\"select-lang\">
                        <select onchange=\"location = this.options[this.selectedIndex].value;\">
                            ";
        // line 54
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["valid_lang"]) ? $context["valid_lang"] : null));
        foreach ($context['_seq'] as $context["lang_key"] => $context["lang_item"]) {
            // line 55
            echo "                                <option value=\"";
            echo $this->getAttribute((isset($context["lang_item"]) ? $context["lang_item"] : null), "link");
            echo "\" ";
            echo ((((isset($context["lang_key"]) ? $context["lang_key"] : null) == (isset($context["current_lang"]) ? $context["current_lang"] : null))) ? ("selected") : (""));
            echo ">";
            echo $this->getAttribute((isset($context["lang_item"]) ? $context["lang_item"] : null), "label");
            echo "</option>
                            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['lang_key'], $context['lang_item'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 57
        echo "                        </select>
                    </li>
                </ul>
            </div>
            <div style=\"clear: both\"></div>
        </div>
        <div class=\"row\">
            <div class=\"logo\">
                <a href=\"";
        // line 65
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/home\">
                    <img src=\"";
        // line 66
        echo Uri::base();
        echo "/assets/css/images/logo.png\"/>
                </a>
            </div>
        </div>
        <div class=\"row menu-container\">
            <ul class=\"inline-list bottom main-menu\">
                ";
        // line 72
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["frontend_menus"]) ? $context["frontend_menus"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["menu"]) {
            // line 73
            echo "                    <li>
                        <a href=\"";
            // line 74
            echo $this->getAttribute((isset($context["menu"]) ? $context["menu"] : null), "route");
            echo "\" class=\"";
            echo $this->getAttribute((isset($context["menu"]) ? $context["menu"] : null), "menu_class_a");
            echo "\">";
            echo $this->getAttribute((isset($context["menu"]) ? $context["menu"] : null), "label");
            echo "</a>
                    </li>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['menu'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 77
        echo "            </ul>
        </div>
    </div>
            
    ";
        // line 81
        $this->displayBlock('frontend_content', $context, $blocks);
        // line 82
        echo "    <div class=\"footer\">
        <div class=\"row\">
            <img src=\"";
        // line 84
        echo Uri::base();
        echo "/assets/css/images/border-footer.jpg\"/>
        </div>
        <div class=\"row footer-menu\">
            <div style=\"height: 50px;\"></div>
            <div class=\"footer-home\">
                <a href=\"";
        // line 89
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/home\">HOME</a>
                <hr/>
            </div>

            <div style=\"height: 20px;\"></div>
            <div class=\"footer-home\">
                <ul class=\"inline-list\">
                    <li><a href=\"";
        // line 96
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/philosophy\">Philosophy</a><hr/></li>
                    <li><a href=\"";
        // line 97
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/project\">Project</a><hr/>
                        <ul class=\"no-bullet\">
                            <li><a href=\"";
        // line 99
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/project\">TLID Projects</a></li>
                        </ul>
                    </li>
                    <li><a href=\"";
        // line 102
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/about-group\">About Group</a><hr/>
                        <ul class=\"no-bullet\">
                            <li><a href=\"";
        // line 104
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/about-group\">Tokyu Fudosan Holdings</a></li>
                        </ul>
                    </li>
                    <li><a href=\"";
        // line 107
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/about-us/president\">About Us</a><hr/>
                        <ul class=\"no-bullet\">
                            <li><a href=\"";
        // line 109
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/about-us/president\">President Director’s message</a></li>
                            <li><a href=\"";
        // line 110
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/about-us/history\">History in Indonesia</a></li>
                            <li><a href=\"";
        // line 111
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/about-us/business\">Business Deployment</a></li>
                            <li><a href=\"";
        // line 112
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/about-us/company\">Company Profile</a></li>
                        </ul>
                    </li>
                    <li><a href=\"";
        // line 115
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/csr\">CSR</a><hr/></li>
                    <li><a href=\"";
        // line 116
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/career\">Career</a><hr/></li>
                    <li><a href=\"";
        // line 117
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/news\">News Release</a><hr/></li>
                    <li><a href=\"";
        // line 118
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/contact-us\">Contact Us</a><hr/></li>
                </ul>
            </div>
            <div class=\"footer-foot\">
                <p ></p>
                <ul class=\"inline-list\" style=\"font-size: 10px; padding-top: 20px; font-family: helvetica;\">
                    <li>Copyright ©2015 TOKYU LAND INDONESIA | All rights reserved</li>
                    <li><strong><a style=\"color: #000000; font-family: 'MS PMincho';\" href=\"";
        // line 125
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/privacy\">Privacy Policy</a></strong></li>
                    <li>|</li>
                    <li><strong><a style=\"color: #000000; font-family: 'MS PMincho'; \" href=\"";
        // line 127
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/terms\">Terms &amp Conditions</a></strong></li>
                </ul>
            </div>
        </div>
    </div>
    ";
        // line 132
        $this->displayBlock('frontend_js', $context, $blocks);
        // line 136
        echo "    <script type=\"text/javascript\">
        \$(document).foundation();
    </script>
    <script type=\"text/javascript\">
        var video = document.getElementById(\"videoId\");
        var videoTag = document.getElementById(\"video-tag\");
//        video.addEventListener(\"loadedmetadata\", function() {
//        video.addEventListener(\"loadeddata\", function() {
        video.addEventListener(\"canplaythrough\", function() {
            videoTag.style.display = \"block\";
        }, false);

        setTimeout(\"displayBlock()\", 5000);

        function displayBlock(){
            if(videoTag.style.display == 'none'){
                videoTag.style.display = \"block\";
            }
        }

    </script>

</body>
</html>
";
    }

    // line 3
    public function block_fb_head_prefix($context, array $blocks = array())
    {
    }

    // line 6
    public function block_fb_meta_data($context, array $blocks = array())
    {
    }

    // line 15
    public function block_frontend_css($context, array $blocks = array())
    {
        // line 16
        echo "        ";
        echo Asset::css("normalize.css");
        echo "
        ";
        // line 17
        echo Asset::css("foundation.min.css");
        echo "
        ";
        // line 18
        echo Asset::css("font-awesome.min.css");
        echo "
        ";
        // line 19
        echo Asset::css("style.css");
        echo "
    ";
    }

    // line 33
    public function block_fb_js_sdk($context, array $blocks = array())
    {
    }

    // line 81
    public function block_frontend_content($context, array $blocks = array())
    {
    }

    // line 132
    public function block_frontend_js($context, array $blocks = array())
    {
        // line 133
        echo "        ";
        echo Asset::js("jquery.min.js");
        echo "
        ";
        // line 134
        echo Asset::js("foundation.min.js");
        echo "
    ";
    }

    public function getTemplateName()
    {
        return "pages/views/template_frontend.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  391 => 134,  386 => 133,  383 => 132,  378 => 81,  373 => 33,  367 => 19,  363 => 18,  359 => 17,  354 => 16,  351 => 15,  346 => 6,  341 => 3,  313 => 136,  311 => 132,  302 => 127,  296 => 125,  285 => 118,  280 => 117,  275 => 116,  270 => 115,  263 => 112,  258 => 111,  253 => 110,  248 => 109,  242 => 107,  235 => 104,  229 => 102,  222 => 99,  216 => 97,  200 => 89,  192 => 84,  188 => 82,  186 => 81,  180 => 77,  167 => 74,  164 => 73,  160 => 72,  151 => 66,  136 => 57,  123 => 55,  119 => 54,  94 => 39,  85 => 37,  78 => 33,  70 => 28,  61 => 21,  54 => 13,  50 => 12,  46 => 11,  40 => 7,  33 => 4,  25 => 1,  215 => 85,  211 => 96,  206 => 83,  203 => 82,  194 => 75,  178 => 69,  174 => 68,  170 => 67,  161 => 63,  155 => 62,  150 => 59,  146 => 65,  138 => 54,  129 => 47,  114 => 40,  110 => 39,  106 => 43,  102 => 37,  93 => 33,  89 => 38,  84 => 29,  80 => 34,  71 => 23,  59 => 15,  56 => 13,  45 => 10,  42 => 9,  38 => 6,  32 => 4,  29 => 3,);
    }
}
