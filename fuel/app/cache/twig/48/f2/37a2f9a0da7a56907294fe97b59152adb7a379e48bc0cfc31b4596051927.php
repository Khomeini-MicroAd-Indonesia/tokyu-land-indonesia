<?php

/* contact.twig */
class __TwigTemplate_48f237a2f9a0da7a56907294fe97b59152adb7a379e48bc0cfc31b4596051927 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("pages/views/template_frontend.twig");

        $this->blocks = array(
            'frontend_content' => array($this, 'block_frontend_content'),
            'frontend_js' => array($this, 'block_frontend_js'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "pages/views/template_frontend.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_frontend_content($context, array $blocks = array())
    {
        // line 4
        echo "    <div class=\"tokyuland-contact\">
        <div class=\"row\">
            <ul class=\"breadcrumbs\">
                <li><a href=\"";
        // line 7
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "\">Home</a></li>
                <li class=\"current\">Contact Us</li>
            </ul>
        </div>
        <div class=\"row\">
            <div class=\"small-5 columns\">
                <img src=\"";
        // line 13
        echo Uri::base();
        echo "/assets/css/images/contact-sakura.jpg\"/>
            </div>
            <div class=\"small-7 columns\">
                <h1>CONTACT US</h1>
                <div>
                    <p>
                        PT. Tokyo Land Indonesia
                    </p>
                    <p>
                        Menara Cakrawala (Skyline Building 9F)<br>
                        Jl. M.H. Thamrin No. 9<br>
                        Jakarta 10340<br>
                        <hr>
                    </p>
                    <p>
                        <img src=\"";
        // line 28
        echo Uri::base();
        echo "/assets/css/images/telp-icon.png\"/> +62 21 3192 0109<br>
                        <img src=\"";
        // line 29
        echo Uri::base();
        echo "/assets/css/images/mail-icon.png\"/> contact@tokyu-land.co.id<br>
                    </p>
                </div>
                <div class=\"contact-form\">
                    <form action=\"\" method=\"post\" data-abide novalidate=\"novalidate\">
                        <div>
                            <div>
                                <div class=\"small-12 columns\">
                                    <input id=\"contact-name\" placeholder=\"Your Name *\" type=\"text\" required pattern=\"[a-zA-Z]+\" />
                                    <small class=\"error\"><img src=\"";
        // line 38
        echo Uri::base();
        echo "/assets/css/images/warning-symbol.png\"/>The  data is wrong / blank</small>
                                </div>
                                <div style=\"clear: both;\"></div>
                            </div>
                            <div>
                                <div class=\"small-12 columns\">
                                    <input id=\"contact-email\" placeholder=\"Your E-mail *\" type=\"email\" required />
                                    <small class=\"error\"><img src=\"";
        // line 45
        echo Uri::base();
        echo "/assets/css/images/warning-symbol.png\"/> The  data is wrong / blank</small>
                                </div>
                                <div style=\"clear: both;\"></div>
                            </div>
                            <div>
                                <div class=\"small-12 columns\">
                                    <input id=\"contact-number\" placeholder=\"Your Phone Number *\" type=\"text\" required pattern=\"[-+]?[1-9]\\d*\" />
                                    <small class=\"error\"><img src=\"";
        // line 52
        echo Uri::base();
        echo "/assets/css/images/warning-symbol.png\"/>  The  data is wrong / blank</small>
                                </div>
                                <div style=\"clear: both;\"></div>
                            </div>
                            <div>
                                <div class=\"small-12 columns\">

                                    <input id=\"contact-address\" placeholder=\"Address *\" type=\"text\"  />

                                    <small class=\"error\"><img src=\"";
        // line 61
        echo Uri::base();
        echo "/assets/css/images/warning-symbol.png\"/>  The  data is wrong / blank</small>
                                </div>
                                <div style=\"clear: both;\"></div>
                            </div>
                            <div>
                                <div class=\"small-6 columns\">
                                    <input type=\"radio\" id=\"request\" name=\"brochure\" value=\"Request for Brochure\" checked> <label for=\"request\">Request for Brochure</label>
                                    <input type=\"radio\" id=\"no-request\" name=\"brochure\" value=\"No Brochure\"> <label for=\"no-request\">No Brochure</label>
                                </div>
                                <div class=\"small-6 columns\">

                                </div>
                                <div style=\"clear: both;\"></div>
                            </div>
                            <div>
                                <div class=\"small-12 columns\">
                                    <textarea id=\"contact-msg\" placeholder=\"Your Message\"></textarea>
                                    <div style=\"font-size: 12px; padding-bottom: 10px;\">* Please make sure all the column fiiled in properly</div>
                                    <div></div>
                                </div>
                                <div style=\"clear: both;\"></div>
                            </div>
                            <input type=\"submit\" value=\"Submit\" class=\"button\" onclick=\"submitContact()\"/>
                            <div id=\"thanks\"></div>
                        </div>
                    </form>
                </div>
            </div>
            <div style=\"clear: both;\"></div>
            <div style=\"height: 20px;\"></div>
        </div>
    </div>

    <div style=\"height: 50px;\"></div>
   
";
    }

    // line 98
    public function block_frontend_js($context, array $blocks = array())
    {
        // line 99
        echo "    ";
        $this->displayParentBlock("frontend_js", $context, $blocks);
        echo "
    ";
        // line 100
        echo Asset::js("foundation/foundation.abide.js");
        echo "

    <script>

        \$(\"#no-request\").click(function() {
            \$(\"#contact-address\").prop({ readonly:true });
        });
        \$(\"#request\").click(function() {
            \$(\"#contact-address\").prop({ readonly:false });
        });

    function submitContact(){
        
        
        var name = document.getElementById('contact-name').value;
        var mail = document.getElementById('contact-email').value;
        var num = document.getElementById('contact-number').value;
        var addr = document.getElementById('contact-address').value;
        var msg = document.getElementById('contact-msg').value;
        var stat;
        
        if(document.getElementById(\"request\").checked == true){
            stat = \"requesting a brochure\";
        }else if(document.getElementById(\"no-request\").checked == true){
            stat = \"don\\'t request any brochure\";
        }

        \$.ajax({
            type: \"POST\",
            url: \"";
        // line 129
        echo Uri::base();
        echo "get-submit-contact\", // 
            data: { 
                'name':name,
                'mail':mail,
                'num':num,
                'addr':addr,
                'msg':msg,
                'stat':stat,
            },
            success: function(){
                window.location ='";
        // line 139
        echo (Uri::base() . (isset($context["current_lang"]) ? $context["current_lang"] : null));
        echo "/contact-us/thanks'
            }
                 
        });
    }
        
    </script>    
    
    
";
    }

    public function getTemplateName()
    {
        return "contact.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  206 => 139,  193 => 129,  161 => 100,  156 => 99,  153 => 98,  113 => 61,  101 => 52,  91 => 45,  81 => 38,  69 => 29,  65 => 28,  47 => 13,  37 => 7,  32 => 4,  29 => 3,);
    }
}
