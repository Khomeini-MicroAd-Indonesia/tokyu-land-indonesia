<?php

/* pages/views/template_frontend.twig */
class __TwigTemplate_f54aacb68f3d46bc0cab4ed9f5f84a9e156854ecbd37950bbfc66563e9d787a3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'fb_head_prefix' => array($this, 'block_fb_head_prefix'),
            'fb_meta_data' => array($this, 'block_fb_meta_data'),
            'frontend_css' => array($this, 'block_frontend_css'),
            'fb_js_sdk' => array($this, 'block_fb_js_sdk'),
            'frontend_content' => array($this, 'block_frontend_content'),
            'frontend_js' => array($this, 'block_frontend_js'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html class=\"no-js\" lang=\"en\">
<head ";
        // line 3
        $this->displayBlock('fb_head_prefix', $context, $blocks);
        echo ">
    <base href=\"";
        // line 4
        echo Uri::base();
        echo "\" />

    ";
        // line 6
        $this->displayBlock('fb_meta_data', $context, $blocks);
        // line 7
        echo "
    <meta charset=\"utf-8\" />
    <meta name=\"viewport\" content=\"width=1100, initial-scale=1.0\" />

    <title>";
        // line 11
        echo (isset($context["meta_title"]) ? $context["meta_title"] : null);
        echo "</title>
    <link rel=\"icon\" type=\"image/ico\" href=\"";
        // line 12
        echo Uri::base();
        echo "assets/img/favicon.png\" />
    <meta name=\"description\" content=\"";
        // line 13
        echo (isset($context["meta_desc"]) ? $context["meta_desc"] : null);
        echo "\">

    ";
        // line 15
        $this->displayBlock('frontend_css', $context, $blocks);
        // line 21
        echo "    
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', '";
        // line 28
        echo Config::get("config_basic.google_analytic_code");
        echo "', 'auto');
        ga('send', 'pageview');
    </script>
</head>
<body>
    ";
        // line 33
        $this->displayBlock('fb_js_sdk', $context, $blocks);
        // line 34
        echo "    <div class=\"header\">
        <div class=\"row\" style=\"position: relative; width: 1022px;\">
            <div class=\"season-image\" id=\"video-tag\" style=\"display:none;\">
               ";
        // line 37
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["banner"]) ? $context["banner"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["filename"]) {
            // line 38
            echo "                    <video id=\"videoId\" poster=\"";
            echo Uri::base();
            echo "assets/img/poster_summer.gif\" width=\"1022\" height=\"280\" autoplay loop>
                        <source src=\"";
            // line 39
            echo Uri::base();
            echo "media/banner/";
            echo (isset($context["filename"]) ? $context["filename"] : null);
            echo "\" type=\"video/mp4\">
                        <source src=\"";
            // line 40
            echo Uri::base();
            echo "media/banner/fishpond_10.ogg\" type=\"video/ogg\">
                        Your browser does not support the video tag.
                    </video>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['filename'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 44
        echo "            </div>
            
        </div>
        <div class=\"row top-web\">
            <div class=\"large-4 columns title-web\">
                Hospitality Quality of Japan
            </div>
            <div class=\"large-8 columns\">
                <ul class=\"top-menu inline-list\">
                    <li class=\"select-lang\">
                        <select onchange=\"location = this.options[this.selectedIndex].value;\">
                            ";
        // line 55
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["valid_lang"]) ? $context["valid_lang"] : null));
        foreach ($context['_seq'] as $context["lang_key"] => $context["lang_item"]) {
            // line 56
            echo "                                <option value=\"";
            echo $this->getAttribute((isset($context["lang_item"]) ? $context["lang_item"] : null), "link");
            echo "\" ";
            echo ((((isset($context["lang_key"]) ? $context["lang_key"] : null) == (isset($context["current_lang"]) ? $context["current_lang"] : null))) ? ("selected") : (""));
            echo ">";
            echo $this->getAttribute((isset($context["lang_item"]) ? $context["lang_item"] : null), "label");
            echo "</option>
                            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['lang_key'], $context['lang_item'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 58
        echo "                        </select>
                    </li>
                </ul>
            </div>
            <div style=\"clear: both\"></div>
        </div>
        <div class=\"row\">
            <div class=\"logo\">
                <a href=\"";
        // line 66
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/home\">
                    <img src=\"";
        // line 67
        echo Uri::base();
        echo "/assets/css/images/logo.png\"/>
                </a>
            </div>
        </div>
        <div class=\"row menu-container\">
            <ul class=\"inline-list bottom main-menu\">
                ";
        // line 73
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["frontend_menus"]) ? $context["frontend_menus"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["menu"]) {
            // line 74
            echo "                    <li>
                        <a href=\"";
            // line 75
            echo $this->getAttribute((isset($context["menu"]) ? $context["menu"] : null), "route");
            echo "\" class=\"";
            echo $this->getAttribute((isset($context["menu"]) ? $context["menu"] : null), "menu_class_a");
            echo "\">";
            echo $this->getAttribute((isset($context["menu"]) ? $context["menu"] : null), "label");
            echo "</a>
                    </li>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['menu'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 78
        echo "            </ul>
        </div>
    </div>
            
    ";
        // line 82
        $this->displayBlock('frontend_content', $context, $blocks);
        // line 83
        echo "    <div class=\"footer\">
        <div class=\"row\">
            <img src=\"";
        // line 85
        echo Uri::base();
        echo "/assets/css/images/border-footer.jpg\"/>
        </div>
        <div class=\"row footer-menu\">
            <div style=\"height: 50px;\"></div>
            <div class=\"footer-home\">
                <a href=\"";
        // line 90
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/home\">HOME</a>
                <hr/>
            </div>

            <div style=\"height: 20px;\"></div>
            <div class=\"footer-home\">
                <ul class=\"inline-list\">
                    <li><a href=\"";
        // line 97
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/philosophy\">Philosophy</a><hr/></li>
                    <li><a href=\"";
        // line 98
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/project\">Project</a><hr/>
                        <ul class=\"no-bullet\">
                            <li><a href=\"";
        // line 100
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/project\">TLID Projects</a></li>
                        </ul>
                    </li>
                    <li><a href=\"";
        // line 103
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/about-group\">About Group</a><hr/>
                        <ul class=\"no-bullet\">
                            <li><a href=\"";
        // line 105
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/about-group\">Tokyu Fudosan Holdings</a></li>
                        </ul>
                    </li>
                    <li><a href=\"";
        // line 108
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/about-us/president\">About Us</a><hr/>
                        <ul class=\"no-bullet\">
                            <li><a href=\"";
        // line 110
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/about-us/president\">President Director’s message</a></li>
                            <li><a href=\"";
        // line 111
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/about-us/history\">History in Indonesia</a></li>
                            <li><a href=\"";
        // line 112
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/about-us/business\">Business Deployment</a></li>
                            <li><a href=\"";
        // line 113
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/about-us/company\">Company Profile</a></li>
                        </ul>
                    </li>
                    <li><a href=\"";
        // line 116
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/csr\">CSR</a><hr/></li>
                    <li><a href=\"";
        // line 117
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/career\">Career</a><hr/></li>
                    <li><a href=\"";
        // line 118
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/news\">News Release</a><hr/></li>
                    <li><a href=\"";
        // line 119
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/contact-us\">Contact Us</a><hr/></li>
                </ul>
            </div>
            <div class=\"footer-foot\">
                <p ></p>
                <ul class=\"inline-list\" style=\"font-size: 10px; padding-top: 20px; font-family: helvetica;\">
                    <li>Copyright ©2015 TOKYU LAND INDONESIA | All rights reserved</li>
                    <li><strong><a style=\"color: #000000; font-family: 'MS PMincho';\" href=\"";
        // line 126
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/privacy\">Privacy Policy</a></strong></li>
                    <li>|</li>
                    <li><strong><a style=\"color: #000000; font-family: 'MS PMincho'; \" href=\"";
        // line 128
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/terms\">Terms &amp Conditions</a></strong></li>
                </ul>
            </div>
        </div>
    </div>
    ";
        // line 133
        $this->displayBlock('frontend_js', $context, $blocks);
        // line 137
        echo "    <script type=\"text/javascript\">
        \$(document).foundation();
    </script>
    <script type=\"text/javascript\">
        var video = document.getElementById(\"videoId\");
        var videoTag = document.getElementById(\"video-tag\");
//        video.addEventListener(\"loadedmetadata\", function() {
//        video.addEventListener(\"loadeddata\", function() {
        video.addEventListener(\"canplaythrough\", function() {
            videoTag.style.display = \"block\";
        }, false);

        setTimeout(\"displayBlock()\", 5000);

        function displayBlock(){
            if(videoTag.style.display == 'none'){
                videoTag.style.display = \"block\";
            }
        }

    </script>

</body>
</html>
";
    }

    // line 3
    public function block_fb_head_prefix($context, array $blocks = array())
    {
    }

    // line 6
    public function block_fb_meta_data($context, array $blocks = array())
    {
    }

    // line 15
    public function block_frontend_css($context, array $blocks = array())
    {
        // line 16
        echo "        ";
        echo Asset::css("normalize.css");
        echo "
        ";
        // line 17
        echo Asset::css("foundation.min.css");
        echo "
        ";
        // line 18
        echo Asset::css("font-awesome.min.css");
        echo "
        ";
        // line 19
        echo Asset::css("style.css");
        echo "
    ";
    }

    // line 33
    public function block_fb_js_sdk($context, array $blocks = array())
    {
    }

    // line 82
    public function block_frontend_content($context, array $blocks = array())
    {
    }

    // line 133
    public function block_frontend_js($context, array $blocks = array())
    {
        // line 134
        echo "        ";
        echo Asset::js("jquery.min.js");
        echo "
        ";
        // line 135
        echo Asset::js("foundation.min.js");
        echo "
    ";
    }

    public function getTemplateName()
    {
        return "pages/views/template_frontend.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  395 => 135,  390 => 134,  387 => 133,  382 => 82,  377 => 33,  371 => 19,  367 => 18,  363 => 17,  358 => 16,  355 => 15,  350 => 6,  345 => 3,  317 => 137,  315 => 133,  306 => 128,  300 => 126,  289 => 119,  284 => 118,  279 => 117,  274 => 116,  267 => 113,  262 => 112,  257 => 111,  252 => 110,  246 => 108,  239 => 105,  233 => 103,  226 => 100,  220 => 98,  215 => 97,  204 => 90,  196 => 85,  192 => 83,  190 => 82,  184 => 78,  171 => 75,  168 => 74,  164 => 73,  155 => 67,  150 => 66,  140 => 58,  127 => 56,  123 => 55,  110 => 44,  100 => 40,  94 => 39,  89 => 38,  85 => 37,  80 => 34,  78 => 33,  70 => 28,  61 => 21,  59 => 15,  54 => 13,  50 => 12,  46 => 11,  40 => 7,  38 => 6,  33 => 4,  29 => 3,  25 => 1,);
    }
}
