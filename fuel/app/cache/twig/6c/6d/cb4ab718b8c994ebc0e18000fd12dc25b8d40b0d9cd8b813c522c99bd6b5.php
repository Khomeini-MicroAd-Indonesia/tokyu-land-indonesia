<?php

/* career.twig */
class __TwigTemplate_6c6dcb4ab718b8c994ebc0e18000fd12dc25b8d40b0d9cd8b813c522c99bd6b5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("pages/views/template_frontend.twig");

        $this->blocks = array(
            'frontend_content' => array($this, 'block_frontend_content'),
            'frontend_js' => array($this, 'block_frontend_js'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "pages/views/template_frontend.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_frontend_content($context, array $blocks = array())
    {
        // line 4
        echo "    <div class=\"tokyuland-career\">
        <div class=\"row\">
            <ul class=\"breadcrumbs\">
                <li><a href=\"";
        // line 7
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/home\">Home</a></li>
                <li class=\"current\">Career</li>
            </ul>
        </div>
        <div class=\"row\">
            <h1>Career</h1>
        </div>
        <div class=\"row\">
            <h3>Career Opportunities in PT.Tokyu Land Indonesia</h3>
            <p>
                We are a foreign investment company engaging in property developer business and we aim to expand our business growth in Indonesia.<br/>
                We are always looking for talented people to join us and invite highly committed individuals to strengthen our team.<br/>
            </p>
        </div>
        <div class=\"row\">
            <h3>General Requirement</h3>
            <ul>
                <li>Hold a minimum Diploma Degree (2, 6), Bachelor Degree (1, 3, 4, 5, 7) in any major from reputable university.</li>
                <li>Familiar and experience in operating Microsoft Office program such as MS. Word, MS. Excel and MS. Power Point.</li>
                <li>Good written and verbal communication in English.</li>
                <li>Able to work in a dynamic working environment, highly motivated, hardworking, responsible, willing to learn, able to work under pressure.</li>
            </ul>
        </div>
        <div class=\"row\">
            <h4>Job Vacancy :</h4>
            <div>
                <ul class=\"accordion\" data-accordion>
                    ";
        // line 34
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["vacancy_data"]) ? $context["vacancy_data"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["vacancy"]) {
            // line 35
            echo "                    <li class=\"accordion-navigation\">
                        <a href=\"#panel-";
            // line 36
            echo $this->getAttribute((isset($context["vacancy"]) ? $context["vacancy"] : null), "id");
            echo "\">
                            <h5>";
            // line 37
            echo $this->getAttribute((isset($context["vacancy"]) ? $context["vacancy"] : null), "name");
            echo " (";
            echo $this->getAttribute((isset($context["vacancy"]) ? $context["vacancy"] : null), "code");
            echo ")</h5>
                            <p>Specific Requirement <i class=\"fa fa-play\"></i></p>
                        </a>
                        <div class=\"apply-now\">
                            <a href=\"";
            // line 41
            echo Uri::base();
            echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
            echo "/career/submit/";
            echo $this->getAttribute((isset($context["vacancy"]) ? $context["vacancy"] : null), "id");
            echo "\">
                                Apply Now
                            </a>
                        </div>
                        <div id=\"panel-";
            // line 45
            echo $this->getAttribute((isset($context["vacancy"]) ? $context["vacancy"] : null), "id");
            echo "\" class=\"content\">
                            ";
            // line 46
            echo $this->getAttribute((isset($context["vacancy"]) ? $context["vacancy"] : null), "req");
            echo "
                        </div>
                        <hr/>
                    </li>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['vacancy'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 51
        echo "                </ul>
            </div>
        </div>
        <div class=\"row foot-note\">
            If you are interested in working in challenging environment and have the key skill to fill these vacancies, please click “Apply Now” button<br/>
            or send us your comprehensive resume (maximum 500KB size) by quoting your name and your desired position (example : John-Sales Consultant) in the email subject to:<br/>
            <a href=\"mailto:hrga@tokyu-land.co.id\"><span>hrga@tokyu-land.co.id</span></a>
            <br/><br/>

            Only shortlisted candidates will be notified by us.
        </div>
   </div>
   <div style=\"height: 50px;\"></div>
";
    }

    // line 66
    public function block_frontend_js($context, array $blocks = array())
    {
        // line 67
        echo "        ";
        $this->displayParentBlock("frontend_js", $context, $blocks);
        echo "
        ";
        // line 68
        echo Asset::js("foundation/foundation.accordion.js");
        echo "
        <script>
            \$(\".accordion\").on(\"click\", \"li\", function (event) {
//                \$('i.fa', 'li.active').addClass('fa-down');
//                \$('li.active', '.accordion').find(\"i.fa\").removeClass(\"fa-down\");
                \$(\"p\").find(\".content\").slideToggle(\"slow\");
                if(!\$(this).hasClass(\"active\")) {
                    \$(this).find(\".content\").slideToggle(\"slow\");
                }

            });
        </script>
";
    }

    public function getTemplateName()
    {
        return "career.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  138 => 68,  133 => 67,  130 => 66,  113 => 51,  102 => 46,  98 => 45,  88 => 41,  79 => 37,  75 => 36,  72 => 35,  68 => 34,  37 => 7,  32 => 4,  29 => 3,);
    }
}
