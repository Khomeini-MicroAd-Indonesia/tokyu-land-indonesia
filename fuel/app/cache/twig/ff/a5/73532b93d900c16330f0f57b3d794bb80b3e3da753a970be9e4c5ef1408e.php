<?php

/* terms.twig */
class __TwigTemplate_ffa573532b93d900c16330f0f57b3d794bb80b3e3da753a970be9e4c5ef1408e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("pages/views/template_frontend.twig");

        $this->blocks = array(
            'frontend_content' => array($this, 'block_frontend_content'),
            'frontend_js' => array($this, 'block_frontend_js'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "pages/views/template_frontend.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_frontend_content($context, array $blocks = array())
    {
        // line 4
        echo "    <div class=\"tokyuland-philosophy\">
        <div class=\"row\">
            <ul class=\"breadcrumbs\">
                <li><a href=\"";
        // line 7
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/home\">Home</a></li>
                <li class=\"current\">Terms &amp; Conditions</li>
            </ul>
        </div>
        <div class=\"extra-content row\">
            <h1>Terms &amp; Conditions</h1>
            <div>


                ";
        // line 16
        echo Lang::get("terms");
        echo "

            </div>
        </div>
    </div>

    <div style=\"height: 50px;\"></div>
";
    }

    // line 25
    public function block_frontend_js($context, array $blocks = array())
    {
        // line 26
        echo "    ";
        $this->displayParentBlock("frontend_js", $context, $blocks);
        echo "
";
    }

    public function getTemplateName()
    {
        return "terms.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  65 => 26,  62 => 25,  50 => 16,  37 => 7,  32 => 4,  29 => 3,);
    }
}
