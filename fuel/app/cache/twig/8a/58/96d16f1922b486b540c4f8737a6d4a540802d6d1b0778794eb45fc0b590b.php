<?php

/* pages/views/mobile_template.twig */
class __TwigTemplate_8a5896d16f1922b486b540c4f8737a6d4a540802d6d1b0778794eb45fc0b590b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'fb_head_prefix' => array($this, 'block_fb_head_prefix'),
            'fb_meta_data' => array($this, 'block_fb_meta_data'),
            'frontend_css' => array($this, 'block_frontend_css'),
            'fb_js_sdk' => array($this, 'block_fb_js_sdk'),
            'frontend_content' => array($this, 'block_frontend_content'),
            'frontend_js' => array($this, 'block_frontend_js'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html class=\"no-js\" lang=\"en\">
<head ";
        // line 3
        $this->displayBlock('fb_head_prefix', $context, $blocks);
        echo ">
    <base href=\"";
        // line 4
        echo Uri::base();
        echo "\" />

    ";
        // line 6
        $this->displayBlock('fb_meta_data', $context, $blocks);
        // line 7
        echo "
    <meta charset=\"utf-8\" />
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\" />

    <title>";
        // line 11
        echo (isset($context["meta_title"]) ? $context["meta_title"] : null);
        echo "</title>
    <link rel=\"icon\" type=\"image/ico\" href=\"";
        // line 12
        echo Uri::base();
        echo "assets/img/favicon.png\" />
    <meta name=\"description\" content=\"";
        // line 13
        echo (isset($context["meta_desc"]) ? $context["meta_desc"] : null);
        echo "\">

    ";
        // line 15
        $this->displayBlock('frontend_css', $context, $blocks);
        // line 21
        echo "
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', '";
        // line 28
        echo Config::get("config_basic.google_analytic_code");
        echo "', 'auto');
        ga('send', 'pageview');
    </script>
</head>
<body>
";
        // line 33
        $this->displayBlock('fb_js_sdk', $context, $blocks);
        // line 34
        echo "<div class=\"header\">
    <div class=\"header-item\">
        <div class=\"banner-image\">
            <img src=\"";
        // line 37
        echo Uri::base();
        echo "/assets/css/images/banner-mobile.jpg\">
        </div>
        <div class=\"mobile-logo\">
            <a href=\"";
        // line 40
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/home\"><img src=\"";
        echo Uri::base();
        echo "/assets/css/images/logo.png\"/></a>
        </div>
    </div>
    <div class=\"contain-to-grid sticky\">

        <nav class=\"top-bar top-item \" data-topbar role=\"navigation\" data-options=\"scrolltop:true\">
            <ul class=\"title-area\">
                <li class=\"toggle-topbar menu-icon\"><a href=\"\"><span></span></a></li>
                <li class=\"name\">
                    ";
        // line 49
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["valid_lang"]) ? $context["valid_lang"] : null));
        foreach ($context['_seq'] as $context["lang_key"] => $context["lang_item"]) {
            // line 50
            echo "                        <div class=\"m-lang\">
                            <a href=\"";
            // line 51
            echo $this->getAttribute((isset($context["lang_item"]) ? $context["lang_item"] : null), "link");
            echo "\" class=\"";
            echo ((((isset($context["lang_key"]) ? $context["lang_key"] : null) == (isset($context["current_lang"]) ? $context["current_lang"] : null))) ? ("active") : (""));
            echo "\">";
            echo $this->getAttribute((isset($context["lang_item"]) ? $context["lang_item"] : null), "label_mobile");
            echo "</a>
                        </div>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['lang_key'], $context['lang_item'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 54
        echo "                </li>
                <!-- Remove the class \"menu-icon\" to get rid of menu icon. Take out \"Menu\" to just have icon alone -->
            </ul>

            <section class=\"top-bar-section\">

                <!-- Left Nav Section -->
                <ul class=\"left\">
                    <li><a href=\"";
        // line 62
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/project\">Project</a></li>
                    <li><a href=\"";
        // line 63
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/about-us/business\">About Us</a></li>
                    <li><a href=\"";
        // line 64
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/career\">Career</a></li>
                    <li><a href=\"";
        // line 65
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/news\">News Release</a></li>
                    <li><a href=\"";
        // line 66
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/contact-us\">Contact Us</a></li>
                    <li class=\"left-lang\">
                        ";
        // line 68
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["valid_lang"]) ? $context["valid_lang"] : null));
        foreach ($context['_seq'] as $context["lang_key"] => $context["lang_item"]) {
            // line 69
            echo "                            <div class=\"m-lang\">
                                <a href=\"";
            // line 70
            echo $this->getAttribute((isset($context["lang_item"]) ? $context["lang_item"] : null), "link");
            echo "\" class=\"";
            echo ((((isset($context["lang_key"]) ? $context["lang_key"] : null) == (isset($context["current_lang"]) ? $context["current_lang"] : null))) ? ("active") : (""));
            echo "\">";
            echo $this->getAttribute((isset($context["lang_item"]) ? $context["lang_item"] : null), "label_mobile");
            echo "</a>
                            </div>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['lang_key'], $context['lang_item'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 73
        echo "                    </li>
                </ul>
            </section>
        </nav>
        <div class=\"sticky-menu\">
            <a href=\"";
        // line 78
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/home\"><img src=\"";
        echo Uri::base();
        echo "/assets/css/images/sticky-image.jpg\"/></a>
        </div>
        <nav class=\"top-bar top-fixed\" data-topbar role=\"navigation\" data-options=\"scrolltop:false\">
            <ul class=\"title-area\">
                <li class=\"toggle-topbar menu-icon\"><a href=\"\"><span></span></a></li>
                <li class=\"name\">
                    ";
        // line 84
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["valid_lang"]) ? $context["valid_lang"] : null));
        foreach ($context['_seq'] as $context["lang_key"] => $context["lang_item"]) {
            // line 85
            echo "                        <div class=\"m-lang\">
                            <a href=\"";
            // line 86
            echo $this->getAttribute((isset($context["lang_item"]) ? $context["lang_item"] : null), "link");
            echo "\" class=\"";
            echo ((((isset($context["lang_key"]) ? $context["lang_key"] : null) == (isset($context["current_lang"]) ? $context["current_lang"] : null))) ? ("active") : (""));
            echo "\">";
            echo $this->getAttribute((isset($context["lang_item"]) ? $context["lang_item"] : null), "label_mobile");
            echo "</a>
                        </div>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['lang_key'], $context['lang_item'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 89
        echo "                </li>
                <!-- Remove the class \"menu-icon\" to get rid of menu icon. Take out \"Menu\" to just have icon alone -->
            </ul>

            <section class=\"top-bar-section \">

                <!-- Left Nav Section -->
                <ul class=\"left\">
                    <li><a href=\"";
        // line 97
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/project\">Project</a></li>
                    <li><a href=\"";
        // line 98
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/about-us/business\">About Us</a></li>
                    <li><a href=\"";
        // line 99
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/career\">Career</a></li>
                    <li><a href=\"";
        // line 100
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/news\">News Release</a></li>
                    <li><a href=\"";
        // line 101
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/contact-us\">Contact Us</a></li>
                    <li class=\"left-lang\">
                        ";
        // line 103
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["valid_lang"]) ? $context["valid_lang"] : null));
        foreach ($context['_seq'] as $context["lang_key"] => $context["lang_item"]) {
            // line 104
            echo "                            <div class=\"m-lang\">
                                <a href=\"";
            // line 105
            echo $this->getAttribute((isset($context["lang_item"]) ? $context["lang_item"] : null), "link");
            echo "\" class=\"";
            echo ((((isset($context["lang_key"]) ? $context["lang_key"] : null) == (isset($context["current_lang"]) ? $context["current_lang"] : null))) ? ("active") : (""));
            echo "\">";
            echo $this->getAttribute((isset($context["lang_item"]) ? $context["lang_item"] : null), "label_mobile");
            echo "</a>
                            </div>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['lang_key'], $context['lang_item'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 108
        echo "                    </li>
                </ul>
            </section>
        </nav>
    </div>
</div>
<div style=\"height: 50px;\"></div>
<!-- content goes here -->
";
        // line 116
        $this->displayBlock('frontend_content', $context, $blocks);
        // line 117
        echo "
<div class=\"footer\">
    <div class=\"row\">
        <div class=\"small-5 columns\">
            <a href=\"";
        // line 121
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/privacy\">Privacy Policy</a>
        </div>
        <div class=\"small-1 columns\">
            |
        </div>
        <div class=\"small-6 columns\">
            <a href=\"";
        // line 127
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/terms\">Terms &amp; Conditions</a>
        </div>
    </div>
    <div>
        Copyright &copy;2015 TOKYU LAND INDONESIA | All rights reserved
    </div>
</div>

";
        // line 135
        $this->displayBlock('frontend_js', $context, $blocks);
        // line 140
        echo "<script type=\"text/javascript\">
    \$(document).foundation();
</script>
</body>
</html>";
    }

    // line 3
    public function block_fb_head_prefix($context, array $blocks = array())
    {
    }

    // line 6
    public function block_fb_meta_data($context, array $blocks = array())
    {
    }

    // line 15
    public function block_frontend_css($context, array $blocks = array())
    {
        // line 16
        echo "        ";
        echo Asset::css("normalize.css");
        echo "
        ";
        // line 17
        echo Asset::css("foundation.css");
        echo "
        ";
        // line 18
        echo Asset::css("font-awesome.min.css");
        echo "
        ";
        // line 19
        echo Asset::css("mobile-style.css");
        echo "
    ";
    }

    // line 33
    public function block_fb_js_sdk($context, array $blocks = array())
    {
    }

    // line 116
    public function block_frontend_content($context, array $blocks = array())
    {
    }

    // line 135
    public function block_frontend_js($context, array $blocks = array())
    {
        // line 136
        echo "    ";
        echo Asset::js("jquery.min.js");
        echo "
    ";
        // line 137
        echo Asset::js("foundation.min.js");
        echo "
    ";
        // line 138
        echo Asset::js("foundation/foundation.topbar.js");
        echo "
";
    }

    public function getTemplateName()
    {
        return "pages/views/mobile_template.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  381 => 138,  377 => 137,  372 => 136,  369 => 135,  364 => 116,  359 => 33,  353 => 19,  349 => 18,  345 => 17,  340 => 16,  337 => 15,  332 => 6,  327 => 3,  319 => 140,  317 => 135,  305 => 127,  295 => 121,  289 => 117,  287 => 116,  277 => 108,  264 => 105,  261 => 104,  257 => 103,  251 => 101,  246 => 100,  241 => 99,  236 => 98,  231 => 97,  221 => 89,  208 => 86,  205 => 85,  201 => 84,  189 => 78,  182 => 73,  169 => 70,  162 => 68,  156 => 66,  146 => 64,  141 => 63,  136 => 62,  126 => 54,  113 => 51,  106 => 49,  85 => 37,  80 => 34,  78 => 33,  70 => 28,  61 => 21,  59 => 15,  50 => 12,  46 => 11,  38 => 6,  33 => 4,  25 => 1,  191 => 90,  186 => 89,  183 => 88,  166 => 69,  163 => 74,  151 => 65,  142 => 64,  137 => 61,  133 => 60,  110 => 50,  107 => 40,  95 => 34,  91 => 40,  82 => 29,  77 => 26,  73 => 25,  57 => 12,  54 => 13,  43 => 8,  40 => 7,  36 => 6,  32 => 4,  29 => 3,);
    }
}
