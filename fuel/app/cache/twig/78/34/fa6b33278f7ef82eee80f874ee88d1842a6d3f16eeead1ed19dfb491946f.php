<?php

/* group.twig */
class __TwigTemplate_7834fa6b33278f7ef82eee80f874ee88d1842a6d3f16eeead1ed19dfb491946f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("pages/views/template_frontend.twig");

        $this->blocks = array(
            'frontend_content' => array($this, 'block_frontend_content'),
            'frontend_js' => array($this, 'block_frontend_js'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "pages/views/template_frontend.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_frontend_content($context, array $blocks = array())
    {
        // line 4
        echo "    <div class=\"tokyuland-group\">
        <div class=\"row\">
            <ul class=\"breadcrumbs\">
                <li><a href=\"#\">Home</a></li>
                <li class=\"current\"><a href=\"#\">About Group</a></li>
            </ul>
        </div>
        <div class=\"row\">
            <h1>ABOUT GROUP</h1>
            <div>
                <ul class=\"button-group\">
                    <li><a id=\"fodusanClick\" class=\"button\">Tokyu Fodusan Holding</a></li>
                    <li><a id=\"landClick\" class=\"button second\">Tokyu Land Corporation</a></li>
                </ul>
            </div>
        </div>
        <div class=\"row\">
            <div id=\"fodusan\">
                <div>
                    <img src=\"";
        // line 23
        echo Uri::base();
        echo "/assets/css/images/fodusan.png\"/>
                </div>
                <div style=\"height: 20px;\"></div>
                <div class=\"holding\">
                    <h2>Tokyu Fudosan Holding's Data</h2>
                    <p>
                        Tokyu Fudosan Holdings—Continuing to create new value for living, business, and culture in Japan
                    </p>
                </div>
                <div class=\"holding-data\" data-equalizer>
                    <div class=\"small-4 columns\" data-equalizer-watch>
                        <div class=\"img-holding\">
                            <img src=\"";
        // line 35
        echo Uri::base();
        echo "/assets/css/images/100.jpg\"/>
                        </div>
                        <div class=\"text-holding\">
                            Tokyu Fudosan Holdings has taken a proactive stance toward spin-offs into separate subsidiaries, in order to develop a high level of specialization in all of its business activities. Currently, nearly 100 group companies continue to tackle challenges in their respective business domains.
                        </div>
                    </div>
                    <div class=\"small-4 columns\" data-equalizer-watch>
                        <div class=\"img-holding\">
                            <img src=\"";
        // line 43
        echo Uri::base();
        echo "/assets/css/images/17000.jpg\"/>
                        </div>
                        <div class=\"text-holding\">
                            There are more than 17,000 employees at Tokyu Fudosan Holdings. Each one is a business professional committed to perseverance and facing challenges, while keeping a vision of the future in mind for the customer.
                        </div>
                    </div>
                    <div class=\"small-4 columns\" data-equalizer-watch>
                        <div class=\"img-holding\">
                            <img src=\"";
        // line 51
        echo Uri::base();
        echo "/assets/css/images/2500.jpg\"/>
                        </div>
                        <div class=\"text-holding\">
                            Founded upon the concepts of safety, security, and comfort, we build about 2,500 houses and condominiums in Japan for sale per year that fulfill the dreams of every single customer. Going forward, we will continue building houses and condominiums that represent our steadfast values.
                        </div>
                    </div>
                    <div style=\"clear: both\"></div>
                </div>
                <div class=\"holding-data\" data-equalizer>
                    <div class=\"small-4 columns\" data-equalizer-watch>
                        <div class=\"img-holding\">
                            <img src=\"";
        // line 62
        echo Uri::base();
        echo "/assets/css/images/60.jpg\"/>
                        </div>
                        <div class=\"text-holding\">
                            Tokyu Land Corporation’s office building business manages about 60 office buildings in the heart of the capital, and has been one of our core businesses since the company was established. We are committed to ensuring safety, security, and comfort; and we provide many diverse values in addition to supporting the needs of an office.
                        </div>
                    </div>
                    <div class=\"small-4 columns\" data-equalizer-watch>
                        <div class=\"img-holding\">
                            <img src=\"";
        // line 70
        echo Uri::base();
        echo "/assets/css/images/669000.jpg\"/>
                        </div>
                        <div class=\"text-holding\">
                            We develop office buildings based on both “hard” and “soft” aspects. This lets us create buildings that can maximize a business’s potential while remaining environmentally and people-friendly. We have expanded our office leasing business to cover a total surface area of about 669,000 Square meter.
                        </div>
                    </div>
                    <div class=\"small-4 columns\" data-equalizer-watch>
                        <div class=\"img-holding\">
                            <img src=\"";
        // line 78
        echo Uri::base();
        echo "/assets/css/images/6billion.jpg\"/>
                        </div>
                        <div class=\"text-holding\">
                            Tokyu Fudosan Holdings made consolidated net sales of  US\$6 billion for fiscal 2014. Heading into the future, we will use the Group’s management resources to the best of our ability, further evolve our business model, and continue to create new value while always keeping future generations in mind.
                        </div>
                    </div>
                    <div style=\"clear: both\"></div>
                </div>
            </div>
            <div style=\"height: 50px;\"></div>
            <div id=\"land\">
                <div class=\"tokyuland-land\">
                    <div class=\"img-land\">
                        <img src=\"";
        // line 91
        echo Uri::base();
        echo "/assets/css/images/title-land.jpg\"/>
                    </div>
                    <div style=\"height: 20px;\"></div>
                    <div class=\"sub-land\">
                        <p>Creating urban landscapes, supporting lifestyles, and opening doors to the future. Tokyu Fudosan Holdings’ business ties in to all aspects of life. </p>
                    </div>
                    <div class=\"land-data\" data-equalizer>
                        <div class=\"small-4 columns\" data-equalizer-watch>
                            <div class=\"wrap-land-corp\">
                                <div class=\"title-land-corp\">
                                    <p class=\"big-one\">C</p><p class=\"small-one\">omfortable offices that</p>
                                    <p class=\"small-one\">speed up business development</p>
                                </div>
                                <div class=\"img-land-corp\">
                                    <img src=\"";
        // line 105
        echo Uri::base();
        echo "/assets/css/images/comfortable.jpg\"/>
                                </div>
                                <div class=\"content-land-corp\">
                                    We pursue developing office buildings from both “hard” and “soft” aspects, in order to create buildings that can maximize a
                                    business’s potential while remaining
                                    environmentally and people-friendly. Various projects that will unlock the future of businesses are in progress. These include the
                                    Hamamatsu-cho project, the Shin-Aoyama Tokyu Building, and the Osaki Wiz Tower.
                                </div>
                            </div>

                        </div>
                        <div class=\"small-4 columns\" data-equalizer-watch>
                            <div class=\"wrap-land-corp mid-wrap-land\">
                                <div class=\"title-land-corp\">
                                    <p class=\"big-one\">P</p><p class=\"small-one\">roviding support for secure and healthy senior living</p>
                                </div>
                                <div class=\"img-land-corp\">
                                    <img src=\"";
        // line 122
        echo Uri::base();
        echo "/assets/css/images/providing.jpg\"/>
                                </div>
                                <div class=\"content-land-corp\">
                                    Japan now faces the issue of a rapidly-aging society, and naturally there is growing interest in providing quality living for senior citizens. Tokyu Fudosan has developed Senior
                                    Residences which provide hotel-like services, so that senior citizens can live in security into the future. It has also developed
                                    Care Residences as well, which come with nursing care services for those needing regular care throughout the day. We guarantee secure homes that last a lifetime, and provide
                                    assistance for healthy living.
                                </div>
                            </div>
                        </div>
                        <div class=\"small-4 columns\" data-equalizer-watch>
                            <div class=\"wrap-land-corp last-wrap-land\">
                                <div class=\"title-land-corp\">
                                    <p class=\"big-one\">R</p><p class=\"small-one\">esorts that heal people, and are kind to the environment</p>
                                </div>
                                <div class=\"img-land-corp\">
                                    <img src=\"";
        // line 138
        echo Uri::base();
        echo "/assets/css/images/resorts.jpg\"/>
                                </div>
                                <div class=\"content-land-corp\">
                                    We want to add color and flavor to your busy daily lives, helping you enjoy a richer life.
                                    We at Tokyu Fudosan have developed our resort business based on the basic philosophy of “coexistence and harmony with nature and people.” In order to respond to a broad range of diverse values and lifestyles, we are
                                    developing resorts and leisure facilities soon to be in demand by consumers. These include our resort complex “Tokyu Resort Town,”
                                    as well as other exclusive members-only hotels, golf courses, ski resorts, and holiday homes and villas.
                                </div>
                            </div>
                            <div style=\"clear: both;\"></div>
                        </div>
                    </div>
                    <div class=\"land-data\" data-equalizer>
                        <div class=\"small-4 columns\" data-equalizer-watch>
                            <div class=\"wrap-land-corp\">
                                <div class=\"title-land-corp\">
                                    <p class=\"big-one\">B</p><p class=\"small-one\">oldly proposing a new style of working</p>
                                </div>
                                <div class=\"img-land-corp\">
                                    <img src=\"";
        // line 157
        echo Uri::base();
        echo "/assets/css/images/boldly.jpg\"/>
                                </div>
                                <div class=\"content-land-corp\">
                                    The rapid development of IT, and the fall in production and working populations, have brought about improvements in productivity of business people, as well as a shift toward variable costs for the office. In light of this, Business Airport is our members-only satellite office that aims to respond to the social needs that companies confront. Similar to an
                                    international airport where all types of people come to depart on flights, we seek to help our customers’ businesses take flight. In addition to providing a comfortable business space,
                                    we also propose a new style of working
                                    for the future.
                                </div>
                            </div>

                        </div>
                        <div class=\"small-4 columns\" data-equalizer-watch>
                            <div class=\"wrap-land-corp mid-wrap-land\">
                                <div class=\"title-land-corp\">
                                    <p class=\"big-one\">C</p><p class=\"small-one\">ommercial facilities that create new value for the city</p>
                                </div>
                                <div class=\"img-land-corp\">
                                    <img src=\"";
        // line 174
        echo Uri::base();
        echo "/assets/css/images/commercial.jpg\"/>
                                </div>
                                <div class=\"content-land-corp\">
                                    Based on our track record of success in
                                    urban development, and by harnessing the power of the Group network, we provide total planning and support services for commercial facilities, spanning everything from planning and development to management. We are committed to creating facilities that harmonize with the city, are rooted in the community, and which will grow and evolve together with the region. To that end, we opened Tokyu Plaza Omotesando Harajuku in 2012. Currently, we are involved in projects that aim to further enhance the value of Tokyo, including projects in Ginza 5-chome and Jingumae 6-chome.
                                </div>
                            </div>
                        </div>
                        <div class=\"small-4 columns\" data-equalizer-watch>
                            <div class=\"wrap-land-corp last-wrap-land\">
                                <div class=\"title-land-corp\">
                                    <p class=\"big-one\">H</p><p class=\"small-one\">omes that fulfill dreams and desires</p>
                                </div>
                                <div class=\"img-land-corp\">
                                    <img src=\"";
        // line 188
        echo Uri::base();
        echo "/assets/css/images/homes.jpg\"/>
                                </div>
                                <div class=\"content-land-corp\">
                                    Founded upon the concepts of safety and security, we create homes that can fulfill the dreams and desires of each and every customer. Beginning with our condominium and landed housing brand “BRANZ”, which provides sophisticated residential spaces of high quality, we provide homes for the future, such as rental condominium units which let residents enjoy smart, urban lifestyles.
                                </div>
                            </div>
                            <div style=\"clear: both;\"></div>
                        </div>
                    </div>
                    <div class=\"land-data\" data-equalizer>
                        <div class=\"small-4 columns\" data-equalizer-watch>
                            <div class=\"wrap-land-corp\">
                                <div class=\"title-land-corp\">
                                    <p class=\"big-one\">O</p><p class=\"small-one\" style=\"margin-top:10px; \">verseas projects</p>
                                </div>
                                <div class=\"img-land-corp\">
                                    <img src=\"";
        // line 204
        echo Uri::base();
        echo "/assets/css/images/overseas.jpg\"/>
                                </div>
                                <div class=\"content-land-corp\">
                                    In the 40 years since the company entered the Indonesia market, Tokyu Fudosan has turned its eyes to the world and its future. Since 2013, it has invested in various types of real estate from its base in Los Angeles, including creative offices in Los Angeles and apartments in Houston. While entering into cooperative businesses with Japanese and local companies, it aims to steadily continue expanding its business into the future. It has also launched condominium projects in China, in Qingdao and Dalian; as well as complex development projects in Shenyang, Liaoning Province. Tokyu Fudosan will continue to anticipate what the world and our lifestyles will be like in the next 10 or 20 years, and expand its scope of challenges across the world.
                                </div>
                            </div>
                        </div>
                        <div class=\"small-8 columns\" data-equalizer-watch>
                            <div id=\"back-top\">
                                <img src=\"";
        // line 213
        echo Uri::base();
        echo "/assets/css/images/top-land.png\"/>
                            </div>
                        </div>
                </div>
            </div>
        </div>
    </div>

    <div style=\"height: 50px;\"></div>
";
    }

    // line 224
    public function block_frontend_js($context, array $blocks = array())
    {
        // line 225
        echo "    ";
        $this->displayParentBlock("frontend_js", $context, $blocks);
        echo "
    ";
        // line 226
        echo Asset::js("foundation/foundation.equalizer.js");
        echo "

    <script>
        \$(\"#fodusanClick\").click(function (){
            //\$(this).animate(function(){
            \$('html, body').animate({
                scrollTop: \$(\"#fodusan\").offset().top
            }, 2000);
            //});
        });

        \$(\"#landClick\").click(function (){
            //\$(this).animate(function(){
            \$('html, body').animate({
                scrollTop: \$(\"#land\").offset().top
            }, 2000);
            //});
        });
        \$(\"#back-top\").click(function (){
            //\$(this).animate(function(){
            \$('html, body').animate({
                scrollTop: 0
            }, 2000);
            //});
        });
    </script>

";
    }

    public function getTemplateName()
    {
        return "group.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  310 => 226,  305 => 225,  302 => 224,  288 => 213,  276 => 204,  257 => 188,  240 => 174,  220 => 157,  198 => 138,  179 => 122,  159 => 105,  142 => 91,  126 => 78,  115 => 70,  104 => 62,  90 => 51,  79 => 43,  68 => 35,  53 => 23,  32 => 4,  29 => 3,);
    }
}
