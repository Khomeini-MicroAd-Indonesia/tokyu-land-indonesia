<?php

/* history.twig */
class __TwigTemplate_b90c6afea3f525e07c486e94440f0434f4bfeebc1b69d9e098ab1afb2af5a2ec extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("pages/views/template_frontend.twig");

        $this->blocks = array(
            'frontend_content' => array($this, 'block_frontend_content'),
            'frontend_js' => array($this, 'block_frontend_js'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "pages/views/template_frontend.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_frontend_content($context, array $blocks = array())
    {
        // line 4
        echo "    <div class=\"tokyuland-history\">
        <div class=\"row\">
            <ul class=\"breadcrumbs\">
                <li><a href=\"";
        // line 7
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/home\">Home</a></li>
                <li><a href=\"";
        // line 8
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/about-us/president\">About Us</a></li>
                <li class=\"current\">History in Indonesia</li>
            </ul>
        </div>
        <div class=\"row about-sub-menu\">
            <div class=\"small-2 columns\">
                About us |
            </div>
            <div class=\"small-10 columns\">
                <ul class=\"inline-list\">
                    <li><a href=\"";
        // line 18
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/about-us/president\">President Director’s Message</a></li>
                    <li><a class=\"active\" href=\"";
        // line 19
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/about-us/history\">History in Indonesia</a></li>
                    <li><a href=\"";
        // line 20
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/about-us/business\">Business Development</a></li>
                    <li><a href=\"";
        // line 21
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/about-us/company\">Company Profile</a></li>
                </ul>
            </div>
        </div>
        <div class=\"row\">
            <div class=\"small-12 columns\">
                <h3>
                   <strong>History In Indonesia</strong><br/>
                   The path travelled by Tokyu Land Corporation for 60 years in Japan.<br/>
                   The bonds with Indonesia that have endured for 40 years.
                </h3>
                <p>
                    ";
        // line 33
        echo Lang::get("history1");
        echo "
                </p>
                <p>
                    ";
        // line 36
        echo Lang::get("history2");
        echo "
                </p>
                <div class=\"small-text-center\">
                    <img style=\"width: 100%;\" src=\"";
        // line 39
        echo Uri::base();
        echo "/assets/css/images/timeline-history-";
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo ".png\"/>
                </div>
            </div>
        </div>
    </div>

    <div style=\"height: 50px;\"></div>
</div>
";
    }

    // line 49
    public function block_frontend_js($context, array $blocks = array())
    {
        // line 50
        echo "    ";
        $this->displayParentBlock("frontend_js", $context, $blocks);
        echo "
";
    }

    public function getTemplateName()
    {
        return "history.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  117 => 50,  114 => 49,  99 => 39,  93 => 36,  87 => 33,  71 => 21,  66 => 20,  61 => 19,  56 => 18,  42 => 8,  37 => 7,  32 => 4,  29 => 3,);
    }
}
