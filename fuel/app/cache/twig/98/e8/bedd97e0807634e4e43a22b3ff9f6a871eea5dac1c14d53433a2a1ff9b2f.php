<?php

/* home.twig */
class __TwigTemplate_98e8bedd97e0807634e4e43a22b3ff9f6a871eea5dac1c14d53433a2a1ff9b2f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("pages/views/template_frontend.twig");

        $this->blocks = array(
            'frontend_content' => array($this, 'block_frontend_content'),
            'frontend_js' => array($this, 'block_frontend_js'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "pages/views/template_frontend.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_frontend_content($context, array $blocks = array())
    {
        // line 4
        echo "    <div class=\"tokyuland-home\">
        <div class=\"row\">
            <ul class=\"example-orbit\" data-orbit data-options=\"animation:slide; pause_on_hover:true; resume_on_mouseout: true; timer_speed: 5000; slide_number: false; animation_speed:2000; navigation_arrows:false; bullets:false;\">

                ";
        // line 8
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["slide_img"]) ? $context["slide_img"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["filename"]) {
            // line 9
            echo "                    <li>
                        <img src=\"";
            // line 10
            echo Uri::base();
            echo "media/slide/";
            echo (isset($context["filename"]) ? $context["filename"] : null);
            echo "\" class=\"img-responsive\"/>
                    </li>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['filename'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 13
        echo "            </ul>

            <a href=\"";
        // line 15
        echo Uri::base();
        echo "branz/index.html\" target=\"_blank\"><img src=\"";
        echo Uri::base();
        echo "assets/css/images/Branz-Banner.jpg\"/></a>
        </div>
        <div class=\"row\" data-equalizer = \"mother\">
            <div class=\"small-6 columns\" data-equalizer-watch = \"mother\">
                <div class=\"project-home-title\">
                    <div class=\"small-8 columns\">
                        Project
                    </div>
                    <div class=\"small-4 columns\">
                        <span> > </span> <a href=\"";
        // line 24
        echo (isset($context["base_url"]) ? $context["base_url"] : null);
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/project\">All Projects</a>
                    </div>
                    <div style=\"clear: both\"></div>

                </div>
                ";
        // line 29
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["project_data"]) ? $context["project_data"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["project"]) {
            // line 30
            echo "                <div class=\"row\" data-equalizer=\"project\">
                    <div class=\"project-home\" data-equalizer-watch=\"project\">
                        <div class=\"small-6 columns\">
                            <a href=\"";
            // line 33
            echo $this->getAttribute((isset($context["project"]) ? $context["project"] : null), "web");
            echo "\" target=\"_blank\">
                                <img src=\"";
            // line 34
            echo Uri::base();
            echo "media/project/thumbnail/";
            echo $this->getAttribute((isset($context["project"]) ? $context["project"] : null), "image");
            echo "\" class=\"img-responsive\"/>
                            </a>
                        </div>
                        <div class=\"small-6 columns\">
                            <h5>";
            // line 38
            echo $this->getAttribute((isset($context["project"]) ? $context["project"] : null), "name");
            echo "</h5>
                            <h6><b>( ";
            // line 39
            echo $this->getAttribute((isset($context["project"]) ? $context["project"] : null), "type");
            echo " )</b></h6>
                            <p>";
            // line 40
            echo $this->getAttribute((isset($context["project"]) ? $context["project"] : null), "detail");
            echo "</p>
                            <p><b>Web: </b><a href=\"";
            // line 41
            echo $this->getAttribute((isset($context["project"]) ? $context["project"] : null), "web");
            echo "\" target=\"_blank\">";
            echo $this->getAttribute((isset($context["project"]) ? $context["project"] : null), "web");
            echo "</a></p>
                        </div>
                        <div style=\"clear: both;\"></div>
                    </div>
                        
                </div>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['project'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 48
        echo "            </div>
            <div class=\"small-6 columns\" data-equalizer-watch = \"mother\">
                <div class=\"news-home-title\">
                    <div class=\"small-8 columns\">
                        News Release
                    </div>
                    <div class=\"small-4 columns\">
                        <span> > </span> <a href=\"";
        // line 55
        echo (isset($context["base_url"]) ? $context["base_url"] : null);
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/news\">All News</a>
                    </div>
                    <div style=\"clear: both\"></div>
                </div>
                ";
        // line 59
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["news_data"]) ? $context["news_data"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["news"]) {
            // line 60
            echo "                    
                ";
            // line 61
            if (((isset($context["current_lang"]) ? $context["current_lang"] : null) == "en")) {
                echo "    
                
                <div class=\"row\" data-equalizer=\"news\">
                    <div class=\"news-home\" data-equalizer-watch=\"news\">
                        <div class=\"small-6 columns\">
                            <a href=\"";
                // line 66
                echo Uri::base();
                echo "media/news/doc/";
                echo $this->getAttribute((isset($context["news"]) ? $context["news"] : null), "doc_filename");
                echo "\" target=\"_blank\">
                                <img src=\"";
                // line 67
                echo Uri::base();
                echo "media/news/thumbnail/";
                echo $this->getAttribute((isset($context["news"]) ? $context["news"] : null), "image");
                echo "\" class=\"img-responsive\"/>
                            </a>
                        </div>
                        <div class=\"small-6 columns\">
                            <h5>";
                // line 71
                echo $this->getAttribute((isset($context["news"]) ? $context["news"] : null), "date");
                echo "</h5>
                            <p>";
                // line 72
                echo $this->getAttribute((isset($context["news"]) ? $context["news"] : null), "doc");
                echo "</p>
                            <p><a href=\"";
                // line 73
                echo Uri::base();
                echo "media/news/doc/";
                echo $this->getAttribute((isset($context["news"]) ? $context["news"] : null), "doc_filename");
                echo "\" target=\"_blank\"><img src=\"";
                echo Uri::base();
                echo "assets/img/pdf-icon-smallest.png\" class=\"img-responsive\"/></a></p>
                        </div>
                        <div style=\"clear: both\"></div>
                    </div>
                </div>
                        
                ";
            } else {
                // line 80
                echo "                
                <div class=\"row\" data-equalizer=\"news\">
                    <div class=\"news-home\" data-equalizer-watch=\"news\">
                        <div class=\"small-6 columns\">
                            <a href=\"";
                // line 84
                echo Uri::base();
                echo "media/news/docid/";
                echo $this->getAttribute((isset($context["news"]) ? $context["news"] : null), "doc_filename");
                echo "\" target=\"_blank\">
                                <img src=\"";
                // line 85
                echo Uri::base();
                echo "media/news/thumbnail/";
                echo $this->getAttribute((isset($context["news"]) ? $context["news"] : null), "image");
                echo "\" class=\"img-responsive\"/>
                            </a>
                        </div>
                        <div class=\"small-6 columns\">
                            <h5>";
                // line 89
                echo $this->getAttribute((isset($context["news"]) ? $context["news"] : null), "date");
                echo "</h5>
                            <p>";
                // line 90
                echo $this->getAttribute((isset($context["news"]) ? $context["news"] : null), "doc");
                echo "</p>
                            <p><a href=\"";
                // line 91
                echo Uri::base();
                echo "media/news/docid/";
                echo $this->getAttribute((isset($context["news"]) ? $context["news"] : null), "doc_filename");
                echo "\" target=\"_blank\"><img src=\"";
                echo Uri::base();
                echo "assets/img/pdf-icon-smallest.png\" class=\"img-responsive\"/></a></p>
                        </div>
                        <div style=\"clear: both\"></div>
                    </div>
                </div>
                ";
            }
            // line 97
            echo "                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['news'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 98
        echo "            </div>
        </div>
    </div>

    <div style=\"height: 50px;\"></div>
";
    }

    // line 105
    public function block_frontend_js($context, array $blocks = array())
    {
        // line 106
        echo "    ";
        $this->displayParentBlock("frontend_js", $context, $blocks);
        echo "
    ";
        // line 107
        echo Asset::js("foundation/foundation.orbit.js");
        echo "
    ";
        // line 108
        echo Asset::js("foundation/foundation.equalizer.js");
        echo "
";
    }

    public function getTemplateName()
    {
        return "home.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  270 => 108,  266 => 107,  261 => 106,  258 => 105,  249 => 98,  243 => 97,  230 => 91,  226 => 90,  222 => 89,  213 => 85,  207 => 84,  201 => 80,  187 => 73,  183 => 72,  179 => 71,  170 => 67,  164 => 66,  156 => 61,  153 => 60,  149 => 59,  141 => 55,  132 => 48,  117 => 41,  113 => 40,  109 => 39,  105 => 38,  96 => 34,  92 => 33,  87 => 30,  83 => 29,  74 => 24,  60 => 15,  56 => 13,  45 => 10,  42 => 9,  38 => 8,  32 => 4,  29 => 3,);
    }
}
