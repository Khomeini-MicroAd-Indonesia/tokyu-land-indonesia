<?php

/* philosophy.twig */
class __TwigTemplate_5c2590a2bd8a9694e7cf891814809747b8f78673c696992630923c50516f59ea extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("pages/views/template_frontend.twig");

        $this->blocks = array(
            'frontend_content' => array($this, 'block_frontend_content'),
            'frontend_js' => array($this, 'block_frontend_js'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "pages/views/template_frontend.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_frontend_content($context, array $blocks = array())
    {
        // line 4
        echo "    <div class=\"tokyuland-philosophy\">
        <div class=\"row\">
            <ul class=\"breadcrumbs\">
                <li><a href=\"";
        // line 7
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/home\">Home</a></li>
                <li class=\"current\">Philosophy</li>
            </ul>
        </div>
        <div class=\"philosophy-content row\">
            <h1>PHILOSOPHY</h1>
            <div class=\"small-12 columns\">
                <h5>
                    Tradition and progress. Technology and sensibility. People and nature. Taking on a new type of urban development that unites all values into one.
                </h5>
                <p>
                    ";
        // line 18
        echo Lang::get("philosophy-top1");
        echo "

                    ";
        // line 20
        echo Lang::get("philosophy-top2");
        echo "

                    ";
        // line 22
        echo Lang::get("philosophy-top3");
        echo "
                </p>
            </div>
            <div style=\"clear: both;\"></div>
            <div style=\"height: 30px;\"></div>
            <div class=\"philosphy-concept\">
                <div class=\"concept-1\">
                    <img src=\"";
        // line 29
        echo Uri::base();
        echo "/assets/css/images/back-1.jpg\"/>
                    <div class=\"black-content top-black\">
                        ";
        // line 31
        echo Lang::get("philosophy-left");
        echo "
                    </div>
                    <div class=\"black-content\">
                        Bringing the spirit of hospitality<br/>
                        and craftsmanship to Indonesia,<br/>
                        nurtured proudly through the<br/>
                        traditions and culture of Japan.
                    </div>
                </div>
                <div class=\"concept-2\">
                    <div class=\"small-7 columns\">
                        <img src=\"";
        // line 42
        echo Uri::base();
        echo "/assets/css/images/back-2.jpg\"/>
                    </div>
                    <div class=\"small-5 columns concept-text\">
                        <p>
                            <strong>
                                Harnessing our distinct character as<br/>
                                a Japanese corporation with<br/>
                                a deep knowledge of Indonesia.<br/>
                                We propose beautiful, comfortable,<br/>
                                and rich lifestyles that seek to protect<br/>
                                both natural environments and culture.<br/>
                                <span style=\"color: #888888;\">_________</span>
                            </strong>
                        </p>

                        <p class=\"tokyu-text\">

                            ";
        // line 59
        echo Lang::get("philosophy-right1");
        echo "<br/><br/>
                            ";
        // line 60
        echo Lang::get("philosophy-right2");
        echo "<br/><br/>
                            ";
        // line 61
        echo Lang::get("philosophy-right3");
        echo "<br/><br/>
                        </p>
                    </div>
                    <div style=\"clear: both;\"></div>
                </div>
                <div class=\"concept-3\">
                    <img src=\"";
        // line 67
        echo Uri::base();
        echo "/assets/css/images/back-3.jpg\"/>
                    <div class=\"trans-content bottom-content\">

                        <p>
                            <strong>
                                Providing the homes and lifestyles that the world desires.<br/>
                                Changing what a “premium residence” means,<br/>
                                starting with Indonesia.
                            </strong>
                        </p>
                    </div>
                    <div class=\"trans-content\">

                        <p>
                            ";
        // line 81
        echo Lang::get("philosophy-bottom1");
        echo "<br/><br/>

                            ";
        // line 83
        echo Lang::get("philosophy-bottom2");
        echo "<br/><br/>

                            ";
        // line 85
        echo Lang::get("philosophy-bottom3");
        echo "<br/><br/>

                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div style=\"height: 50px;\"></div>
";
    }

    // line 97
    public function block_frontend_js($context, array $blocks = array())
    {
        // line 98
        echo "    ";
        $this->displayParentBlock("frontend_js", $context, $blocks);
        echo "
";
    }

    public function getTemplateName()
    {
        return "philosophy.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  173 => 98,  170 => 97,  155 => 85,  150 => 83,  145 => 81,  128 => 67,  119 => 61,  115 => 60,  111 => 59,  91 => 42,  77 => 31,  72 => 29,  62 => 22,  57 => 20,  52 => 18,  37 => 7,  32 => 4,  29 => 3,);
    }
}
