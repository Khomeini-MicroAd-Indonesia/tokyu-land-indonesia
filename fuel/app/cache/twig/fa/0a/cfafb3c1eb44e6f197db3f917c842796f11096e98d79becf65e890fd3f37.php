<?php

/* president.twig */
class __TwigTemplate_fa0acfafb3c1eb44e6f197db3f917c842796f11096e98d79becf65e890fd3f37 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("pages/views/template_frontend.twig");

        $this->blocks = array(
            'frontend_content' => array($this, 'block_frontend_content'),
            'frontend_js' => array($this, 'block_frontend_js'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "pages/views/template_frontend.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_frontend_content($context, array $blocks = array())
    {
        // line 4
        echo "    <div class=\"tokyuland-president\">
        <div class=\"row\">
            <ul class=\"breadcrumbs\">
                <li><a href=\"";
        // line 7
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/home\">Home</a></li>
                <li><a href=\"";
        // line 8
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/about-us/president\">About Us</a></li>
                <li class=\"current\">President Director's Message</li>
            </ul>
        </div>
        <div class=\"row about-sub-menu\">
            <div class=\"small-2 columns\">
                About us |
            </div>
            <div class=\"small-10 columns\">
                <ul class=\"inline-list\">
                    <li><a class=\"active\" href=\"";
        // line 18
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/about-us/president\">President Director’s Message</a></li>
                    <li><a href=\"";
        // line 19
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/about-us/history\">History in Indonesia</a></li>
                    <li><a href=\"";
        // line 20
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/about-us/business\">Business Development</a></li>
                    <li><a href=\"";
        // line 21
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/about-us/company\">Company Profile</a></li>
                </ul>
            </div>
        </div>
        <div class=\"row\">
            <div class=\"small-5 columns\">
                <img src=\"";
        // line 27
        echo Uri::base();
        echo "/assets/css/images/president.jpg\">
            </div>
            <div class=\"small-7 columns\">
                <p>President director’s message :</p>
                <p>Keeping our goal of contributing to Indonesia’s development in mind, aiming high, and seeking business continuity.</p>
                <div class=\"vcard\">
                    <p>";
        // line 33
        echo Lang::get("director1");
        echo "</p>
                    <p>";
        // line 34
        echo Lang::get("director2");
        echo "</p>
                    <p>";
        // line 35
        echo Lang::get("director3");
        echo "</p>

                    <div class=\"small-text-right\">
                        <img src=\"";
        // line 38
        echo Uri::base();
        echo "/assets/css/images/signature.jpg\"/>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div style=\"height: 50px;\"></div>
";
    }

    // line 48
    public function block_frontend_js($context, array $blocks = array())
    {
        // line 49
        echo "    ";
        $this->displayParentBlock("frontend_js", $context, $blocks);
        echo "
";
    }

    public function getTemplateName()
    {
        return "president.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  120 => 49,  117 => 48,  104 => 38,  98 => 35,  94 => 34,  90 => 33,  81 => 27,  71 => 21,  66 => 20,  61 => 19,  56 => 18,  42 => 8,  37 => 7,  32 => 4,  29 => 3,);
    }
}
