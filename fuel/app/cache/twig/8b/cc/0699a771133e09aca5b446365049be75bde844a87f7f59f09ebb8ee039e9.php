<?php

/* thanks.twig */
class __TwigTemplate_8bcc0699a771133e09aca5b446365049be75bde844a87f7f59f09ebb8ee039e9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("pages/views/template_frontend.twig");

        $this->blocks = array(
            'frontend_content' => array($this, 'block_frontend_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "pages/views/template_frontend.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_frontend_content($context, array $blocks = array())
    {
        // line 4
        echo "    <div class=\"tokyuland-career\">
    <div class=\"row\">
        <ul class=\"breadcrumbs\">
            <li><a href=\"";
        // line 7
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "home\">Home</a></li>
            <li class=\"current\">Contact</li>
        </ul>
    </div>
    <div class=\"row\">
        <h1>Contact Us</h1>
    </div>

    <div class=\"row\">
        <h3>Thank You</h3>
        <div class=\"foot-note\" style=\"font-weight: normal;\">
            Thank you for taking time to contact us.<br/>
            We will be in touch shortly.<br/><br/>
        </div>

        <div class=\"apply-button\">
            <a href=\"";
        // line 23
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/home\" class=\"button\">Home</a>
        </div>


    </div>
    <div style=\"height: 50px;\"></div>
";
    }

    public function getTemplateName()
    {
        return "thanks.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  56 => 23,  36 => 7,  31 => 4,  28 => 3,);
    }
}
