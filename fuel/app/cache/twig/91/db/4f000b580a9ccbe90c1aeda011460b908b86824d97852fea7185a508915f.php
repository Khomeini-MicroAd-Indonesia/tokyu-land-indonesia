<?php

/* project.twig */
class __TwigTemplate_91db4f000b580a9ccbe90c1aeda011460b908b86824d97852fea7185a508915f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("pages/views/template_frontend.twig");

        $this->blocks = array(
            'frontend_content' => array($this, 'block_frontend_content'),
            'frontend_js' => array($this, 'block_frontend_js'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "pages/views/template_frontend.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_frontend_content($context, array $blocks = array())
    {
        // line 4
        echo "    <div class=\"tokyuland-project\">
        <div class=\"row\">
            <ul class=\"breadcrumbs\">
                <li><a href=\"#\">Home</a></li>
                <li class=\"current\"><a href=\"#\">Project</a></li>
            </ul>
        </div>
        <div class=\"row\">
            <h1>PROJECT - PT Tokyu Land Indonesia</h1>
            
            ";
        // line 14
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["project_data"]) ? $context["project_data"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["project"]) {
            // line 15
            echo "                <div class=\"small-3 columns\">
                    <a href=\"";
            // line 16
            echo $this->getAttribute((isset($context["project"]) ? $context["project"] : null), "web");
            echo "\" target=\"_blank\">
                        <img src=\"";
            // line 17
            echo Uri::base();
            echo "media/project/";
            echo $this->getAttribute((isset($context["project"]) ? $context["project"] : null), "image");
            echo "\"/>
                    </a>
                </div>
                <div class=\"small-9 columns\">
                    <div class=\"title-project\">
                       ";
            // line 22
            echo $this->getAttribute((isset($context["project"]) ? $context["project"] : null), "name");
            echo " (";
            echo $this->getAttribute((isset($context["project"]) ? $context["project"] : null), "type");
            echo ")
                    </div>
                    <div class=\"content-project\">
                        <div>";
            // line 25
            echo $this->getAttribute((isset($context["project"]) ? $context["project"] : null), "detail");
            echo "</div>
                        <div><b>Web: </b><a href=\"";
            // line 26
            echo $this->getAttribute((isset($context["project"]) ? $context["project"] : null), "web");
            echo "\" target=\"_blank\">";
            echo $this->getAttribute((isset($context["project"]) ? $context["project"] : null), "web");
            echo "</a></div>
                    </div>
                </div>
                <div style=\"clear: both\"></div>
                <div style=\"height: 20px;\"></div>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['project'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 32
        echo "        </div>
    </div>

    <div style=\"height: 50px;\"></div>
";
    }

    // line 38
    public function block_frontend_js($context, array $blocks = array())
    {
        // line 39
        echo "    ";
        $this->displayParentBlock("frontend_js", $context, $blocks);
        echo "

";
    }

    public function getTemplateName()
    {
        return "project.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  102 => 39,  99 => 38,  91 => 32,  77 => 26,  73 => 25,  65 => 22,  55 => 17,  51 => 16,  48 => 15,  44 => 14,  32 => 4,  29 => 3,);
    }
}
