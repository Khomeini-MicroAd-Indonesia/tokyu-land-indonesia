<?php

/* company.twig */
class __TwigTemplate_91f6e6c6363f8906562a183a89612cad344a965403b49383867a650d4a0f208b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("pages/views/template_frontend.twig");

        $this->blocks = array(
            'frontend_content' => array($this, 'block_frontend_content'),
            'frontend_js' => array($this, 'block_frontend_js'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "pages/views/template_frontend.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_frontend_content($context, array $blocks = array())
    {
        // line 4
        echo "    <div class=\"tokyuland-company\">
        <div class=\"row\">
            <ul class=\"breadcrumbs\">
                <li><a href=\"";
        // line 7
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/home\">Home</a></li>
                <li><a href=\"";
        // line 8
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/about-us/president\">About Us</a></li>
                <li class=\"current\">Company Profile</li>
            </ul>
        </div>
        <div class=\"row about-sub-menu\">
            <div class=\"small-2 columns\">
                About us |
            </div>
            <div class=\"small-10 columns\">
                <ul class=\"inline-list\">
                    <li><a href=\"";
        // line 18
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/about-us/president\">President Director’s Message</a></li>
                    <li><a href=\"";
        // line 19
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/about-us/history\">History in Indonesia</a></li>
                    <li><a href=\"";
        // line 20
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/about-us/business\">Business Development</a></li>
                    <li><a class=\"active\" href=\"";
        // line 21
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/about-us/company\">Company Profile</a></li>
                </ul>
            </div>
        </div>
        <div class=\"row\">
            <div class=\"small-12 columns\">
                <h3>
                    <strong>Company Profile</strong>
                </h3>
                <div class=\"company-profile\">
                    <div class=\"right text-center small-6 columns profile-content\">
                        <h4>ESTABLISHED</h4>
                        <p>- 27 JULY 2012 -</p>
                        <h4>Headquarters</h4>
                        <p>
                            Menara Cakrawala (Skyline Building) 9th Fl.<br/>
                            Jl. M.H. Thamrin No. 9<br/>
                            Jakarta 10340<br/>
                            Indonesia
                        </p>
                        <h4>CAPITAL</h4>
                        <p>33.000.000 USD</p>
                        <h4>PRESIDENT DIRECTOR</h4>
                        <p>Shinya Miwa</p>
                    </div>
                    <div style=\"clear: both;\"></div>
                </div>
            </div>
            <div style=\"clear: both;\"></div>
            <div style=\"height: 30px\"></div>
            <div>
                <div class=\"company-maps-title\">
                    FIND US HERE
                </div>
                <div style=\"height: 30px\"></div>
                <div class=\"small-text-center\">
                    <iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3966.5831470440667!2d106.82319099999998!3d-6.186499999999999!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69f428a9019247%3A0xf54ef8d877de30d8!2sPT+Tokyu+Land+Indonesia!5e0!3m2!1sen!2sid!4v1432117561840\" width=\"796\" height=\"372\" frameborder=\"0\" style=\"border:0\"></iframe>
                </div>
            </div>
        </div>
    </div>

    <div style=\"height: 50px;\"></div>
";
    }

    // line 66
    public function block_frontend_js($context, array $blocks = array())
    {
        // line 67
        echo "    ";
        $this->displayParentBlock("frontend_js", $context, $blocks);
        echo "
";
    }

    public function getTemplateName()
    {
        return "company.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  123 => 67,  120 => 66,  71 => 21,  66 => 20,  61 => 19,  56 => 18,  42 => 8,  37 => 7,  32 => 4,  29 => 3,);
    }
}
