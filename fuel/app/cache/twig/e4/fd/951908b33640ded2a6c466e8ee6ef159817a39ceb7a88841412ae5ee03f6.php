<?php

/* csr.twig */
class __TwigTemplate_e4fd951908b33640ded2a6c466e8ee6ef159817a39ceb7a88841412ae5ee03f6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("pages/views/template_frontend.twig");

        $this->blocks = array(
            'frontend_content' => array($this, 'block_frontend_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "pages/views/template_frontend.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_frontend_content($context, array $blocks = array())
    {
        // line 4
        echo "    <div class=\"tokyuland-csr\">
        <div class=\"row\">
            <ul class=\"breadcrumbs\">
                <li><a href=\"";
        // line 7
        echo Uri::base();
        echo (isset($context["current_lang"]) ? $context["current_lang"] : null);
        echo "/home\">Home</a></li>
                <li class=\"current\">CSR</li>
            </ul>
        </div>
        <div class=\"row\">
            <h1>CSR</h1>
        </div>
        
        ";
        // line 15
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["activity_data"]) ? $context["activity_data"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["activity"]) {
            // line 16
            echo "            
        <div class=\"row\">
            <div class=\"primary-content\">
                ";
            // line 19
            echo $this->getAttribute((isset($context["activity"]) ? $context["activity"] : null), "highlight_content");
            echo "<br/><br/>
            </div>
            <div class=\"secondary-content\">
                <p>";
            // line 22
            echo Lang::get("csr1");
            echo "</p><br/>
            </div>
            <div style=\"height: 20px;\"></div>
        </div>
        <div class=\"row csr-content\">
            <div class=\"title-csr\">
                <div>";
            // line 28
            echo Lang::get("csr2");
            echo "</div>
            </div>
            <div class=\"another-content\">
                <p>";
            // line 31
            echo Lang::get("csr3");
            echo "</p>
            </div>
        
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['activity'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 35
        echo "            <div>
                <img src=\"";
        // line 36
        echo Uri::base();
        echo "/assets/css/images/csr-1.jpg\"/>
            </div>
            <div  class=\"right-csr\">
                <img src=\"";
        // line 39
        echo Uri::base();
        echo "/assets/css/images/csr-3.jpg\"/>
            </div>
            <div>
                <img src=\"";
        // line 42
        echo Uri::base();
        echo "/assets/css/images/csr-2.jpg\"/>
            </div>
        </div>
        <div style=\"height: 50px;\"></div>
  </div>
";
    }

    public function getTemplateName()
    {
        return "csr.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  103 => 42,  97 => 39,  91 => 36,  88 => 35,  78 => 31,  72 => 28,  63 => 22,  57 => 19,  52 => 16,  48 => 15,  36 => 7,  31 => 4,  28 => 3,);
    }
}
