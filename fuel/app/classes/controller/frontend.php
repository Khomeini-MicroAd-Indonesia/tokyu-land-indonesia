<?php
use FacebookManagement\Util_Api;

class Controller_Frontend extends Controller_Mama
{
	protected $_data_template = array(
		'meta_title' => '',
		'meta_desc' => '',
		'frontend_menus' => array(),
		'menu_parent_key' => '',
		'menu_current_key' => '',
	);
	protected $_check_lang = true;
	protected $_valid_lang = array(
		'en' => array(
			'active' => false,
			'label' => 'English',
            'label_mobile' => 'ENG',
			'link' => ''
		),
		'id' => array(
			'active' => false,
			'label' => 'Bahasa',
            'label_mobile' => 'INA',
			'link' => ''
		),
	);
	protected $_current_lang = 'en';
	
	public function before() {
		parent::before();
		$_change_lang_url = '/home';
		if ($this->_check_lang) {
			$_is_lang_valid = true;
			$temp_lang = $this->param('lang_code');
			if (empty($temp_lang)) {
				$_is_lang_valid = false;
			} else {
				if (isset($this->_valid_lang[$temp_lang])) {
					$this->_current_lang = $temp_lang;
					$this->_valid_lang[$temp_lang]['active'] = true;
				} else {
					$_is_lang_valid = false;
					\Log::error('Lang code '.$temp_lang.' is Invalid!');
				}
			}
			if (!$_is_lang_valid) {
				\Response::redirect(\Uri::base().$this->_current_lang.'/home');
			}
			$temp = '';
			foreach (\Uri::segments() as $key => $value) {
				if ($key > 0) {
					$temp .= '/'.$value;
				} else {
					if (!isset($this->_valid_lang[$value])) {
						break;
					}
				}
			}
			if (!empty($temp)) {
				$_change_lang_url = $temp;
                unset($temp);
			}
		}
		foreach ($this->_valid_lang as $key => $value) {
			$this->_valid_lang[$key]['link'] = \Uri::base().$key.$_change_lang_url;
		}
		$this->_data_template['valid_lang'] = $this->_valid_lang;
		$this->_data_template['current_lang'] = $this->_current_lang;
                $this->_data_template['banner'] = \Bannermanagement\Bannerutil::get_banner_img();
                $this->_data_template['footer_img'] = \Footermanagement\Footerutil::get_footer_img();
                
                //$this->_data_template['doc'] = \News\Newsutil::get_news_data($this->_current_lang);
		
		\Config::set('language', $this->_current_lang);
		\Lang::load($this->_current_lang);
		
		$this->_data_template['frontend_menus'] = array(
			'philosophy' => array(
				'label' => 'Philosophy',
				'route' => \Uri::base().$this->_current_lang.'/philosophy',
				'menu_id' => 'menu-philosophy',
				'menu_class' => 'menu-item',
			),
			'project' => array(
				'label' => 'Project',
				'route' => \Uri::base().$this->_current_lang.'/project',
				'menu_id' => 'menu-project',
				'menu_class' => 'menu-item',
			),
			'about_group' => array(
				'label' => 'About Group',
				'route' => \Uri::base().$this->_current_lang.'/about-group',
				'menu_id' => 'menu-about-group',
				'menu_class' => 'menu-item',
			),
			'about_us' => array(
				'label' => 'About Us',
				'route' => \Uri::base().$this->_current_lang.'/about-us/president',
				'menu_id' => 'menu-about-us',
				'menu_class' => 'menu-item',
				'submenus' => array(),
			),
			'csr' => array(
				'label' => 'CSR',
				'route' => \Uri::base().$this->_current_lang.'/csr',
				'menu_id' => 'menu-csr',
				'menu_class' => 'menu-item',
			),
			'career' => array(
				'label' => 'Career',
				'route' => \Uri::base().$this->_current_lang.'/career',
				'menu_id' => 'menu-career',
				'menu_class' => 'menu-item',
			),
			'news' => array(
				'label' => 'News Release',
				'route' => \Uri::base().$this->_current_lang.'/news',
				'menu_id' => 'menu-career',
				'menu_class' => 'menu-item',
			),
			'contact' => array(
				'label' => 'Contact Us',
				'route' => \Uri::base().$this->_current_lang.'/contact-us',
				'menu_id' => 'menu-contact-us',
				'menu_class' => 'menu-item',
			),
		);
	}
	
	protected function set_meta_info($current_route) {
		$this->_data_template['meta_title'] = \Config::get('config_basic.app_name');
		$model_page = \Pages\Model_Pages::query()
			->where('url_path', $current_route)
			->where('status', 1)
			->get_one();
		if (!empty($model_page)) {
			$this->_data_template['page_title'] = $model_page->title;
            $this->_data_template['page_content'] = $model_page->content;
			if (strlen($model_page->meta_title) > 0) {
				$this->_data_template['meta_title'] .= ' - '.$model_page->meta_title;
			}
			$this->_data_template['meta_desc'] = $model_page->meta_desc;
		}
	}
}
