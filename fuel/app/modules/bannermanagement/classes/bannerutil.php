<?php

namespace Bannermanagement;

class Bannerutil{

    /**
     * get banner image
     */
    
    public static function get_banner_img()
    {
        $bannerimgs = \Bannermanagement\Model_Banners::query()
                ->where('status', 1)
                ->get();
        
        if(empty($bannerimgs)){
            $data = array();
        }else{
            foreach ($bannerimgs as $bannerimg){
                $data[$bannerimg->id] = $bannerimg->filename;
            }
        }
        return $data;
        
    }
}