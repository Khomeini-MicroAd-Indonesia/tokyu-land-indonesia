<?php
namespace MediaUploader;

class Controller_FooterImage extends \Controller_Backend
{
	private $_module_url = 'backend/media-uploader/footerimage';
	private $_menu_key = 'media_uploader_banner_footerimage';
	
	public function before() {
		parent::before();
		$this->authenticate();
		// Check menu permission
		if (!$this->check_menu_permission($this->_menu_key, 'read')) {
			// if not have an access then redirect to error page
			\Response::redirect(\Uri::base().'backend/no-permission');
		}
		$this->_data_template['meta_title'] = 'Media Uploader - Footer Image';
		$this->_data_template['menu_parent_key'] = 'media_uploader';
		$this->_data_template['menu_current_key'] = 'media_uploader_footerimage';
	}
	
	public function action_index() {
		$this->_data_template['success_message'] = \Session::get_flash('success_message');
		$this->_data_template['error_message'] = \Session::get_flash('error_message');
		
		$this->_data_template['media_uploader_subtitle'] = 'Footer Image';
		$this->_data_template['form_action_url'] = \Uri::base().$this->_module_url.'/upload';
		$this->_data_template['information_text'] = '
			<ul>
				<li>Max filesize is 1 MB</li>
				<li>Optimal Image Dimension is 220px X 90px</li>
			</ul>
		';
		
		return \Response::forge(\View::forge('backend/jquery_image_uploader.twig', $this->_data_template, FALSE));
	}
	
	public function action_upload() {
		\Package::load('jqueryfileupload');
		$options = array(
			'script_url' => \Uri::base().$this->_module_url.'/upload',
			'upload_dir' => DOCROOT.'media/footer/',
                        'upload_url' => \Uri::base().'media/footer/',
			'max_file_size' => 20000000, // 20 MB
			'image_versions' => array(
				'' => array(
					'auto_orient' => false,
					'max_width' => 1920,
				),
				'thumbnail' => array(
					'crop' => false,
					'max_width' => 350,
				)
			),
		);
		$upload_handler = new \JqueryFileUploadHandler($options);
	}
}

