<?php

namespace Footermanagement;

class Footerutil{

    /**
     * get slide image
     */
    
    public static function get_footer_img()
    {
        $footerimgs = \Footermanagement\Model_FooterImages::query()
                ->where('status', 1)
		->order_by('seq')
                ->limit(4)
                ->get();
        

        return $footerimgs;
        
    }
}