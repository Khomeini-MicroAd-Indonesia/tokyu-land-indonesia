<?php
namespace Footermanagement;

class Model_FooterImages extends \Orm\Model {
	private $status_name = array('InActive', 'Active');
	private $image_path = 'media/footer/';
	
	protected static $_table_name = 'footer_images';

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'footer_images'=>array('before_insert'),
			'mysql_timestamp' => true
		),
		'Orm\Observer_UpdatedAt' => array(
			'footer_images'=>array('before_update'),
			'mysql_timestamp' => true
		),
		'Orm\Observer_Validation' => array(
			'footer_images'=>array('before_save')
		)
	);
        
	protected static $_properties = array(
		'id',
		'name' => array(
			'label' => 'Footer Banner Name',
			'validation' => array(
				'required',
				'max_length' => array(50),
			)
		),
		'filename' => array(
			'label' => 'Footer Banner Filename',
			'validation' => array(
				'required',
				'max_length' => array(150),
			)
		),
                'anchor' => array(
			'label' => 'Anchor Link',
			'validation' => array(
				'required',
				'max_length' => array(500),
			)
		),
		'status' => array(
			'label' => 'Status',
			'validation' => array(
				'required',
			)
		),
		'seq' => array(
			'label' => 'Sequence',
			'validation' => array(
				'required',
			)
		),
		'created_by',
		'created_at',
		'updated_by',
		'updated_at'
	);
	
	public function get_status_name() {
		$flag = $this->status;
		return isset($this->status_name[$flag]) ? $this->status_name[$flag] : '-';
	}
	
	public function get_image_path() {
		return $this->image_path;
	}
	
	public function get_all_images() {
		if (file_exists(DOCROOT.$this->image_path)) {
			$contents = \File::read_dir(DOCROOT.$this->image_path);
		} else {
			$contents = array();
		}
		return $contents;
	}
	
	public function get_form_data_basic() {
		return array(
			'attributes' => array(
				'name' => 'frm_footer_image',
				'class' => 'form-horizontal',
				'role' => 'form',
				'action' => '',
				'method' => 'post',
			),
			'hidden' => array(),
			'fieldset' => array(
				array(
					'label' => array(
						'label' => 'Image Name',
						'id' => 'image_name',
						'attributes' => array(
							'class' => 'col-sm-2 control-label'
						)
					),
					'input' => array(
						'name' => 'image_name',
						'value' => $this->name,
						'attributes' => array(
							'class' => 'form-control',
							'placeholder' => 'Image Name',
							'required' => '',
						),
						'container_class' => 'col-sm-10'
					)
				),
                            	array(
					'label' => array(
						'label' => 'Anchor Link',
						'id' => 'anchor',
						'attributes' => array(
							'class' => 'col-sm-2 control-label'
						)
					),
					'input' => array(
						'name' => 'anchor',
						'value' => $this->anchor,
						'attributes' => array(
							'class' => 'form-control',
							'placeholder' => 'Anchor Link',
							'required' => '',
						),
						'container_class' => 'col-sm-10'
					)
				),
				array(
					'label' => array(
						'label' => 'Status',
						'id' => 'status',
						'attributes' => array(
							'class' => 'col-sm-2 control-label'
						)
					),
					'select' => array(
						'name' => 'status',
						'value' => $this->status,
						'options' => $this->status_name,
						'attributes' => array(
							'class' => 'form-control',
							'placeholder' => 'Status',
							'required' => ''
						),
						'container_class' => 'col-sm-10'
					)
				),
				array(
					'label' => array(
						'label' => 'Seq',
						'id' => 'image_seq',
						'attributes' => array(
							'class' => 'col-sm-2 control-label'
						)
					),
					'input' => array(
						'name' => 'image_seq',
						'value' => $this->seq,
						'attributes' => array(
							'class' => 'form-control',
							'placeholder' => '0',
							'required' => '',
						),
						'container_class' => 'col-sm-10'
					)
				),
				array(
					'label' => array(
						'label' => 'Image',
						'id' => 'image_filename',
						'attributes' => array(
							'class' => 'col-sm-2 control-label'
						)
					),
					'select_image_picker' => array(
						'name' => 'image_filename',
						'value' => $this->filename,
						'options' => $this->get_all_images(),
						'attributes' => array(
							'class' => 'form-control image-picker',
						),
						'container_class' => 'col-sm-10',
						'image_url' => \Uri::base().$this->image_path,
					)
				),
			)
		);
	}
}
