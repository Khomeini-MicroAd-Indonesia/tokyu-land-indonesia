<?php
namespace News;

class Model_News extends \Orm\Model {
	private $status_name = array('InActive', 'Active');
	
	protected static $_table_name = 'news';

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'news'=>array('before_insert'),
			'mysql_timestamp' => true
		),
		'Orm\Observer_UpdatedAt' => array(
			'news'=>array('before_update'),
			'mysql_timestamp' => true
		),
		'Orm\Observer_Validation' => array(
			'news'=>array('before_save')
		)
	);
        
	protected static $_properties = array(
		'id',
                'image_id' => array(
			'label' => 'Image',
			'validation' => array(
				'required',
			)
		),
                'doc_id' => array(
			'label' => 'Doc',
			'validation' => array(
				'required',
			)
		),
                'docid_id' => array(
                            'label' => 'Doc ID',
                            'validation' => array(
                                    'required',
                            )
		),
		'title' => array(
			'label' => 'News Title',
			'validation' => array(
				'required',
				'max_length' => array(150),
			)
		),
                'meta_title' => array(
			'label' => 'News Meta_Title',
			'validation' => array(
				'required',
				'max_length' => array(150),
			)
		),
                'content' => array(
                        'label' => 'News Content',
                        'validation' => array()
                ),
		'status' => array(
			'label' => 'Status',
			'validation' => array(
				'required',
			)
		),
                'post_date' => array(
			'label' => 'Post Date',
			'validation' => array(
				'required',
				'valid_date' => array(
					'format' => 'Y-m-d'
				)
			)
		),
		'created_by',
		'created_at',
		'updated_by',
		'updated_at'
	);
	
        protected static $_belongs_to = array(
            'images' => array(
                'key_from' => 'image_id',
                'model_to' => '\News\Model_NewsImages',
                'key_to' => 'id',
                'cascade_save' => true,
                'cascade_delete' => false,
            ),
            'docs' => array(
                'key_from' => 'doc_id',
                'model_to' => '\News\Model_NewsDocs',
                'key_to' => 'id',
                'cascade_save' => true,
                'cascade_delete' => false,
            ),
            'docids' => array(
                'key_from' => 'docid_id',
                'model_to' => '\News\Model_NewsDocids',
                'key_to' => 'id',
                'cascade_save' => true,
                'cascade_delete' => false,
            )
        
        );
        
        private static $_images;
        private static $_docs;
        private static $_docids;
        
	public function get_status_name() {
            $flag = $this->status;
            return isset($this->status_name[$flag]) ? $this->status_name[$flag] : '-';
	}
        
        public function get_image_name(){
            if(empty(self::$_images)){
                self::$_images = Model_NewsImages::get_as_array();
            }
            $flag = $this->image_id;
            return isset(self::$_images[$flag]) ? self::$_images[$flag] : '-';
        }
        
        public function get_doc_name(){
            if(empty(self::$_docs)){
                self::$_docs = Model_NewsDocs::get_as_array();
            }
            $flag = $this->image_id;
            return isset(self::$_images[$flag]) ? self::$_images[$flag] : '-';
        }
        
        public function get_docid_name(){
            if(empty(self::$_docids)){
                self::$_docids = Model_NewsDocids::get_as_array();
            }
            $flag = $this->image_id;
            return isset(self::$_images[$flag]) ? self::$_images[$flag] : '-';
        }
	
	public function get_form_data_basic($image, $doc, $docid) {
		return array(
			'attributes' => array(
				'name' => 'frm_news_news',
				'class' => 'form-horizontal',
				'role' => 'form',
				'action' => '',
				'method' => 'post',
			),
			'hidden' => array(),
			'fieldset' => array(
				array(
					'label' => array(
						'label' => 'Image',
						'id' => 'image_id',
						'attributes' => array(
							'class' => 'col-sm-2 control-label'
						)
					),
					'select' => array(
						'name' => 'image_id',
						'value' => $this->image_id,
						'options' => $image,
						'attributes' => array(
							'class' => 'form-control bootstrap-select',
							'placeholder' => 'Image',
							'data-live-search' => 'true',
							'data-size' => '3',
						),
						'container_class' => 'col-sm-10'
					)
				),
                                array(
					'label' => array(
						'label' => 'Doc',
						'id' => 'doc_id',
						'attributes' => array(
							'class' => 'col-sm-2 control-label'
						)
					),
					'select' => array(
						'name' => 'doc_id',
						'value' => $this->doc_id,
						'options' => $doc,
						'attributes' => array(
							'class' => 'form-control bootstrap-select',
							'placeholder' => 'Doc',
							'data-live-search' => 'true',
							'data-size' => '3',
						),
						'container_class' => 'col-sm-10'
					)
				),
                                array(
                                            'label' => array(
                                                    'label' => 'Doc ID',
                                                    'id' => 'docid_id',
                                                    'attributes' => array(
                                                            'class' => 'col-sm-2 control-label'
                                                    )
                                            ),
                                            'select' => array(
                                                    'name' => 'docid_id',
                                                    'value' => $this->docid_id,
                                                    'options' => $docid,
                                                    'attributes' => array(
                                                            'class' => 'form-control bootstrap-select',
                                                            'placeholder' => 'Doc ID',
                                                            'data-live-search' => 'true',
                                                            'data-size' => '3',
                                                    ),
                                                    'container_class' => 'col-sm-10'
                                            )
                                    ),
                                array(
					'label' => array(
						'label' => 'Title',
						'id' => 'news_title',
						'attributes' => array(
							'class' => 'col-sm-2 control-label'
						)
					),
					'input' => array(
						'name' => 'news_title',
						'value' => $this->title,
						'attributes' => array(
							'class' => 'form-control',
							'placeholder' => 'Title',
							'required' => '',
						),
						'container_class' => 'col-sm-10'
					)
				),
                            array(
					'label' => array(
						'label' => 'Meta_Title',
						'id' => 'news_metatitle',
						'attributes' => array(
							'class' => 'col-sm-2 control-label'
						)
					),
					'input' => array(
						'name' => 'news_metatitle',
						'value' => $this->meta_title,
						'attributes' => array(
							'class' => 'form-control',
							'placeholder' => 'Meta_Title',
							'required' => '',
						),
						'container_class' => 'col-sm-10'
					)
				),
                                array(
					'label' => array(
						'label' => 'Post Date',
						'id' => 'post_date',
						'attributes' => array(
							'class' => 'col-sm-2 control-label'
						)
					),
					'input' => array(
						'name' => 'post_date',
						'value' => $this->post_date,
						'attributes' => array(
							'class' => 'form-control mask-date',
							'placeholder' => 'Post Date',
							'required' => '',
						),
						'container_class' => 'col-sm-10'
					)
				),
				array(
					'label' => array(
						'label' => 'Status',
						'id' => 'status',
						'attributes' => array(
							'class' => 'col-sm-2 control-label'
						)
					),
					'select' => array(
						'name' => 'status',
						'value' => $this->status,
						'options' => $this->status_name,
						'attributes' => array(
							'class' => 'form-control',
							'placeholder' => 'Status',
							'required' => ''
						),
						'container_class' => 'col-sm-10'
					)
				),
                            array(
					'label' => array(
						'label' => 'News Content',
						'id' => 'news_content',
						'attributes' => array(
							'class' => 'col-sm-2 control-label'
						)
					),
					'textarea' => array(
						'name' => 'news_content',
						'value' => $this->content,
						'attributes' => array(
							'class' => 'form-control ckeditor',
							'placeholder' => '',
						),
						'container_class' => 'col-sm-10'
					)
				)
			)
		);
	}
}
