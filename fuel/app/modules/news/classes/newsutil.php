<?php
namespace News;

class Newsutil{
	
	public static function get_news_latest_year(){
        $latest_year = date('Y');
		$news_archive = \News\Model_News::query()
			->where('status', 1)
			->order_by('post_date', 'DESC')
			->get_one();
		if (!empty($news_archive)) {
			$latest_year = date('Y', strtotime($news_archive->post_date));
		}
        return $latest_year;
    }
	
	public static function get_news_data_group_year($lang){
            
            if($lang == 'en'){
                $news_archive = \News\Model_News::query()
			->related('images')
			->related('docs')
                        ->related('docids')
			->where('status', 1)
			->order_by('post_date', 'DESC')
			->get();
                
                $data = array();
                foreach ($news_archive as $news){
                    $year = date('Y', strtotime($news->post_date));
                    $data[$year][] = array(
                        'title'        => $news->title,
                        'meta_title'   => $news->meta_title,
                        'date'         => date('l, d F Y', strtotime($news->post_date)),
                        'content'      => $news->content,
                        'image'        => $news->images->filename,
                        'doc'          => $news->docs->name,
                        'doc_filename' => $news->docs->filename
                    );
                }
            }else{
                $news_archive = \News\Model_News::query()
			->related('images')
			->related('docs')
                        ->related('docids')
			->where('status', 1)
			->order_by('post_date', 'DESC')
			->get();
                
                $data = array();
                foreach ($news_archive as $news){
                    $year = date('Y', strtotime($news->post_date));
                    $data[$year][] = array(
                        'title'        => $news->title,
                        'date'         => date('l, d F Y', strtotime($news->post_date)),
                        'content'      => $news->content,
                        'image'        => $news->images->filename,
                        'doc'          => $news->docids->name,
                        'doc_filename' => $news->docids->filename
                    );
                }
            }
        
        return $data;
    }

    /**
     * get news data
     */
    
    public static function get_news_data($lang){
        //var_dump($lang); exit;
        if($lang == 'en'){
            $news_archive = \News\Model_News::query()
			->related('images')
			->related('docs')
                        ->related('docids')
			->where('status', 1)
			->order_by('post_date', 'DESC')
			->limit(3)
			->get();
                
            $data = array();
            //$post_date;
            foreach ($news_archive as $news){

                $data[] = array(
                    'title'        => $news->title,
                    'meta_title'   => $news->meta_title, 
                    'date'         => date('l, d F Y', strtotime($news->post_date)),
                    'content'      => $news->content,
                    'image'        => $news->images->filename,
                    'doc'          => $news->docs->name,
                    'doc_filename' => $news->docs->filename
                );
            }    
        }else{
                $news_archive = \News\Model_News::query()
                            ->related('images')
                            ->related('docs')
                            ->related('docids')
                            ->where('status', 1)
                            ->order_by('post_date', 'DESC')
                            ->limit(3)
                            ->get();

                $data = array();
                //$post_date;
                foreach ($news_archive as $news){

                    $data[] = array(
                        'title'        => $news->title,
                        'meta_title'   => $news->meta_title,
                        'date'         => date('l, d F Y', strtotime($news->post_date)),
                        'content'      => $news->content,
                        'image'        => $news->images->filename,
                        'doc'          => $news->docids->name,
                        'doc_filename' => $news->docids->filename
                    );
                }
            }
        return $data;
    }
    
    
    
    /**
     * get news image
     */
    
    public static function get_news_img()
    {
        $imgs = \News\Model_NewsImages::query()
                ->where('status', 1)
                ->get();
        
        if(empty($imgs)){
            $data = array();
        }else{
            foreach ($imgs as $img){
                $data[$img->id] = $img->name;
            }
        }
        return $data;
    }
    
    
    /**
     * get news doc
     */
    
    public static function get_news_doc($lang)
    {   
        if($lang == 'en'){
            $docs = \News\Model_NewsDocs::query()
                ->where('status', 1)
                ->get();
        
            if(empty($docs)){
                $data = array();
            }else{
                foreach ($docs as $doc){
                    $data[$doc->id] = $doc->name;
                }
            }
        }else{
            $docids = \News\Model_NewsDocids::query()
                ->where('status', 1)
                ->get();
        
            if(empty($docids)){
                $data = array();
            }else{
                foreach ($docids as $docid){
                    $data[$docid->id] = $docid->name;
                }
            }
        }
        
        return $data;
    }
    
}

