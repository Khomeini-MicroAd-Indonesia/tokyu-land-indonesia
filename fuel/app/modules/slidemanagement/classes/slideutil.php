<?php

namespace Slidemanagement;

class Slideutil{

    /**
     * get slide image
     */
    
    public static function get_slide_img()
    {
        $slideimgs = \Slidemanagement\Model_Slides::query()
                ->where('status', 1)
				->order_by('seq')
                ->get();
        
        if(empty($slideimgs)){
            $data = array();
        }else{
            foreach ($slideimgs as $slideimg){
                $data[$slideimg->id] = $slideimg->filename;
            }
        }
        return $data;
        
    }
}