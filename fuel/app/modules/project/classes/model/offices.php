<?php
namespace Project;

class Model_Offices extends \Orm\Model {
	private $status_name = array('InActive', 'Active');
	
	protected static $_table_name = 'offices';

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'projects'=>array('before_insert'),
			'mysql_timestamp' => true
		),
		'Orm\Observer_UpdatedAt' => array(
			'projects'=>array('before_update'),
			'mysql_timestamp' => true
		),
		'Orm\Observer_Validation' => array(
			'projects'=>array('before_save')
		)
	);
        
	protected static $_properties = array(
		'id',
		'city_id' => array(
			'label' => 'City',
			'validation' => array(
				'required',
			)
		),
		'name' => array(
			'label' => 'Office Name',
			'validation' => array(
				'required',
				'max_length' => array(50),
			)
		),
		'phone' => array(
			'label' => 'Office Phone',
			'validation' => array(
				'required',
				'max_length' => array(125),
			)
		),
                'email' => array(
			'label' => 'Office E-mail',
			'validation' => array(
				'required',
				'max_length' => array(50),
			)
		),
                'web' => array(
			'label' => 'Office Web',
			'validation' => array(
				'required',
				'max_length' => array(50),
			)
		),
		'address' => array(
			'label' => 'Office Address',
			'validation' => array(
				'required',
				'max_length' => array(255),
			)
		),
		'status' => array(
			'label' => 'Status',
			'validation' => array(
				'required',
			)
		),
		'created_by',
		'created_at',
		'updated_by',
		'updated_at'
	);
	
        protected static $_has_many = array(
            'projects' => array(
                'key_from'          =>  'id',
                'model_to'          =>  '\Project\Model_Projects',
                'key_to'            =>  'office_id',
                'cascade_save'      =>  false,
                'cascade_delete'    =>  false
                
            )
        );
        
	private static $_cities;
	
	public function get_status_name() {
		$flag = $this->status;
		return isset($this->status_name[$flag]) ? $this->status_name[$flag] : '-';
	}
	
	public function get_city_name() {
		if (empty(self::$_cities)) {
			self::$_cities = Model_Cities::get_as_array();
		}
		$flag = $this->city_id;
		return isset(self::$_cities[$flag]) ? self::$_cities[$flag] : '-';
	}
        
        public static function get_as_array ($filter=array()) {
		$items = self::find('all', $filter);
		if (empty($items)) {
			$data = array();
		} else {
			foreach ($items as $item) {
				$data[$item->id] = $item->name;
			}
		}
		return $data;
	}
	
	public function get_form_data_basic($city) {
		return array(
			'attributes' => array(
				'name' => 'frm_project_city',
				'class' => 'form-horizontal',
				'role' => 'form',
				'action' => '',
				'method' => 'post',
			),
			'hidden' => array(),
			'fieldset' => array(
				array(
					'label' => array(
						'label' => 'City',
						'id' => 'city_id',
						'attributes' => array(
							'class' => 'col-sm-2 control-label'
						)
					),
					'select' => array(
						'name' => 'city_id',
						'value' => $this->city_id,
						'options' => $city,
						'attributes' => array(
							'class' => 'form-control bootstrap-select',
							'placeholder' => 'City',
							'data-live-search' => 'true',
							'data-size' => '3',
						),
						'container_class' => 'col-sm-10'
					)
				),
				array(
					'label' => array(
						'label' => 'Office Name',
						'id' => 'office_name',
						'attributes' => array(
							'class' => 'col-sm-2 control-label'
						)
					),
					'input' => array(
						'name' => 'office_name',
						'value' => $this->name,
						'attributes' => array(
							'class' => 'form-control',
							'placeholder' => 'Office Name',
							'required' => '',
						),
						'container_class' => 'col-sm-10'
					)
				),
				array(
					'label' => array(
						'label' => 'Office Phone',
						'id' => 'office_phone',
						'attributes' => array(
							'class' => 'col-sm-2 control-label'
						)
					),
					'input' => array(
						'name' => 'office_phone',
						'value' => $this->phone,
						'attributes' => array(
							'class' => 'form-control',
							'placeholder' => 'Office Phone',
							'required' => '',
						),
						'container_class' => 'col-sm-10'
					)
				),
                                array(
					'label' => array(
						'label' => 'Office E-mail',
						'id' => 'office_email',
						'attributes' => array(
							'class' => 'col-sm-2 control-label'
						)
					),
					'input' => array(
						'name' => 'office_email',
						'value' => $this->email,
						'attributes' => array(
							'class' => 'form-control',
							'placeholder' => 'Office Email',
							'required' => '',
						),
						'container_class' => 'col-sm-10'
					)
				),
                            array(
					'label' => array(
						'label' => 'Office Web',
						'id' => 'office_web',
						'attributes' => array(
							'class' => 'col-sm-2 control-label'
						)
					),
					'input' => array(
						'name' => 'office_web',
						'value' => $this->web,
						'attributes' => array(
							'class' => 'form-control',
							'placeholder' => 'Office Web',
							'required' => '',
						),
						'container_class' => 'col-sm-10'
					)
				),
				array(
					'label' => array(
						'label' => 'Status',
						'id' => 'status',
						'attributes' => array(
							'class' => 'col-sm-2 control-label'
						)
					),
					'select' => array(
						'name' => 'status',
						'value' => $this->status,
						'options' => $this->status_name,
						'attributes' => array(
							'class' => 'form-control',
							'placeholder' => 'Status',
							'required' => ''
						),
						'container_class' => 'col-sm-10'
					)
				),
				array(
					'label' => array(
						'label' => 'Office Address',
						'id' => 'office_address',
						'attributes' => array(
							'class' => 'col-sm-2 control-label'
						)
					),
					'textarea' => array(
						'name' => 'office_address',
						'value' => $this->address,
						'attributes' => array(
							'class' => 'form-control',
							'placeholder' => '',
							'required' => '',
						),
						'container_class' => 'col-sm-10'
					)
				)
			)
		);
	}
}
