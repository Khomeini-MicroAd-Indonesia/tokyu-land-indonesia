<?php
namespace Project;

class Model_Projects extends \Orm\Model {
	private $status_name = array('InActive', 'Active');
	
	protected static $_table_name = 'projects';

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'projects'=>array('before_insert'),
			'mysql_timestamp' => true
		),
		'Orm\Observer_UpdatedAt' => array(
			'projects'=>array('before_update'),
			'mysql_timestamp' => true
		),
		'Orm\Observer_Validation' => array(
			'projects'=>array('before_save')
		)
	);
        
	protected static $_properties = array(
		'id',
                'city_id' => array(
			'label' => 'City',
			'validation' => array(
				'required',
			)
		),
                'office_id' => array(
			'label' => 'Office',
			'validation' => array(
				'required',
			)
		),
                'image_id' => array(
			'label' => 'Image',
			'validation' => array(
				'required',
			)
		),
		'name' => array(
			'label' => 'Project Name',
			'validation' => array(
				'required',
				'max_length' => array(50),
			)
		),
                'type' => array(
			'label' => 'Project Type',
			'validation' => array(
				'required',
				'max_length' => array(50),
			)
		),
                'detail' => array(
                        'label' => 'Project Detail',
                        'validation' => array()
                ),
		'status' => array(
			'label' => 'Status',
			'validation' => array(
				'required',
			)
		),
                'post_date' => array(
			'label' => 'Post Date',
			'validation' => array(
				'required',
				'valid_date' => array(
					'format' => 'Y-m-d'
				)
			)
		),
		'created_by',
		'created_at',
		'updated_by',
		'updated_at'
	);
	
        protected static $_belongs_to = array(
            'cities' => array(
                'key_from' => 'city_id',
                'model_to' => '\Project\Model_Cities',
                'key_to' => 'id',
                'cascade_save' => true,
                'cascade_delete' => false,
            ),
            'offices' => array(
                'key_from' => 'office_id',
                'model_to' => '\Project\Model_Offices',
                'key_to' => 'id',
                'cascade_save' => true,
                'cascade_delete' => false,
            ),
            'images' => array(
                'key_from' => 'image_id',
                'model_to' => '\Project\Model_ProjectImages',
                'key_to' => 'id',
                'cascade_save' => true,
                'cascade_delete' => false,
            )
        
        );
        
        private static $_cities;
        private static $_offices;
        private static $_images;
        
	public function get_status_name() {
            $flag = $this->status;
            return isset($this->status_name[$flag]) ? $this->status_name[$flag] : '-';
	}
        
        public function get_city_name(){
            if(empty(self::$_cities)){
                self::$_cities = Model_Cities::get_as_array();
            }
            $flag = $this->city_id;
            return isset(self::$_cities[$flag]) ? self::$_cities[$flag] : '-';
        }
        
        public function get_office_name(){
            if(empty(self::$_offices)){
                self::$_offices = Model_Offices::get_as_array();
            }
            $flag = $this->office_id;
            return isset(self::$_offices[$flag]) ? self::$_offices[$flag] : '-';
        }
        
        public function get_image_name(){
            if(empty(self::$_images)){
                self::$_images = Model_ProjectImages::get_as_array();
            }
            $flag = $this->image_id;
            return isset(self::$_images[$flag]) ? self::$_images[$flag] : '-';
        }
	
	public function get_form_data_basic($city, $office, $image) {
		return array(
			'attributes' => array(
				'name' => 'frm_project_project',
				'class' => 'form-horizontal',
				'role' => 'form',
				'action' => '',
				'method' => 'post',
			),
			'hidden' => array(),
			'fieldset' => array(
				array(
					'label' => array(
						'label' => 'City',
						'id' => 'city_id',
						'attributes' => array(
							'class' => 'col-sm-2 control-label'
						)
					),
					'select' => array(
						'name' => 'city_id',
						'value' => $this->city_id,
						'options' => $city,
						'attributes' => array(
                                                    'class' => 'form-control bootstrap-select',
                                                    'placeholder' => 'City',
                                                    'data-live-search' => 'true',
                                                    'data-size' => '3',
						),
						'container_class' => 'col-sm-10'
					)
				),
                                array(
					'label' => array(
						'label' => 'Office',
						'id' => 'office_id',
						'attributes' => array(
							'class' => 'col-sm-2 control-label'
						)
					),
					'select' => array(
						'name' => 'office_id',
						'value' => $this->office_id,
						'options' => $office,
						'attributes' => array(
							'class' => 'form-control bootstrap-select',
							'placeholder' => 'Office',
							'data-live-search' => 'true',
							'data-size' => '3',
						),
						'container_class' => 'col-sm-10'
					)
				),
                                array(
					'label' => array(
						'label' => 'Image',
						'id' => 'image_id',
						'attributes' => array(
							'class' => 'col-sm-2 control-label'
						)
					),
					'select' => array(
						'name' => 'image_id',
						'value' => $this->image_id,
						'options' => $image,
						'attributes' => array(
							'class' => 'form-control bootstrap-select',
							'placeholder' => 'Image',
							'data-live-search' => 'true',
							'data-size' => '3',
						),
						'container_class' => 'col-sm-10'
					)
				),
                                array(
					'label' => array(
						'label' => 'Project Name',
						'id' => 'project_name',
						'attributes' => array(
							'class' => 'col-sm-2 control-label'
						)
					),
					'input' => array(
						'name' => 'project_name',
						'value' => $this->name,
						'attributes' => array(
							'class' => 'form-control',
							'placeholder' => 'Project Name',
							'required' => '',
						),
						'container_class' => 'col-sm-10'
					)
				),
                            array(
					'label' => array(
						'label' => 'Project Type',
						'id' => 'project_type',
						'attributes' => array(
							'class' => 'col-sm-2 control-label'
						)
					),
					'input' => array(
						'name' => 'project_type',
						'value' => $this->type,
						'attributes' => array(
							'class' => 'form-control',
							'placeholder' => 'Project Type',
							'required' => '',
						),
						'container_class' => 'col-sm-10'
					)
				),
                                array(
					'label' => array(
						'label' => 'Post Date',
						'id' => 'post_date',
						'attributes' => array(
							'class' => 'col-sm-2 control-label'
						)
					),
					'input' => array(
						'name' => 'post_date',
						'value' => $this->post_date,
						'attributes' => array(
							'class' => 'form-control mask-date',
							'placeholder' => 'Post Date',
							'required' => '',
						),
						'container_class' => 'col-sm-10'
					)
				),
				array(
					'label' => array(
						'label' => 'Status',
						'id' => 'status',
						'attributes' => array(
							'class' => 'col-sm-2 control-label'
						)
					),
					'select' => array(
						'name' => 'status',
						'value' => $this->status,
						'options' => $this->status_name,
						'attributes' => array(
							'class' => 'form-control',
							'placeholder' => 'Status',
							'required' => ''
						),
						'container_class' => 'col-sm-10'
					)
				),
                            array(
					'label' => array(
						'label' => 'Project Detail',
						'id' => 'project_detail',
						'attributes' => array(
							'class' => 'col-sm-2 control-label'
						)
					),
					'textarea' => array(
						'name' => 'project_detail',
						'value' => $this->detail,
						'attributes' => array(
							'class' => 'form-control ckeditor',
							'placeholder' => '',
						),
						'container_class' => 'col-sm-10'
					)
				)
			)
		);
	}
}
