<?php
namespace Project;

class Projectutil{

    /**
     * get project data
     */
    
    public static function get_project_data(){
        $projects = \Project\Model_Projects::query()
                ->related('offices')
                ->related('images')
                ->where('status', 1)
                ->limit(3)
                ->order_by('post_date', 'DESC')
                ->get();
                
        $data = array();
        //$post_date;
        foreach ($projects as $project){
            
            $data[] = array(
                'name'          => $project->name,
                'type'          => $project->type,
                'detail'        => $project->detail,
                'office_name'   => $project->offices->name,
                'phone'         => $project->offices->phone,
                'email'         => $project->offices->email,
                'web'           => $project->offices->web,
                'image'         => $project->images->filename
            );
        }
        return $data;
    }
    
    /**
     * get project data (detil page)
     */
    
    public static function get_project_data_detail(){
        $projects = \Project\Model_Projects::query()
                ->related('offices')
                ->related('images')
                ->where('status', 1)
                ->order_by('post_date', 'DESC')
                ->get();
                
        $data = array();
        //$post_date;
        foreach ($projects as $project){
            
            $data[] = array(
                'name'          => $project->name,
                'type'          => $project->type,
                'detail'        => $project->detail,
                'office_name'   => $project->offices->name,
                'phone'         => $project->offices->phone,
                'email'         => $project->offices->email,
                'web'           => $project->offices->web,
                'image'         => $project->images->filename
            );
        }
        return $data;
    }
    
    
    /**
     * get project image
     */
    
    public static function get_project_img()
    {
        $imgs = \Project\Model_ProjectImages::query()
                ->where('status', 1)
                ->get();
        
        if(empty($imgs)){
            $data = array();
        }else{
            foreach ($imgs as $img){
                $data[$img->id] = $img->name;
            }
        }
        return $data;
    }
    
}

