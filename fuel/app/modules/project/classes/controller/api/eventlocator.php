<?php

namespace Event;

class Controller_API_Eventlocator extends \Controller_Frontend
{
    
    private $_module_url = '';
    private $_menu_key = 'event';
	
	public function before() {
		$this->_check_lang = false;
		parent::before();
	}
    
    public function action_getCountry(){
        $countries = \Event\Model_Countries::query()
                ->where('region_id', \Input::get('id'))
                ->where('status', 1)
                ->get();
        
        $data = array();
        
        foreach ($countries as $country){
            $data[] = array(
                'id'    => $country->id,
                'name'  => $country->name
            );
            
        }
        
        return new \Response(json_encode($data),200);
    }
     
    public function action_getCity(){
        $cities = \Event\Model_Cities::query()
                ->where('country_id', \Input::get('id'))
                ->where('status', 1)
                ->get();
        
        $data = array();
        foreach ($cities as $city){
            $data[] = array(
                'id'    => $city->id,
                'name'  => $city->name,
                'phone' => $city->phone,
                'email' => $city->email,
                'web'   => $city->web
            );
        }
        return new \Response(json_encode($data),200);
    }
    
    public function action_getEvent(){
        $events = \Event\Model_Events::query()
                ->where('city_id', \Input::get('id'))
                ->where('status', 1)
                ->related('cities')
                ->get();
                
        $data = array();
        //$post_date;
        foreach ($events as $event){
            
            $data[] = array(
                'name'      => $event->name,
                'post_date' => date('l, d F Y', strtotime($event->post_date)),
                'meta_desc' => $event->meta_desc,
                'phone'     => $event->cities->phone,
                'email'     => $event->cities->email,
                'web'       => $event->cities->web,
                'content'   => $event->content
            );
        }
        return new \Response(json_encode($data),200);
    }
    
    public function action_getSubmit(){
               
        $myemail = 'khomeini@microad.co.id';
        
        if (isset($_POST['eventname'])) {
            
            
            $eventname = strip_tags($_POST['eventname']);
            $email = strip_tags($_POST['email']);
            $desc = strip_tags($_POST['desc']);
            $date = strip_tags($_POST['date']);
            $time = strip_tags($_POST['time']);
            $country = strip_tags($_POST['country']);
            $city = strip_tags($_POST['city']);
            $location = strip_tags($_POST['location']);
            $organisation = strip_tags($_POST['organisation']);
            $weblink = strip_tags($_POST['weblink']);
            
            echo "<span class=\"alert alert-success\" >Your message has been submitted. Thanks!</span><br><br>";

            $to = $myemail;
            $email_subject = "Contact form submission for new event: $eventname";
            $email_body = "You have received a new message. ".
            " Here are the details:\n Event Name: $eventname \n ".
            "Email: $email\n ".
            "Description: $desc\n ".
            "Date: $date\n ".
            "Time: $time\n ".
            "Country: $country\n ".
            "City: $city\n ".
            "Location: $location\n ".
            "Organisation: $organisation\n ".
            "Weblink: $weblink\n ";
            
            $headers = "From: $myemail\n";
            $headers .= "Reply-To: $email";
            mail($to,$email_subject,$email_body,$headers);
        }
    
    }


}
