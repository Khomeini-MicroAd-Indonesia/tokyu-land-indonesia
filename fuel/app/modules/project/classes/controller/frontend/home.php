<?php
namespace Project;

class Controller_Frontend_Home extends \Controller_Frontend
{
    private $_module_url = '';
    private $_menu_key = 'project';
    private $_meta_slug = '/project';

    public function before() {
        parent::before();
    }

    public function action_index() {
        $this->set_meta_info($this->_meta_slug);
        $this->_data_template['project_data'] = \Project\Projectutil::get_project_data_detail();

        $this->_data_template['frontend_menus'][$this->_menu_key]['menu_class_a'] = 'active';
        if (\Fuel\Core\Agent::is_mobiledevice())
        {
            return \Response::forge(\View::forge('project::frontend/m_project.twig', $this->_data_template, FALSE));
        }
        else
        {
            return \Response::forge(\View::forge('project::frontend/project.twig', $this->_data_template, FALSE));
        }

    }

}

