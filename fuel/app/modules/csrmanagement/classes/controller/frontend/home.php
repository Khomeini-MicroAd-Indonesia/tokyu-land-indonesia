<?php
namespace Csrmanagement;

class Controller_Frontend_Home extends \Controller_Frontend
{
    private $_module_url = '';
    private $_menu_key = 'csr';
    private $_meta_slug = '/csr';

    public function before() {
        parent::before();
    }

    public function action_index() {
        $this->set_meta_info($this->_meta_slug);
        $this->_data_template['activity_data'] = \Csrmanagement\Activityutil::get_activity_data();
        $this->_data_template['frontend_menus'][$this->_menu_key]['menu_class_a'] = 'active';
        return \Response::forge(\View::forge('Csrmanagement::frontend/csr.twig', $this->_data_template, FALSE));
    }

}

