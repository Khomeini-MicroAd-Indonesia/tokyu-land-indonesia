<?php
namespace Csrmanagement;

class Activityutil{

    /**
     * get vacancy data
     */
    
    public static function get_activity_data(){
        $activities = \Csrmanagement\Model_Activities::query()
                ->where('status', 1)
                ->get();
                
        $data = array();
        //$post_date;
        foreach ($activities as $activity){
            
            $data[] = array(
                'title'             => $activity->title,
                'sub_title'         => $activity->sub_title,
                'highlight_content' => $activity->highlight_content,
                'header_content'    => $activity->header_content,
                'body_content'      => $activity->body_content
            );
        }
        return $data;
    }
    
}

