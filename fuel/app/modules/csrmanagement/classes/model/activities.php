<?php
namespace Csrmanagement;

class Model_Activities extends \Orm\Model {
	private $status_name = array('InActive', 'Active');
	
	protected static $_table_name = 'activities';

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'activities'=>array('before_insert'),
			'mysql_timestamp' => true
		),
		'Orm\Observer_UpdatedAt' => array(
			'activities'=>array('before_update'),
			'mysql_timestamp' => true
		),
		'Orm\Observer_Validation' => array(
			'activities'=>array('before_save')
		)
	);
        
	protected static $_properties = array(
		'id',
                'title' => array(
			'label' => 'Activity Title',
			'validation' => array(
				'required',
				'max_length' => array(50),
			)
		),
                'sub_title' => array(
			'label' => 'Activity Sub_Title',
			'validation' => array(
				'required',
				'max_length' => array(50),
			)
		),
                'highlight_content' => array(
                        'label' => 'Highlight Content',
                        'validation' => array()
                ),
                'header_content' => array(
                        'label' => 'Header Content',
                        'validation' => array()
                ),
                'body_content' => array(
                        'label' => 'Body Content',
                        'validation' => array()
                ),
		'status' => array(
			'label' => 'Status',
			'validation' => array(
				'required',
			)
		),
		'created_by',
		'created_at',
		'updated_by',
		'updated_at'
	);
	
	public function get_status_name() {
		$flag = $this->status;
		return isset($this->status_name[$flag]) ? $this->status_name[$flag] : '-';
	}
	
	public static function get_as_array ($filter=array()) {
            $items = self::find('all', $filter);
            if (empty($items)) {
                $data = array();
            } else {
                foreach ($items as $item) {
                $data[$item->id] = $item->name;
                }
            }
            return $data;
	}
	
	public function get_form_data_basic() {
		return array(
			'attributes' => array(
				'name' => 'frm_csr_activity',
				'class' => 'form-horizontal',
				'role' => 'form',
				'action' => '',
				'method' => 'post',
			),
			'hidden' => array(),
			'fieldset' => array(
                                array(
					'label' => array(
						'label' => 'Title',
						'id' => 'title',
						'attributes' => array(
							'class' => 'col-sm-2 control-label'
						)
					),
					'input' => array(
						'name' => 'title',
						'value' => $this->title,
						'attributes' => array(
							'class' => 'form-control',
							'placeholder' => 'Title',
							'required' => '',
						),
						'container_class' => 'col-sm-10'
					)
				),
                                array(
					'label' => array(
						'label' => 'Sub_Title',
						'id' => 'sub_title',
						'attributes' => array(
							'class' => 'col-sm-2 control-label'
						)
					),
					'input' => array(
						'name' => 'sub_title',
						'value' => $this->sub_title,
						'attributes' => array(
							'class' => 'form-control',
							'placeholder' => 'Sub_Title',
							'required' => '',
						),
						'container_class' => 'col-sm-10'
					)
				),
				array(
					'label' => array(
						'label' => 'Highlight Content',
						'id' => 'highlight_content',
						'attributes' => array(
							'class' => 'col-sm-2 control-label'
						)
					),
					'textarea' => array(
						'name' => 'highlight_content',
						'value' => $this->highlight_content,
						'attributes' => array(
							'class' => 'form-control ckeditor',
							'placeholder' => '',
						),
						'container_class' => 'col-sm-10'
					)
				),
                                array(
					'label' => array(
						'label' => 'Header Content',
						'id' => 'header_content',
						'attributes' => array(
							'class' => 'col-sm-2 control-label'
						)
					),
					'textarea' => array(
						'name' => 'header_content',
						'value' => $this->header_content,
						'attributes' => array(
							'class' => 'form-control ckeditor',
							'placeholder' => '',
						),
						'container_class' => 'col-sm-10'
					)
				),
                                array(
					'label' => array(
						'label' => 'Body Content',
						'id' => 'body_content',
						'attributes' => array(
							'class' => 'col-sm-2 control-label'
						)
					),
					'textarea' => array(
						'name' => 'body_content',
						'value' => $this->body_content,
						'attributes' => array(
							'class' => 'form-control ckeditor',
							'placeholder' => '',
						),
						'container_class' => 'col-sm-10'
					)
				),
				array(
					'label' => array(
						'label' => 'Status',
						'id' => 'status',
						'attributes' => array(
							'class' => 'col-sm-2 control-label'
						)
					),
					'select' => array(
						'name' => 'status',
						'value' => $this->status,
						'options' => $this->status_name,
						'attributes' => array(
							'class' => 'form-control',
							'placeholder' => 'Status',
							'required' => ''
						),
						'container_class' => 'col-sm-10'
					)
				),
			)
		);
	}
}
