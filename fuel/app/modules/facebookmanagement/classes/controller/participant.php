<?php
namespace FacebookManagement;

class Controller_Participant extends \Controller_Backend
{
	private $_module_url = 'backend/fb-participant';
	private $_menu_key = 'admin_fb_participant';
	
	public function before() {
		parent::before();
		$this->authenticate();
		// Check menu permission
		if (!$this->check_menu_permission($this->_menu_key, 'read')) {
			// if not have an access then redirect to error page
			\Response::redirect(\Uri::base().'backend/no-permission');
		}
		$this->_data_template['meta_title'] = 'Participant';
		$this->_data_template['menu_parent_key'] = 'admin_fb_management';
		$this->_data_template['menu_current_key'] = 'admin_fb_participant';
	}
	
	public function action_index() {
		$this->_data_template['participant_list'] = Model_Participants::query()->get();
		$this->_data_template['success_message'] = \Session::get_flash('success_message');
		$this->_data_template['error_message'] = \Session::get_flash('error_message');
		return \Response::forge(\View::forge('facebookmanagement::list/participant.twig', $this->_data_template, FALSE));
	}
}

