<?php
namespace FacebookManagement;

class Util_Api {
	public static $version = '1.1';
	private static $session;
	private static $login_url;
	
	/*
	* get current FB Session
	* @return
	* FacebookSession
	*/
	public static function getCurrentSession() {
		return self::$session;
	}
	
	/*
	* get FB Login URL
	* @return
	* string
	*/
	public static function getLoginUrl() {
		return self::$login_url;
	}
	
	/*
	* get current FB User Graph
	* @return
	* UserGraph
	*/
	public static function getCurrentUserGraph() {
		$fb_request = new \FacebookRequest(self::$session, 'GET', '/me');
		return $fb_request->execute()->getGraphObject(\GraphUser::className());
	}
	
	/*
	* init FB Session
	* @return
	* void
	*/
	public static function initSession() {
		\FacebookSession::setDefaultApplication(\Config::get('facebook.app_id'), \Config::get('facebook.secret_id'));
		
		// Get Facebook Session from Canvas
		$fb_canvas_login_helper = new \FacebookCanvasLoginHelper();
		$temp_session = $fb_canvas_login_helper->getSession();
		
		// If the session from FB canvas is empty then get from redirect login
		if (empty($temp_session)) {
			$fb_redirect_login_helper = new \FacebookRedirectLoginHelper(\Config::get('facebook.redirect_url'));
			$temp_session = $fb_redirect_login_helper->getSessionFromRedirect();
		}
		
		// set FB long lived session
		if (!empty($temp_session)) {
			$_SESSION['fb_token'] = $temp_session->getToken();
			self::$session = $temp_session->getLongLivedSession();
		} else {
			// If the session is still empty then get from server session
			if (isset($_SESSION['fb_token'])) {
				try{
					$temp_session = new \FacebookSession($_SESSION['fb_token']);
					$fb_session_validate = $temp_session->validate(); // validate if expired
					self::$session = $temp_session;
				}catch (\Exception $ex){
					unset($_SESSION['fb_token']);
					self::$session = null;
				}
			}
		}
	}
	
	/*
	* check participant of FB User
	* return true if FB user has give permission to the apps
	* save to database if the participant is just join now
	* @return
	* boolean
	*/
	public static function checkParticipant() {
		$return = false;
		if (empty(self::$session)) {
			// DEBUG purpose //print_r('<p>Redirecting...</p>');
			
			// Do Login
			$params = array('scope' => 'email');
			$fb_redirect_login_helper = new \FacebookRedirectLoginHelper(\Config::get('facebook.redirect_url'));
			self::$login_url = $fb_redirect_login_helper->getLoginUrl($params);
		} else {
			$return = true;
			// Check database for participant
			$fb_request = new \FacebookRequest(self::$session, 'GET', '/me');
			$user_graph = $fb_request->execute()->getGraphObject(\GraphUser::className());
			$user_count = Model_Participants::query()->where('fb_suid', $user_graph->getId())->count();
			// Save if participant is not in database
			if ($user_count == 0) {
				$user_graph_arr = $user_graph->asArray();
				$model_data = array(
					'fb_suid' => $user_graph->getId(),
					'fb_fullname' => $user_graph->getName(),
					'fb_link' => $user_graph->getLink(),
					'fb_email' => isset($user_graph_arr['email']) ? $user_graph_arr['email'] : '',
					'gender' => isset($user_graph_arr['gender']) ? $user_graph_arr['gender'] : ''
				);
				$model_participant = Model_Participants::forge($model_data);
				$model_participant->save();
			}
		}
		return $return;
	}
	
	/*
	* get participants model
	* @return
	* FacebookManagement\Model_Participants
	*/
	public static function getParticipant() {
		$fb_request = new \FacebookRequest(self::$session, 'GET', '/me');
		$user_graph = $fb_request->execute()->getGraphObject(\GraphUser::className());
		return Model_Participants::query()->where('fb_suid', $user_graph->getId())->get_one();
	}
}
