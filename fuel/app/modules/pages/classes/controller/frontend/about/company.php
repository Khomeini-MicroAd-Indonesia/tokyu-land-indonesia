<?php
namespace Pages;

class Controller_Frontend_About_Company extends \Controller_Frontend
{
	private $_module_url = '';
	private $_menu_key = 'about_us';
	private $_meta_slug = '/about-us/company';
	
	public function before() {
		parent::before();
	}
	
	public function action_index() {
		$this->set_meta_info($this->_meta_slug);
		$this->_data_template['frontend_menus'][$this->_menu_key]['menu_class_a'] = 'active';
        if (\Fuel\Core\Agent::is_mobiledevice())
        {
            return \Response::forge(\View::forge('pages::frontend/about/m_company.twig', $this->_data_template, FALSE));
        }
        else
        {
            return \Response::forge(\View::forge('pages::frontend/about/company.twig', $this->_data_template, FALSE));
        }
	}
}

