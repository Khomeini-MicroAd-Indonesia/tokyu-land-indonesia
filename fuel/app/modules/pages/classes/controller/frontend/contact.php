<?php
namespace Pages;

class Controller_Frontend_Contact extends \Controller_Frontend
{
	private $_module_url = '';
	private $_menu_key = 'contact';
	private $_meta_slug = '/contact-us';
	
	public function before() {
		parent::before();
	}
	
	public function action_index() {
            
            $this->set_meta_info($this->_meta_slug);
            $this->_data_template['frontend_menus'][$this->_menu_key]['menu_class_a'] = 'active';
            
            if (\Fuel\Core\Agent::is_mobiledevice())
            {
                return \Response::forge(\View::forge('pages::frontend/m_contact.twig', $this->_data_template, FALSE));
            }
            else
            {
                return \Response::forge(\View::forge('pages::frontend/contact.twig', $this->_data_template, FALSE));
            }
            
	}
        
            public function action_thank() {

                $this->set_meta_info($this->_meta_slug);
                $this->_data_template['frontend_menus'][$this->_menu_key]['menu_class_a'] = 'active';

                if (\Fuel\Core\Agent::is_mobiledevice())
                {
                    return \Response::forge(\View::forge('pages::frontend/m_thanks.twig', $this->_data_template, FALSE));
                }
                else
                {
                    return \Response::forge(\View::forge('pages::frontend/thanks.twig', $this->_data_template, FALSE));
                }

            }

}

