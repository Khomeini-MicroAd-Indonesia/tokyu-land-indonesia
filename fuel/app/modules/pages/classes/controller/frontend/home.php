<?php
namespace Pages;

use Fuel\Core\Fuel;

class Controller_Frontend_Home extends \Controller_Frontend
{
	private $_module_url = '';
	private $_menu_key = 'home';
	private $_meta_slug = '/';
	
	public function before() {
            parent::before();
	}
	
	public function action_index() {
            
            $this->set_meta_info($this->_meta_slug);
            $this->_data_template['project_data'] = \Project\Projectutil::get_project_data();
            $this->_data_template['project_img'] = \Project\Projectutil::get_project_img();
            $this->_data_template['news_data'] = \News\Newsutil::get_news_data($this->_current_lang);
            $this->_data_template['slide_img'] = \Slidemanagement\Slideutil::get_slide_img();
                
        // Load a platform specific view
        if (\Fuel\Core\Agent::is_mobiledevice())
        {
            return \Response::forge(\View::forge('pages::frontend/m_home.twig', $this->_data_template, FALSE));
        }
        else
        {
            return \Response::forge(\View::forge('pages::frontend/home.twig', $this->_data_template, FALSE));
        }
	}

    public function action_privacy(){
        if (\Fuel\Core\Agent::is_mobiledevice())
        {
            return \Response::forge(\View::forge('pages::frontend/m_privacy.twig', $this->_data_template, FALSE));
        }
        else
        {
            return \Response::forge(\View::forge('pages::frontend/privacy.twig', $this->_data_template, FALSE));
        }
    }

    public function action_terms(){

        if (\Fuel\Core\Agent::is_mobiledevice())
        {
            return \Response::forge(\View::forge('pages::frontend/m_terms.twig', $this->_data_template, FALSE));
        }
        else
        {
            return \Response::forge(\View::forge('pages::frontend/terms.twig', $this->_data_template, FALSE));
        }
    }

}