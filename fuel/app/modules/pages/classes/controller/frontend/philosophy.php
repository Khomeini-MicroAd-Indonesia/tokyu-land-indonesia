<?php
namespace Pages;

class Controller_Frontend_Philosophy extends \Controller_Frontend
{
	private $_module_url = '';
	private $_menu_key = 'philosophy';
	private $_meta_slug = '/philosophy';
	
	public function before() {
		parent::before();
	}
	
	public function action_index() {
		$this->set_meta_info($this->_meta_slug);
		$this->_data_template['frontend_menus'][$this->_menu_key]['menu_class_a'] = 'active';
        return \Response::forge(\View::forge('pages::frontend/philosophy.twig', $this->_data_template, FALSE));
	}
}

