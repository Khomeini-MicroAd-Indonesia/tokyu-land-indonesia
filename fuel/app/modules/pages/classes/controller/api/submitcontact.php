<?php

namespace Pages;

class Controller_API_SubmitContact extends \Controller_Frontend
{
    
    private $_module_url = '';
    private $_menu_key = 'pages';
	
    public function before() {
        $this->_check_lang = false;
        parent::before();
    }
    
    public function action_getSubmitContact(){
        $post_data = \Input::post();
        if (!empty($post_data)) {
            $data = array(
                'email_from' => array(
                    'email' => $post_data['mail'],
                    'name' => $post_data['name']
                ),
                'email_to' => \Config::get('config_basic.contact_us_email_to'),
                'email_subject' => '[Website tokyuland-id.com] Contact form submission: '.$post_data['name'],
                'email_reply_to' => array(
                    'email' => $post_data['mail'],
                    'name' => $post_data['name']
                ), // Optional
                'email_data' => array(
                    'base_url' => \Uri::base(),
                    'contact_name' => $post_data['name'],
                    'contact_email' => $post_data['mail'],
                    'contact_phone' => $post_data['num'],
                    'contact_address' => $post_data['addr'],
                    'contact_message' => $post_data['msg'],
                    'contact_stat' => $post_data['stat'],
                ),
                'email_view' => 'pages::email_contact_us.twig',
            );
            \Util_Email::queue_send_email($data);
        }else{
            var_dump($data); exit;
        }
        
        //return new \Response(json_encode($data),200);
    }
}
