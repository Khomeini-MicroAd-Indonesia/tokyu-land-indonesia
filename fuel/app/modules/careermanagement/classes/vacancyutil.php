<?php
namespace Careermanagement;

class Vacancyutil{

    /**
     * get vacancy data
     */
    
    public static function get_vacancy_data(){
        $vacancies = \Careermanagement\Model_Vacancies::query()
                ->where('status', 1)
                ->order_by('seq','ASC')
                ->get();
                
        $data = array();
        //$post_date;
        foreach ($vacancies as $vacancy){
            
            $data[] = array(
                'name'  => $vacancy->name,
                'code'  => $vacancy->code,
                'id'    => $vacancy->id,
                'req'   => $vacancy->req
            );
        }
        return $data;
    }
    
}

