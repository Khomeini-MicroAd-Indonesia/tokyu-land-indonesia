<?php
namespace Careermanagement;

class Model_Vacancies extends \Orm\Model {
	private $status_name = array('InActive', 'Active');
	
	protected static $_table_name = 'vacancies';

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'vacancies'=>array('before_insert'),
			'mysql_timestamp' => true
		),
		'Orm\Observer_UpdatedAt' => array(
			'vacancies'=>array('before_update'),
			'mysql_timestamp' => true
		),
		'Orm\Observer_Validation' => array(
			'vacancies'=>array('before_save')
		)
	);
        
	protected static $_properties = array(
		'id',
		'name' => array(
			'label' => 'Name',
			'validation' => array(
				'required',
				'max_length' => array(50),
			)
		),
                'code' => array(
			'label' => 'Code',
			'validation' => array(
				'required',
				'max_length' => array(20),
			)
		),
                'req' => array(
                        'label' => 'Requirement',
                        'validation' => array()
                ),
                'seq' => array(
                        'label' => 'Sequence',
                        'validation' => array()
                ),
		'status' => array(
			'label' => 'Status',
			'validation' => array(
				'required',
			)
		),
		'created_by',
		'created_at',
		'updated_by',
		'updated_at'
	);
	
	public function get_status_name() {
		$flag = $this->status;
		return isset($this->status_name[$flag]) ? $this->status_name[$flag] : '-';
	}
	
	public static function get_as_array ($filter=array()) {
            $items = self::find('all', $filter);
            if (empty($items)) {
                $data = array();
            } else {
                foreach ($items as $item) {
                $data[$item->id] = $item->name;
                }
            }
            return $data;
	}
	
	public function get_form_data_basic() {
		return array(
			'attributes' => array(
				'name' => 'frm_career_vacancy',
				'class' => 'form-horizontal',
				'role' => 'form',
				'action' => '',
				'method' => 'post',
			),
			'hidden' => array(),
			'fieldset' => array(
				array(
					'label' => array(
						'label' => 'Vacancy Name',
						'id' => 'vacancy_name',
						'attributes' => array(
							'class' => 'col-sm-2 control-label'
						)
					),
					'input' => array(
						'name' => 'vacancy_name',
						'value' => $this->name,
						'attributes' => array(
							'class' => 'form-control',
							'placeholder' => 'Vacancy Name',
							'required' => '',
						),
						'container_class' => 'col-sm-10'
					)
				),
                                array(
					'label' => array(
						'label' => 'Vacancy Code',
						'id' => 'vacancy_code',
						'attributes' => array(
							'class' => 'col-sm-2 control-label'
						)
					),
					'input' => array(
						'name' => 'vacancy_code',
						'value' => $this->code,
						'attributes' => array(
							'class' => 'form-control',
							'placeholder' => 'Vacancy Code',
							'required' => '',
						),
						'container_class' => 'col-sm-10'
					)
                                    ),
                                array(
					'label' => array(
						'label' => 'Vacancy Req',
						'id' => 'vacancy_req',
						'attributes' => array(
							'class' => 'col-sm-2 control-label'
						)
					),
					'textarea' => array(
						'name' => 'vacancy_req',
						'value' => $this->req,
						'attributes' => array(
							'class' => 'form-control ckeditor',
							'placeholder' => '',
						),
						'container_class' => 'col-sm-10'
					)
				),
				array(
					'label' => array(
						'label' => 'Status',
						'id' => 'status',
						'attributes' => array(
							'class' => 'col-sm-2 control-label'
						)
					),
					'select' => array(
						'name' => 'status',
						'value' => $this->status,
						'options' => $this->status_name,
						'attributes' => array(
							'class' => 'form-control',
							'placeholder' => 'Status',
							'required' => ''
						),
						'container_class' => 'col-sm-10'
					)
				),
                                array(
					'label' => array(
						'label' => 'Sequence',
						'id' => 'seq',
						'attributes' => array(
							'class' => 'col-sm-2 control-label'
						)
					),
					'input' => array(
						'name' => 'seq',
						'value' => $this->seq,
						'attributes' => array(
							'class' => 'form-control',
							'placeholder' => 'Sequence',
							'required' => '',
						),
						'container_class' => 'col-sm-10'
					)
                                ),
			)
		);
	}
}
