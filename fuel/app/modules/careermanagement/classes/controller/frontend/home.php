<?php
namespace Careermanagement;

class Controller_Frontend_Home extends \Controller_Frontend
{
    private $_module_url = '';
    private $_menu_key = 'career';
    private $_meta_slug = '/career';

    public function before() {
        parent::before();
    }

    public function action_index() {
        $this->set_meta_info($this->_meta_slug);
        $this->_data_template['vacancy_data'] = \Careermanagement\Vacancyutil::get_vacancy_data();
	$this->_data_template['frontend_menus'][$this->_menu_key]['menu_class_a'] = 'active';
        if (\Fuel\Core\Agent::is_mobiledevice())
        {
            return \Response::forge(\View::forge('careermanagement::frontend/m_career.twig', $this->_data_template, FALSE));
        }
        else
        {
            return \Response::forge(\View::forge('careermanagement::frontend/career.twig', $this->_data_template, FALSE));
        }
    }

    public function action_submit() {
        
        $vacancy_id = $this->param('vacancy_id');
        $vacancy_model = Model_Vacancies::find($vacancy_id);
        
        if (empty($vacancy_model)) {
            throw new \HttpNotFoundException;
        }
        
        $post_file = \Input::file();
        
        if(!empty($post_file)){
         
            $file = $post_file['cv_file'];
            $destination_path = DOCROOT.'media'.DS.'applicant'.DS.date('Y').DS.date('m').DS;
            
            if (@is_dir($destination_path) || @mkdir($destination_path, 0777, TRUE)) {
                // To make unique filename
                $ext = pathinfo($file['name'], PATHINFO_EXTENSION);
                $base_filename = basename($file['name'], '.'.$ext);
                $new_filename = $base_filename.date('d_His').$ext;
                
                $target_path = $destination_path . $new_filename;
                move_uploaded_file($file['tmp_name'], $target_path);

                $data = array(
                    'email_from' => 'no-reply@tokyu-land.co.id',
                    'email_to' => \Config::get('config_basic.career_email_to'),
                    'email_subject' => '[Website tokyuland-id.com] CV submission ('.$vacancy_model->name.')',
                    'email_data' => array(
                        'base_url' => \Uri::base(),
                        'vacancy_name' => $vacancy_model->name,
                        'vacancy_code' => $vacancy_model->code,
                    ),
                    'email_view' => 'pages::email_career.twig',
                    'email_attachment' => array(
                        $target_path
                    )
                );
                \Util_Email::queue_send_email($data);
                
                $redirect = \Uri::base().$this->_current_lang.'/career/'.$vacancy_id.'/thanks';
                return \Response::redirect($redirect);
            }
        }
        
        $this->set_meta_info($this->_meta_slug);
        $this->_data_template['frontend_menus'][$this->_menu_key]['menu_class_a'] = 'active';
        $this->_data_template['vacancy_model'] = $vacancy_model;
        
        return \Response::forge(\View::forge('careermanagement::frontend/submit.twig', $this->_data_template, FALSE));
    }

    public function action_thank() {
        $vacancy_id = $this->param('vacancy_id');
        $vacancy_model = Model_Vacancies::find($vacancy_id);
        
        $this->set_meta_info($this->_meta_slug);
	$this->_data_template['frontend_menus'][$this->_menu_key]['menu_class_a'] = 'active';
        $this->_data_template['vacancy_model'] = $vacancy_model;
        
        return \Response::forge(\View::forge('careermanagement::frontend/thanks.twig', $this->_data_template, FALSE));
    }

}

