<?php
/**
 * Created by PhpStorm.
 * User: piso
 * Date: 6/4/15
 * Time: 6:12 PM
 */

return array(

    'fodusan-109' => 'Tokyu Fudosan Holdings has taken a proactive stance toward spin-offs into separate subsidiaries, in order to develop a high level of specialization in all of its business activities. Currently, nearly 109 group companies continue to tackle challenges in their respective business domains.',
    'fodusan-19000' => 'There are more than 19,000 employees at Tokyu Fudosan Holdings. Each one is a business professional committed to perseverance and facing challenges, while keeping a vision of the future in mind for the customer.',
    'fodusan-88000' => 'Founded upon the concepts of safety, security, and comfort, we build about 2,100 houses and condominiums in Japan for sale per year that fulfil the dreams of every single customer. Until now, we have sold a total of 88,000 residences. Going forward, we will continue building houses and condominiums that represent our steadfast values.',
    'fodusan-59' => 'Tokyu Land Corporation’s office building business manages 59 office buildings in the heart of the capital, and has been one of our core businesses since the company was established. We are committed to ensuring safety, security, and comfort; and we provide many diverse values in addition to supporting the needs of an office.',
    'fodusan-646' => 'We develop office buildings based on both “hard” and “soft” aspects. This lets us create buildings that can maximize a business’s potential while remaining environmentally and people-friendly. We have expanded our office leasing business to cover a total surface area of about 646,000 Square meter.',
    'fodusan-7' => 'Tokyu Fudosan Holdings made consolidated net sales of US$7.8 billion for fiscal 2015. Heading into the future, we will use the Group’s management resources to the best of our ability, further evolve our business model, and continue to create new value while always keeping future generations in mind.',


    'land-comfort' => 'We pursue developing office buildings from both “hard” and “soft” aspects, in order to create buildings that can maximize a business’s potential while remaining environmentally and people-friendly. Various projects that will unlock the future of businesses are in progress.',
    'land-prov' => 'Japan now faces the issue of a rapidly-aging society, and naturally there is growing interest in providing quality living for senior citizens. Tokyu Fudosan has developed Senior Residences which provide hotel-like services, so that senior citizens can live in security into the future. It has also developed Care Residences as well, which come with nursing care services for those needing regular care throughout the day. We guarantee secure homes that last a lifetime, and provide assistance for healthy living.',
    'land-resorts' => 'We want to add color and flavor to your busy daily lives, helping you enjoy a richer life. We at Tokyu Fudosan have developed our resort business based on the basic philosophy of “coexistence and harmony with nature and people.” In order to respond to a broad range of diverse values and lifestyles, we are developing resorts and leisure facilities soon to be in demand by consumers. These include our resort complex “Tokyu Resort Town,” as well as other exclusive members-only hotels, golf courses, ski resorts, and holiday homes and villas.',
    'land-bold' => 'The rapid development of IT, and the fall in production and working populations, have brought about improvements in productivity of business people, as well as a shift toward variable costs for the office. In light of this, Business Airport is our members-only satellite office that aims to respond to the social needs that companies confront. Similar to an international airport where all types of people come to depart on flights, we seek to help our customers’ businesses take flight. In addition to providing a comfortable business space, we also propose a new style of working for the future.',
    'land-commercial' => 'Based on our track record of success in urban development, and by harnessing the power of the Group network, we provide total planning and support services for commercial facilities, spanning everything from planning and development to management. We are committed to creating facilities that harmonize with the city, are rooted in the community, and which will grow and evolve together with the region. Currently, we are involved in projects that aim to further enhance the value of Tokyo.',
    'land-homes' => 'Founded upon the concepts of safety and security, we create homes that can fulfill the dreams and desires of each and every customer. Beginning with our condominium and landed housing brand “BRANZ”, which provides sophisticated residential spaces of high quality, we provide homes for the future, such as rental condominium units which let residents enjoy smart, urban lifestyles.',
    'land-overseas' => 'In the 40 years since the company entered the Indonesia market, Tokyu Fudosan has turned its eyes to the world and its future. Since 2013, it has invested in various types of real estate from its base in Los Angeles, including creative offices in Los Angeles and apartments in Houston. While entering into cooperative businesses with Japanese and local companies, it aims to steadily continue expanding its business into the future. It has also launched condominium projects in China, in Qingdao and Dalian; as well as complex development projects in Shenyang, Liaoning Province. Tokyu Fudosan will continue to anticipate what the world and our lifestyles will be like in the next 10 or 20 years, and expand its scope of challenges across the world.',


    'philosophy-top1' => 'TOKYU LAND INDONESIA was established to inherit the technology and spirit that built the Japan we know today. Our mission is to create new lifestyles by uniting various values, while protecting the climate and culture of Indonesia. We seek to provide for the future of Indonesia through urban development.<br/><br/>',
    'philosophy-top2' => 'Our aim is to be a completely unique real estate developer, honest in helping Indonesia achieve development.Innovation doesn’t happen with compromise.',
    'philosophy-top3' => 'Our challenge begins, based on pushing ourselves to the limit.We will strive to meet your expectations.',
    'philosophy-left' => 'Since ancient times, shown by the traditions of judo, flower arrangement, tea ceremony, and calligraphy, the Japanese people have referred to skill-learning and improvement of the mind as “the way.” The Japanese people take pride in mastering the secrets of “the way.” Urban development is but one of “the ways.” We seek to add to Indonesia’s development with an uncompromising spirit, nurtured through the traditions and culture of Japan.',
    'philosophy-right1' => 'Since Tokyu Land Corporation launched its business here in 1975, we have forged deep, strong bonds with Indonesia. ',
    'philosophy-right2' => 'Throughout our history here, we have endeavored to put down roots in this country while loving its people, understanding the lifestyle and culture, and cherishing the rich blessings of nature.',
    'philosophy-right3' => 'By combining our know-how as a Japanese corporation with our knowledge accumulated over a long period of time, and through urban development that only we are capable of realizing, we seek to contribute to Indonesia’s development.',
    'philosophy-bottom1' => 'Even within Asia, which continues to rapidly develop, Indonesia is one of the countries in the region that has drawn incredible attention.',
    'philosophy-bottom2' => 'It welcomes a wide variety of visitors as a business and cultural hub. Hence, we aim to create suitable homes for a country that will support Asia’s future.',
    'philosophy-bottom3' => 'From our base here in Indonesia, we will propose a style of premium residences for the coming future.',

    'head-director' => 'Keeping our goal of contributing to Indonesia’s development in mind, aiming high, and seeking business continuity.',
    'director1' => 'TOKYU LAND INDONESIA is a member of Tokyu Fudosan Holdings, a comprehensive real estate developer representative of Japan. The ties between Indonesia and Tokyu Land Corporation go all the way back to 1975 in Bandung. In 1981, we expanded our business to Jakarta. Until now, we have created a track record of supplying 4,500 units of landed house.',
    'director2' => 'TOKYU LAND INDONESIA was established in 2012 with the aim of harnessing the knowhow and trust built up over our 40-year history, as well as our wealth of experience as a comprehensive developer in Japan and the Group’s collective strength, to develop pioneering new businesses in Indonesia. Soon after its establishment, the company launched the condominium brand “BRANZ” that it has developed in Japan, in Indonesia in 2015. This was the first time that a Japanese company had developed a Japanese condominium brand overseas, and represented our strong will to put down roots in Indonesia as well as to grow and develop alongside with the country. In addition to the condominium sales business, we also aim to tackle the challenges of the leasing and management businesses going forward. It is our desire to give even more to Indonesia by creating new value through fusing the needs of customers in Indonesia with the high quality offered by Japan.',
    'director3' => 'Noboru Goto, the first President of Tokyu Land Corporation, once commented, “The 21st century will be the era of the Pacific, and the era of ASEAN.” As his words become reality today, we will continue to develop our businesses while staying rooted in the region and experiencing that passionate energy.',

    'history1' => 'Tokyu Land Corporation was established in 1953, and to date, we have steadily built up a track record of success. It foresaw the arrival of the “Asian era” early on, and first entered the Indonesia market in 1975. A chaotic social crisis that arose as a result of political instability did not deter it.',
    'history2' => 'Instead business continued, and it contributed to the development of Indonesia by building a cumulative total of more than 4,500 houses for sale. Going forward, Tokyu Land Corporation will continue to face new challenges for the future of Japan and Indonesia.',

    'csr1' => 'Indonesia, which is made up of more than 17,500 islands, is currently undergoing serious environmental destruction. TOKYU LAND INDONESIA is actively involved in mangrove planting initiatives on the coastal regions of Jakarta, aiming to fulfill our role and responsibility as a corporate citizen expanding business in Indonesia, and to raise employees’ awareness toward environmental issues. ',
    'csr2' => 'NATURAL DISASTER MITIGATION <br/> TOKYU LAND INDONESIA CSR',
    'csr3' => 'Indonesia has the greatest number of mangrove forests in the world. Mangrove forests are effective for the environment in various ways, such as coastal defense and protecting biodiversity. <br/> <br/> By protecting these resources, our goal is to work together, united as a company, in order to make a sustainable society a reality. ',
    'csr4' => '<b>December 05, 2015</b><br/>PT.Tokyu Land Indonesia (“TLID”), as Property Developer company prove its commitment to contribute inconservation of natural environment through the second times activity of mangrove planting in Taman Wisata Alam –Angke Kapuk, North Jakarta. Coinciding TLID&#39;s 3rdAnniversary,',
    'csr5' => 'this Corporate Social Responsibility program with theme "Grow Mangrove, Grow Life" aims to strengthen the bond between the employees working together to conserve Indonesia&#39;s rich natural environment and biodiversity and to realize a sustainable community for the future.',
    'csr6' => 'TLID employees and their family of 71 persons<br/>
               participated in this annual program.<br/>
               The program started with an educational briefing about mangrove ecosystem.<br/>',
    'csr7' => '<div style="padding: 60px; padding-top: 82px; height: 267px; font-size:12px; background:#f2f2f2; line-height: 2.5;">
                    Entering muddy water and planting mangrove together bonded all participants and left them with an interesting experience. TLID and the employees hoped to continue their contribution to the Indonesian community and nature in and outside of Jakarta.
                    </div>',

    'information' => '<div class="information-detail">We are pleased to inform you that the new office will be effective from June 20, 2016.<br/> Our company Head Office has been relocated to new building as follows address. <br/></div>',
    'information-where' => '<span>Our new building location at:</span><br/>',


    'group-hands' => '<h5>Creator of colorful lifestyles</h5><p>TOKYU HANDS INC. offers a wide range of products to meet diverse needs, including basic goods that are rooted in the everyday lives of its customers. It considers all of its products to be “materials” for enriching the lives of customers, and it has strived to build up a rich line-up of such “materials.” This wealth of “materials” represents precisely the product appeal of TOKYU HANDS. Through this product appeal, the company aims to bring to its customers the joy of discovering objects and ideas that are unique to TOKYU HANDS.</p>',
    'group-livable' => '<h5>Meeting all needs for real estate</h5><p>Tokyu Livable runs a comprehensive real estate logistics business, whose four pillars are real estate brokerage, real estate solutions, leasing, and sales contracting. Through cooperation between the respective businesses, Tokyu Livable responds to all real estate needs, from housing to utilization and investment in real estate. By providing support for the real estate strategies of companies, and making rich housing lives for customers a reality, the company aims to contribute to the development and stability of society.</p>',
    'group-comm' => '<h5>Maintaining and improving the permanent value of real estate</h5><p>Tokyu Community has an established track record of success in managing a wide range of facilities, which include condominiums, buildings, and public facilities. Besides providing comfortable environments for residents and users alike, it also provides support for the foundation of building management by maintaining asset value, as well as working on the maintenance of facility functions. As your ideal partner in creating future value which supports safe and secure lifestyles and the business environment, we propose optimal management solutions to maintain and improve the permanent value of real estate. </p>',
    'group-oasis' => '<h5>Supporting healthy lifestyles</h5><p>Through its business of fitness club management and accompanying subsidiary businesses, Tokyu Sports Oasis proposes and provides healthy lifestyles for those wishing to maintain a healthy and comfortable body and mind. Tokyu Sports Oasis offers a rich variety of programs to help people remain energetic and full of stamina, and contributes to creating an enriched society. </p>',
    'group-stay' => '<h5>Supporting comfortable long stays in Tokyo</h5><p>We aim to deliver secure and comfortable Tokyo lifestyles to guests visiting the city for business or tourism. 16 Tokyu Stay hotels are located in convenient locations, providing easy access to key places across Tokyo. Spacious rooms, washers and dryers, mini-kitchens, and a rich line-up of other facilities and services are available in our long-term and consecutive stay hotels, which provide support for guests on tourism and business trips. </p>',
    'group-homes' => '<h5>From new homes to renovations—Rich lifestyle proposals</h5><p>Tokyu Homes is a corporation providing comprehensive housing and living solutions. It is capable of proposing and implementing rich lifestyles for customers, including building new homes, renovations, interior decoration, and residence maintenance. Through our experience and knowhow built up over many years, the company is expanding its business beyond housing to the renovation, maintenance, and management support of managed facilities, as well as the asset utilization business. </p>',

    'privacy' => '<strong>Collection and Use of Personal Information</strong><br/><br/>

                Personal information is data that can be used to identify or contact a single person.<br/>
                You may be asked to provide your personal information anytime you are in contact with PT. Tokyu Land Indonesia or affiliated company and Tokyu Fudosan Holding. PT. Tokyu Land Indonesia and its affiliates may share this personal information with each other and use it consistent with this Privacy Policy. They may also combine it with other information to provide and improve our products, services, content, and advertising. You are not required to provide the personal information that we have requested, but, if you chose not to do so, in many cases we will not be able to provide you with our products or services or respond to any queries you may have.<br/><br/>

                Here are some examples of the types of personal information which PT. Tokyu Land Indonesia may collect and how we may use it.<br/>
                What personal information we collect<br/><br/>

                <ul>
                    <li>
                        Contact us or participate in an online survey, we may collect a variety of information, including your name, mailing address, phone number, email address and contact preferences.
                    </li>
                    <li>
                        When you share your content with family and friends using PT. Tokyu Land Indonesia products, or invite others to participate in PT. Tokyu Land Indonesia services or forums, PT. Tokyu Land Indonesia may collect the information you provide about those people such as name, mailing address, email address, and phone number. PT. Tokyu Land Indonesia will use such information to fulfill your requests, provide the relevant product or service, or for anti-fraud purposes.
                    </li>
                </ul>

                <strong>How we use your personal information</strong>

                <ul>
                    <li>
                        The personal information we collect allows us to keep you posted on PT. Tokyu Land Indonesia’s latest product announcements and upcoming events.
                    </li>
                    <li>
                        We also use personal information to help us create, develop, operate, deliver, and improve our products, services, content and advertising, and for loss prevention and anti-fraud purposes.
                    </li>
                    <li>
                        We may use your personal information, including date of birth, to verify identity, assist with identification of users, and to determine appropriate services. For example, we may use date of birth to determine your age.
                    </li>
                    <li>
                        From time to time, we may use your personal information to send important notices, such as communications about purchases and changes to our terms, conditions, and policies. Because this information is important to your interaction with PT. Tokyu Land Indonesia, you may not opt out of receiving these communications.
                    </li>
                    <li>
                        We may also use personal information for internal purposes such as auditing, data analysis, and research to improve PT. Tokyu Land Indonesia’s products, services, and customer communications.
                    </li>
                    <li>
                        If you enter into a sweepstake, contest, or similar promotion we may use the information you provide to administer those programs.
                    </li>
                </ul>

                <strong>Collection and Use of Non-Personal Information</strong><br/><br/>

                We also collect data in a form that does not, on its own, permit direct association with any specific individual. We may collect, use, transfer, and disclose non-personal information for any purpose. The following are some examples of non-personal information that we collect and how we may use it :

                <ul>
                    <li>
                        We may collect information such as occupation, language, zip code, area code, unique device identifier, referrer URL, location, and the time zone where PT. Tokyu Land Indonesia product is used so that we can better understand customer behavior and improve our products, services, and advertising.
                    </li>
                    <li>
                        We may collect information regarding customer activities on our website, and from our other products and services. This information is aggregated and used to help us provide more useful information to our customers and to understand which parts of our website, products, and services are of most interest.
                    </li>
                    <li>
                        We may collect and store details of how you use our services, including search queries. This information may be used to improve the relevancy of results provided by our services. Except in limited instances to ensure quality of our services over the Internet, such information will not be associated with your IP address.
                    </li>
                </ul>

                If we do combine non-personal information with personal information, the combined information will be treated as personal information for as long as it remains combined.<br/><br/>

                <strong>Cookies and Other Technologies</strong><br/><br/>

                PT. Tokyu Land Indonesia’s websites, online services, interactive applications, email messages, and advertisements may use “cookies” and other technologies such as pixel tags and web beacons. These technologies help us better understand user behavior, tell us which parts of our websites people have visited, and facilitate and measure the effectiveness of advertisements and web searches. We treat information collected by cookies and other technologies as non-personal information. However, to the extent that Internet Protocol (IP) addresses or similar identifiers are considered personal information by local law, we also treat these identifiers as personal information. Similarly, to the extent that non-personal information is combined with personal information, we treat the combined information as personal information for the purposes of this Privacy Policy.<br/><br/>

                PT. Tokyu Land Indonesia and its partners use cookies and other technologies in mobile advertising services to control the number of times you see a given ad, deliver ads that relate to your interests, and measure the effectiveness of ad campaigns.<br/><br/>

                As is true of most internet services, we gather some information automatically and store it in log files. This information includes Internet Protocol (IP) addresses, browser type and language, Internet service provider (ISP), referring and exit websites and applications, operating system, date/time stamp, and clickstream data.<br/><br/>

                We use this information to understand and analyze trends, to administer the site, to learn about user behavior on the site, to improve our product and services, and to gather demographic information about our user base as a whole. PT. Tokyu Land Indonesia may use this information in our marketing and advertising services.<br/><br/>

                In some of our email messages, we use a “click-through URL” linked to content on the PT. Tokyu Land Indonesia website. When customers click one of these URLs, they pass through a separate web server before arriving at the destination page on our website. We track this click-through data to help us determine interest in particular topics and measure the effectiveness of our customer communications. If you prefer not to be tracked in this way, you should not click text or graphic links in the email messages.<br/><br/>

                Pixel tags enable us to send email messages in a format customers can read, and they tell us whether mail has been opened. We may use this information to reduce or eliminate messages sent to customers.<br/><br/>




                <strong>Disclosure to Third Parties</strong><br/><br/>

                At times PT. Tokyu Land Indonesia may make certain personal information available to strategic partners that work with PT. Tokyu Land Indonesia to provide products and services, or that help PT. Tokyu Land Indonesia market to customers. Personal information will only be shared by PT. Tokyu Land Indonesia to provide or improve our products, services and advertising; it will not be shared with third parties for their marketing purposes.<br/><br/><br/>




                <strong>Service Providers</strong><br/><br/>

                PT. Tokyu Land Indonesia shares personal information with companies who provide services such as information processing, extending credit, fulfilling customer orders, delivering products to you, managing and enhancing customer data, providing customer service, assessing your interest in our products and services, and conducting customer research or satisfaction surveys. We ensure you that these companies are obligated to protect your information.<br/><br/><br/>




                <strong>Others</strong><br/><br/>

                It may be necessary − by law, legal process, litigation, and/or requests from public and governmental authorities − for PT. Tokyu Land Indonesia to disclose your personal information. We may also disclose information about you if we determine that for purposes of national security, law enforcement, or other issues of public importance, disclosure is necessary or appropriate.<br/><br/>

                We may also disclose information about you if we determine that disclosure is reasonably necessary to enforce our terms and conditions or protect our operations or users. Additionally, in the event of a reorganization, merger, or sale we may transfer any and all personal information we collect to the relevant third party.<br/><br/><br/>




                <strong>Protection of Personal Information</strong><br/><br/>

                PT. Tokyu Land Indonesia takes the security of your personal information very seriously. PT. Tokyu Land Indonesia online services such as the http://www.tokyuland-id.com/ protect your personal information during transit using encryption. When your personal data is stored by PT. Tokyu Land Indonesia, we use computer systems with limited access housed in facilities using physical security measures.<br/><br/>

                When you use some PT. Tokyu Land Indonesia products, services, or social networking service, the personal information and content you share is visible to other users and can be read, collected, or used by them. You are responsible for the personal information you choose to share or submit in these instances. For example, if you list your name and email address in a forum posting, that information is public. Please take care when using these features.<br/><br/><br/>




                <strong>Integrity and Retention of Personal Information</strong><br/><br/>

                PT. Tokyu Land Indonesia makes it easy for you to keep your personal information accurate, complete, and up to date. We will retain your personal information for the period necessary to fulfill the purposes outlined in this Privacy Policy unless a longer retention period is required or permitted by law.<br/><br/><br/>




                <strong>Location-Based Services</strong><br/><br/>

                To provide location-based services on PT. Tokyu Land Indonesia products, PT. Tokyu Land Indonesia and our partners and licensees may collect, use, and share precise location data, including the real-time geographic location of your computer or device. Where available, location-based services may use GPS, Bluetooth, and your IP Address, along with crowd-sourced Wi-Fi hotspot and cell tower locations, and other technologies to determine your devices’ approximate location. Unless you provide consent, this location data is collected anonymously in a form that does not personally identify you and is used by PT. Tokyu Land Indonesia and our partners and licensees to provide and improve location-based products and services. For example, your device may share its geographic location with application providers when you opt in to their location services.<br/><br/><br/>




                <strong>Third-Party Sites and Services</strong><br/><br/>

                PT. Tokyu Land Indonesia websites, products, applications, and services may contain links to third-party websites, products, and services. We encourage you to learn about the privacy practices of those third parties.<br/><br/><br/>




                <strong>Our Companywide Commitment to Your Privacy</strong><br/><br/>

                To make sure your personal information is secure, we communicate our privacy and security guidelines to PT. Tokyu Land Indonesia employees and strictly enforce privacy safeguards within the company.<br/><br/><br/>




                <strong>Privacy Questions</strong><br/><br/>

                If you have any questions or concerns about PT. Tokyu Land Indonesia’s Privacy Policy or data processing or if you would like to make a complaint about a possible breach of local privacy laws, please contact us. You can always contact us by phone at the relevant PT. Tokyu Land Indonesia Support number.<br/><br/>

                All such communications are examined and replies issued where appropriate as soon as possible. If you are unsatisfied with the reply received, you may refer your complaint to the relevant regulator in your jurisdiction. If you ask us, we will endeavor to provide you with information about relevant complaint avenues which may be applicable to your circumstances.<br/><br/>

                PT. Tokyu Land Indonesia may update its Privacy Policy from time to time. When we change the policy in a material way, a notice will be posted on our website along with the updated Privacy Policy.<br/><br/>',


    'terms' => '

                The http://www.tokyuland-id.com/ Web Site (the "Site") is an online information service provided by PT.Tokyu Land Indonesia ("PT. TLID"), subject to your compliance with the terms and conditions set forth below. Please read this document carefully before accessing or using the site. By accessing or using the site, you agree to be bound by the terms and conditions set forth below. If you do not wish to be bound by these terms and conditions, you may not access or use the site. PT. TLID may modify this agreement at any time, and such modifications shall be effective immediately upon posting of the modified agreement on the site. You agree to review the agreement periodically to be aware of such modifications and your continued access or use of the site shall be deemed your conclusive acceptance of the modified agreement.<br/><br/><br/>


                <p style="padding-left: 3em;">

                    <strong>Copyright, Licenses and Idea Submissions.</strong><br/><br/>

                    The entire contents of the Site are protected by international copyright and trademark laws. The owner of the copyrights and trademarks are PT. TLID, its affiliates or other third party licensors. You may not modify, copy, reproduce, republish, upload, post, transmit, or distribute, in any manner, the material on the site, including text, graphics, code and/or software. You may print and download portions of material from the different areas of the Site solely for your own non-commercial use provided that you agree not to change or delete any copyright or proprietary notices from the materials. You agree to grant to PT. TLID a non-exclusive, royalty-free, worldwide, perpetual license, with the right to sub-license, to reproduce, distribute, transmit, create derivative works of, publicly display and publicly perform any materials and other information (including, without limitation, ideas contained therein for new or improved products and services) you submit to any public areas of the Site (such as bulletin boards, forums and newsgroups) or by e-mail to the Site by all means and in any media now known or hereafter developed. You also grant to PT. TLID the right to use your name in connection with the submitted materials and other information as well as in connection with all advertising, marketing and promotional material related thereto. You agree that you shall have no recourse against PT. TLID for any alleged or actual infringement or misappropriation of any proprietary right in your communications to PT. TLID.</p>

                <p style="padding-left: 1em;">
                    <span style="font-size: 14px; text-transform: uppercase;">Trademark</span><br/><br/>
                    Publications, products, content or services referenced herein or on the Site are the exclusive trademarks or service marks of PT. TLID. Other product and company names mentioned in the Site may be the trademarks of their respective owners.<br/><br/>
                </p>



                <p style="padding-left: 3em;">
                    <strong>Use of the Site</strong><br/><br/>

                    You understand that, except for information, products or services clearly identified as being supplied by PT. TLID, the Site  does not operate, control or endorse any information, products or services on the Internet in any way. Except for the Site does identified information, products or services, all information, products and services offered through the Site or on the Internet generally are offered by third parties, that are not affiliated with PT. TLID. You also understand that PT. TLID cannot and does not guarantee or warrant that files available for downloading through the Site will be free of infection or viruses, worms, Trojan horses or other code that manifest contaminating or destructive properties. You are responsible for implementing sufficient procedures and checkpoints to satisfy your particular requirements for accuracy of data input and output, and for maintaining a means external to the Site for the reconstruction of any lost data.<br/><br/>

                    Total responsibility and risk for your use of the site and the internet. PT. TLID provides the site and related information "as is" and does not make any express or implied warranties, representations or endorsements whatsoever (including without limitation warranties of title or noninfringement, or the implied warranties of merchantability or fitness for a particular purpose) with regard to the service, any merchandise information or service provided through the service or on the internet generally, and pt. TLID shall not be liable for any cost or damage arising either directly or indirectly from any such transaction. It is solely your responsibility to evaluate the accuracy, completeness and usefulness of all opinions, advice, services, merchandise and other information provided through the service or on the internet generally. PT. TLID does not warrant that the service will be uninterrupted or error-free or that defects in the service will be corrected.<br/><br/>

                    You understand further that the pure nature of the internet contains unedited materials or may be offensive to you. Your access to such materials is at your risk. PT. TLID has no control over and accepts no responsibility whatsoever for such materials.<br/><br/>

                </p>
                <p style="padding-left: 1em;">
                    <span style="font-size: 14px; text-transform: uppercase;">LIMITATION OF LIABILITY</span><br/><br/>
                    In no event will pt. TLID be liable for (i) any incidental, consequential, or indirect damages (including, but not limited to, damages for loss of profits, business interruption, loss of programs or information, and the like) arising out of the use of or inability to use the service, or any information, or transactions provided on the service, or downloaded from the service, or any delay of such information or service. Even if pt. TLID or its authorized representatives have been advised of the possibility of such damages, or (ii) any claim attributable to errors, omissions, or other inaccuracies in the service and/or materials or information downloaded through the service. Because some states do not allow the exclusion or limitation of liability for consequential or incidental damages, the above limitation may not apply to you. In such states,  pt. TLID liability is limited to the greatest extent permitted by law.<br/><br/>

                    PT. TLID makes no representations whatsoever about any other web site which you may access through this Site or which may link to this Site. When you access other web site, please understand that it is independent from the Site, and that PT. TLID has no control over the content on that web site. In addition, a link to a PT. TLID web site does not mean that PT. TLID endorses or accepts any responsibility for the content, or the use, of such web site.<br/><br/>
                </p>

                <p style="padding-left: 3em;">
                    <strong>Indemnification</strong><br/><br/>

                    You agree to indemnify, defend and hold harmless PT. TLID, its officers, directors, employees, agents, licensors, suppliers and any third party information providers to the Site from and against all losses, expenses, damages and costs, including reasonable attorneys&#39; fees, resulting from any violation of this Agreement (including negligent or wrongful conduct) by you or any other person accessing the Site.<br/><br/><br/>

                    <strong>Third Party Rights</strong><br/><br/>

                    The provisions of paragraphs 2 (Use of the Service), and 3 (Indemnification) are for the benefit of PT. TLID and its officers, directors, employees, agents, licensors, suppliers, and any third party information providers to the Site. Each of these individuals or entities shall have the right to assert and enforce those provisions directly against you on its own behalf.<br/><br/><br/>

                    <strong>Term</strong><br/><br/>

                    This Agreement is effective and prevail to the extent that you are accessing and using the Site.<br/><br/><br/>

                    <strong>Miscellaneous</strong><br/><br/>

                    This Agreement shall all be governed and construed in accordance with the laws of Indonesia applicable to agreements made and to be performed in Indonesia. You agree that any legal action or proceeding between PT. TLID and you for any purpose concerning this Agreement or the parties&#39; obligations hereunder shall be brought exclusively in a federal or state court of competent jurisdiction sitting in Indonesia .<br/><br/><br/>
                </p>',
);