<?php
/**
 * Created by PhpStorm.
 * User: piso
 * Date: 6/4/15
 * Time: 6:13 PM
 */

return array(

    'fodusan-109' => 'Tokyu Fudosan Holdings telah mengambil sikap proaktif melalui berbagai anak perusahaannya dalam rangka mengembangkan spesialisasi tingkat tinggi dalam semua kegiatan bisnisnya. Saat ini, sebanyak hampir 109 perusahaan dalam Tokyu Group terus bekerja untuk mengatasi semua tantangan dalam area bisnis mereka masing-masing.',
    'fodusan-19000' => 'Tokyu Fudosan Holdings memiliki lebih dari 19.000 karyawan. Masing-masing dari mereka adalah pekerja profesional yang memiliki komitmen untuk bekerja secara tekun dan bersedia menghadapi segala tantangan sambil tetap mengingat visi masa depan untuk pelanggan. ',
    'fodusan-88000' => 'Didirikan berdasarkan konsep keselamatan, keamanan, dan kenyamanan, setiap tahun kami membangun sekitar 2.100 rumah dan kondominium di Jepang untuk mewujudkan impian setiap pelanggan. Hingga saat ini, kami sudah menjual sebanyak 88.000 tempat tinggal. Selanjutnya, kami akan terus membangun rumah dan kondominium yang mewakili nilai-nilai mantap kami.',
    'fodusan-59' => 'Bisnis gedung perkantoran Tokyu Land Corporation mengelola 59 gedung perkantoran di jantung ibukota dan telah menjadi salah satu bisnis inti kami sejak perusahaan ini didirikan. Kami berkomitmen terhadap keselamatan, keamanan, dan kenyamanan; dan kami memberikan berbagai macam nilai tambah selain memenuhi kebutuhan kantor.',
    'fodusan-646' => 'Kami mengembangkan gedung perkantoran berdasarkan aspek "keras" maupun "lunak". Hal ini memungkinkan kami membangun bangunan-bangunan yang dapat memaksimalkan potensi bisnis dan pada saat yang sama tetap ramah lingkungan dan ramah pengguna. Kami telah menambah luas bisnis sewa kantor kami menjadi sekitar 646.000 meter persegi secara keseluruhan.',
    'fodusan-7' => 'Pada tahun fiskal 2015 Tokyu Fudosan Holdings membukukan penjualan bersih gabungan sebesar USD 7,8 miliar. Ke depan, kami akan menggunakan sebaik mungkin sumber daya manajemen yang dimiliki oleh Tokyu Group untuk terus mengembangkan model bisnis dan terus menciptakan nilai-nilai baru dengan tetap memperhatikan kelangsungan generasi masa depan.',


    'land-comfort' => 'Kami selalu berusaha untuk mengembangkan gedung-gedung perkantoran dari aspek "keras" dan "lunak" agar mampu membuat bangunan yang dapat memaksimalkan potensi bisnis namun tetap ramah lingkungan dan ramah pengguna. Saat ini berbagai proyek yang akan membuka usaha di masa depan sedang berjalan.',
    'land-prov' => 'Saat ini Jepang sedang menghadapi masalah penuaan penduduk dan dengan sendirinya terdapat kebutuhan akan kehidupan yang berkualitas bagi manula. Tokyu Fudosan telah membangun “Senior Residence” yang memiliki layanan seperti hotel sehingga para manula dapat hidup dengan rasa aman. Perusahaan juga telah mengembangkan “Care Residence” yang memiliki layanan perawatan bagi para manula yang membutuhkan perawatan rutin setiap saat. Kami memastikan tempat tinggal yang aman yang dapat dinikmati seumur hidup, dan mendukung terciptanya hidup yang sehat.',
    'land-resorts' => 'Kami ingin menambah warna dan rasa lain dalam keseharian Anda yang sibuk, sehingga Anda dapat menikmati kehidupan yang lebih mewah. Tokyu Fudosan mengembangkan bisnis resor berdasarkan filosofi dasar yaitu "hidup secara berdampingan dan harmonis dengan alam dan manusia." Untuk menggabungkan nilai dan gaya hidup yang beragam, saat ini kami sedang mengembangkan fasilitas resor dan rekreasi yang nantinya akan dibutuhkan oleh konsumen. Antara lain komplek resor "Tokyu Resort Town," hotel, lapangan golf, resort ski, rumah berlibur dan villa eksklusif yang hanya diperuntukkan bagi anggota.',
    'land-bold' => 'Pesatnya perkembangan IT, penurunan produksi dan jumlah tenaga kerja telah membawa perbaikan produktivitas pebisnis, serta pergeseran ke arah biaya variabel untuk perkantoran. Berdasarkan pertimbangan ini, kami menciptakan Business Airport, suatu kantor satelit yang diperuntukkan bagi anggota demi merespon kebutuhan sosial yang dihadapi oleh perusahaan. Serupa dengan suatu bandara internasional di mana bermacam-macam orang datang untuk terbang, kami berusaha membantu pelanggan bisnis kami untuk melakukan penerbangan. Selain menyediakan ruang bisnis yang nyaman, kami juga menawarkan gaya bekerja yang baru untuk masa depan.',
    'land-commercial' => 'Berbekal rekam jejak keberhasilan yang kami miliki dalam pengembangan kawasan perkotaan dan dengan menggunakan kekuatan yang dimiliki oleh jaringan Tokyu Group, kami memberikan jasa perencanaan dan pelayanan dukungan yang menyeluruh untuk fasilitas-fasilitas komersial yang mencakup segala sesuatu mulai dari perencanaan dan pengembangan hingga pengelolaan. Kami berkomitmen untuk menciptakan fasilitas-fasilitas yang selaras dengan kota, berakar di masyarakat, dan akan terus tumbuh dan berkembang bersama-sama dengan wilayah tersebut. Saat ini kami terlibat dalam proyek-proyek yang bertujuan untuk lebih meningkatkan nilai kota Tokyo.',
    'land-homes' => 'Didirikan berdasarkan konsep keselamatan dan keamanan, kami menciptakan rumah yang akan memenuhi impian dan keinginan setiap pelanggan. Dimulai dengan kondominium dan perumahan dengan nama "BRANZ" yang memiliki ruang perumahan yang canggih dan berkualitas tinggi, kami menyediakan rumah untuk masa depan seperti unit-unit kondominium untuk disewakan yang memungkinkan penghuni menikmati gaya hidup perkotaan yang cerdas.',
    'land-overseas' => 'Dalam kurun waktu 40 tahun sejak perusahaan memasuki pasar Indonesia, Tokyu Fudosan telah mengubah pandangannya terhadap dunia dan masa depan. Sejak 2013, kami telah berinvestasi di berbagai jenis real estat mulai dari basisnya di Los Angeles termasuk kantor-kantor kreatif di Los Angeles dan apartemen di Houston. Sementara itu kerjasama bisnis yang dilakukan dengan perusahaan-perusahaan Jepang dan lokal bertujuan untuk terus melanjutkan ekspansi usaha di masa yang akan datang. Perusahaan juga telah meluncurkan proyek-proyek kondominium di Cina, di Qingdao dan Dalian; serta proyek-proyek pembangunan kompleks di Shenyang, Provinsi Liaoning. Tokyu Fudosan akan terus mengantisipasi tren dunia dan gaya hidup untuk jangka waktu 10 atau 20 tahun ke depan dan memperluas ruang lingkup tantangannya di seluruh dunia.',


    'philosophy-top1' => 'Tokyu Land Indonesia didirikan untuk mewarisi teknologi dan semangat yang telah dibangun oleh Jepang sebagaimana kita ketahui saat ini. Misi kami adalah menciptakan gaya hidup baru dengan cara menyatukan berbagai nilai sekaligus melindungi iklim dan budaya Indonesia. Kami berusaha mempersiapkan masa depan Indonesia melalui pengembangan perkotaan<br/><br/>',
    'philosophy-top2' => 'Tujuan utama kami adalah menjadi perusahaan pengembang real estate yang benar-benar unik, jujur ​​dalam membantu Indonesia mencapai tujuan pembangunan. Inovasi tidak akan pernah terwujud dengan kompromi.<br/><br/>',
    'philosophy-top3' => 'Tantangan kami dimulai, dan kami akan mengerahkan segala daya upaya yang kami miliki. Kami akan senantiasa berusaha memenuhi harapan Anda.',
    'philosophy-left' => 'Sejak zaman kuno yang terlihat dari tradisi judo, seni merangkai bunga, upacara minum teh, dan kaligrafi, masyarakat Jepang menyebut keterampilan dan keahlian sebagai suatu "jalan." Masyarakat Jepang bangga menguasai rahasia dari "Jalan" ini. Pengembangan perkotaan hanyalah salah satu dari "Jalan" tersebut. Kami berusaha untuk memberikan nilai tambah pada pembangunan di Indonesia melalui semangat yang tak kenal kompromi yang dipertahankan melalui tradisi dan budaya Jepang.',
    'philosophy-right1' => 'Sejak Tokyu Land Corporation meluncurkan bisnisnya di sini pada tahun 1975, kami telah menjalin ikatan yang dalam dan kuat dengan Indonesia.Sejak Tokyu Land Corporation meluncurkan bisnisnya di sini pada tahun 1975, kami telah menjalin ikatan yang dalam dan kuat dengan Indonesia.',
    'philosophy-right2' => 'Sepanjang sejarah kami di sini, kami berusaha untuk menanamkan akar di Indonesia dengan cara mencintai masyarakat Indonesia, memahami gaya hidup dan budaya Indonesia serta menghargai kekayaan alam Indonesia.',
    'philosophy-right3' => 'Dengan menggabungkan pengetahuan yang kami miliki sebagai sebuah perusahaan Jepang dan akumulasi pengetahuan yang kami peroleh dalam jangka waktu yang panjang serta melalui pengembangan perkotaan dimana hanya kami yang mampu mewujudkannya, kami berusaha untuk memberikan kontribusi terhadap pembangunan di Indonesia.',
    'philosophy-bottom1' => 'Bahkan di Asia yang terus berkembang dengan pesat, Indonesia adalah salah satu negara yang mampu menarik perhatian yang luar biasa.',
    'philosophy-bottom2' => 'Indonesia menerima berbagai jenis pendatang dan berlaku sebagai pusat bisnis dan budaya. Oleh karena itu kami ingin membangun perumahan yang sesuai untuk negara yang akan mendukung masa depan Asia.',
    'philosophy-bottom3' => 'Dari basis kami di Indonesia, kami ingin menawarkan sebuah gaya hunian premium untuk masa depan.',

    'head-director' => 'Mengingat dalam hati tujuan kami untuk memberikan kontribusi bagi pembangunan Indonesia dan bercita-cita tinggi untuk keberlangsungan bisnis.',
    'director1' => 'TOKYU LAND INDONESIA adalah anggota dari Tokyu Fudosan Holdings, perwakilan pengembang real estate yang komprehensif dari Jepang. Hubungan antara Indonesia dan Tokyu Land Corporation dimulai pada tahun 1975 di Bandung. Pada tahun 1981, kami memperluas bisnis kami ke Jakarta. Sampai saat ini, kami sudah membangun 4.500 rumah tinggal sebagai track record bisnis kami.',
    'director2' => 'TOKYU LAND INDONESIA didirikan pada tahun 2012 dengan tujuan memanfaatkan keterampilan dan kepercayaan yang sudah kami bangun lebih dari 40 tahun, serta kekayaan pengalaman kami sebagai perusahaan pengembang yang komprehensif di Jepang dan kekuatan kolektif yang dimiliki oleh Tokyu Group untuk merintis & mengembangkan bisnis baru di Indonesia.Setelah berdiri dengan baik, pada tahun 2015 TOKYU LAND INDONESIA meluncurkan merek kondominium yang telah dikembangkan di Jepang "BRANZ" di Indonesia. Ini adalah pertama kalinya sebuah perusahaan Jepang mengembangkan merek kondominium Jepang di luar negeri, dan mewakili keinginan kuat kami untuk menciptakan akar di Indonesia serta untuk tumbuh dan berkembang bersama di negeri ini. Selain bisnis penjualan kondominium, kami juga bertujuan untuk mengatasi tantangan leasing dan manajemen bisnis ke depan.Ini adalah keinginan kami untuk memberikan sesuatu yang lebih untuk Indonesia dengan menciptakan nilai baru melalui penggabungan kebutuhan pelanggan Indonesia dengan kualitas tinggi yang ditawarkan oleh Jepang.',
    'director3' => 'Noboru Goto, Presiden pertama Tokyu Land Corporation, pernah berkomentar, "Abad ke-21 akan menjadi era Pasifik, dan era ASEAN." Kata-katanya menjadi kenyataan hari ini, kami akan terus mengembangkan bisnis kami dengan tetap berakar pada semangat besar setiap daerah.',

    'history1' => 'Tokyu Land Corporation didirikan pada tahun 1953, dan sampai saat ini, kami tetap meneruskan rekam jejak kesuksesan.  Tokyu Land Corporation telah memprediksi sejak awal kedatangan "era Asia" dan pertama kali memasuki pasar Indonesia pada tahun 1975. Krisis kekacauan sosial yang muncul sebagai akibat dari ketidakstabilan politik tidak menghalangi kami. ',
    'history2' => 'Sebaliknya bisnis terus berjalan dan memberikan kontribusi terhadap pembangunan Indonesia dengan membangun lebih dari 4.500 rumah tinggal untuk dijual. Ke depan, Tokyu Land Corporation akan terus menghadapi tantangan baru bagi masa depan Jepang dan Indonesia.',


    'csr1' => 'Indonesia yang terdiri dari lebih dari 17.500 pulau saat ini sedang mengalami kerusakan lingkungan yang serius. TOKYU LAND INDONESIA terlibat secara aktif dalam inisiatif penanaman pohon bakau di daerah pesisir Jakarta untuk melaksanakan peran dan tanggung jawab kami sebagai warga perusahaan yang menjalankan bisnis di Indonesia serta meningkatkan kesadaran karyawan terhadap isu-isu lingkungan. ',
    'csr2' => 'NATURAL DISASTER MITIGATION <br/> TOKYU LAND INDONESIA CSR',
    'csr3' => 'Indonesia memiliki jumlah hutan mangrove terbesar di dunia. Hutan mangrove baik dan efektif bagi lingkungan dalam berbagai cara, seperti pertahanan pesisir dan melindungi keanekaragaman hayati. Dengan melindungi sumber daya ini, tujuan kami adalah bekerja sama dan bersatu sebagai sebuah perusahaan untuk mewujudkan masyarakat yang berkelanjutan.',
    'csr4' => '<b>5 Desember 2015</b><br/> PT.Tokyu Land Indonesia (“TLID”), sebagai suatu perusahaan pengembang propertimembuktikan komitmennya untuk berkontribusi dalam pelestarian lingkungan alam melalui aktivitas penanaman mangrove yang kedua kalinyadi Taman Wisata Alam –Angke Kapuk, Jakarta Utara. Bersamaan dengan Ulang Tahun ke-3 TLID, ',
    'csr5' => 'kegiatan Corporate Social Responsibility (“CSR”) dengan tema “Grow Mangrove, Grow Life”ini bertujuan untuk memperkuat ikatan di antara karyawan dengan cara bekerja sama melestarikan lingkungan alam Indonesia yang kaya akan keanekaragamanhayati serta mewujudkan masyarakat yang berkesinambungan di masa depan.',
    'csr6' => 'Karyawan TLID beserta keluarga dengan total 71 orang
               berpartisipasi dalam program tahunan ini.
               Program ini dimulai dengan penjelasan mengenai ekosistem mangrove.<br/>',
    'csr7' => '<div style="padding: 60px; padding-top: 48px; height: 267px; font-size:12px; background:#f2f2f2; line-height: 2.5;">
                        lalu bersama-sama memasuki air berlumpur dan menanam mangrove. Kegiatan inimenumbuhkan rasa saling keterikatan dan memberikan pengalaman yang menarik bagi seluruh karyawan. TLID dan seluruh karyawannya berharap agar dapat terus berkontribusi kepada masyarakat Indonesia dan lingkungan alam,baik di dalam maupun di luar Jakarta.
                    </div>',

    'information' => '<div class="information-detail" style="padding: 0;">Terhitung tanggal 20 Juni 2016, kantor pusat (HO) kami akan pindah ke gedung baru.<br/><br/></div>',
    'information-where' => '<span>Lokasi gedung baru:</span><br/>',

    'group-hands' => '<h5>Pencipta gaya hidup yang berwarna</h5><p>TOKYU HANDS INC. menawarkan berbagai macam produk untuk memenuhi beragam kebutuhan, termasuk barang-barang dasar untuk kehidupan sehari-hari pelanggan. Tokyu Hands menganggap semua produk adalah “material” untuk memperkaya kehidupan pelanggan, dan karena itu berupaya untuk menyediakan koleksi “material” yang lengkap. Kelengkapan “material” inilah yang menjadi daya tarik utama TOKYU HANDS. Melalui daya tarik ini, perusahaan bertujuan untuk membawa kegembiraan bagi pelanggan dengan menemukan objek dan ide-ide yang unik hanya di TOKYU HANDS.</p>',
    'group-livable' => '<h5>Memenuhi segala kebutuhan untuk Real-Estate</h5><p>Tokyu Livable menjalankan sebuah bisnis  penyediaan real estat yang menyeluruh, dimana terdapat 4 pilar bisnis yaitu  broker real estat, solusi real  estat, penyewaan real estat, dan  sales contracting. Melalui kerjasama diantara masing-masing bisnis, Tokyu Livable merespon semua kebutuhan dalam bidang real estate, dimulai dari  sektor perumahan  sampai  pendayagunaan dan investasi di bidang real estate. Dengan menyediakan bantuan untuk penyusunan strategi real estate perusahaan-perusahaan, dan merealisasikan kehidupan bertempat tinggal yang kaya untuk pelanggannya.  Perusahaan ini memiliki tujuan untuk berkontribusi  terhadap perkembangan dan keseimbangan masyarakat.</p>',
    'group-comm' => '<h5>Menjaga dan meningkatkan nilai permanen dari real estate</h5><p>Tokyu Community memiliki  rekam jejak kesuksesan dalam mengelola berbagai jenis fasilitas, termasuk kondominium, gedung, dan fasilitas umum. Disamping menyediakan lingkungan yang nyaman untuk hunian dan penghuni, Tokyu juga menyediakan bantuan untuk pengelolaan fondasi bangunan dengan menjaga  nilai aset, sebaik menjaga fungsi fasilitas yang tersedia. Sebagai partner ideal anda untuk menciptakan nilai masa depan yang mendukung gaya hidup dan lingkungan bisnis  aman dan nyaman, kami menawarkan solusi managemen optimal untuk menjaga dan meningkatkan nilai permanen dari  suatu real estate</p>',
    'group-oasis' => '<h5>Mendukung gaya hidup sehat</h5><p>Melalui bisnis nya yang bergerak dalam manajemen klub kebugaran serta memberikan bisnis anak perusahaan Tokyu Fudosan Holdings, Tokyu Sports Oasis memberikan dan menawarkan gaya hidup sehat untuk mereka yang ingin terus menjaga kesehatan dan kenyamanan tubuh serta pikiran. Tokyu Sports Oasis menawarkan berbagai jenis program untuk membantu pelanggannya untuk selalu energik dan dipenuhi stamina, dan serta berkontribusi dalam pemberdayaan masyarakat.</p>',
    'group-stay' => '<h5>Menunjang kenyamanan singgah di Tokyo</h5><p>Kami memiliki tujuan untuk menyuguhkan gaya hidup Tokyo yang nyaman dan aman bagi tamu yang mengunjungiTokyo untuk urusan bisnis atau berwisata. 16 Tokyu Stay Hotel  berlokasi  di area yang nyaman, menyediakan akses yang mudah menuju tempat-tempat penting di sekitar Tokyo. Ruangan yang besar, fasilitas pencucian dan pengering, dapur, serta berbagai fasilitas serta pelayanan lainnya tersedia di hotel kami sebagai fasiilitas menginap jangka panjang dan kontinu, yang menawarkan kenyamanan bagi para tamu yang sedang berwisata dan atau perjalanan bisnis.</p>',
    'group-homes' => '<h5>Dari hunian baru sampai renovasi –usulan gaya hidup mewah</h5><p>Tokyu Homes adalah sebuah perusahaan yang menyediakan hunian komprehensif dan solusi  untuk hunian. Tokyu Homes memiliki kapabilitas dalam mengajukan ide dan menerapkan gaya hidup mewah untuk para pelanggan, termasuk di dalamnya membangun hunian baru, renovasi, dekorasi interior, dan pemeliharaan hunian. Berdasarkan pengalaman dan keterampilan membangun selama bertahun-tahun, kami melebarkan sayap  bisnisnya bukan hanya sekedar menyediakanhunian tetapi juga meliputi renovasi, pemeliharaan, dan dukungan manajemen dalam memelihara fasilitas, serta pemanfaatan aset bisnis</p>',

    'privacy' => '<strong>Pengumpulan dan Penggunaan Informasi Pribadi</strong><br/><br/>

                Informasi pribadi adalah data yang dapat digunakan untuk mengidentifikasi atau menghubungi satu orang.<br/>
                Anda mungkin diminta untuk memberikan informasi pribadi Anda ketika Anda berhubungan dengan PT. Tokyu Land Indonesia atau perusahaan afiliasinya dan Tokyu Fudosan Holding. PT. Tokyu Land Indonesia dan afiliasinya mungkin berbagi informasi pribadi ini satu sama lain dan menggunakannya sesuai dengan Kebijakan Privasi ini. Mereka juga mungkin menggabungkannya dengan informasi lain untuk menyediakan dan meningkatkan produk, layanan, konten, dan iklan. Anda tidak diharuskan untuk memberikan informasi pribadi yang kami minta, tetapi, jika Anda memilih untuk tidak melakukannya, dalam banyak kasus kami tidak akan mampu menyediakan Anda dengan produk atau layanan kami atau menanggapi pertanyaan yang mungkin Anda miliki.<br/><br/>

                Berikut adalah beberapa contoh jenis informasi pribadi yang dapat dikumpulkan oleh PT. Tokyu Land Indonesia dan bagaimana kami dapat menggunakannya.<br/>
                Informasi pribadi apa yang kami kumpulkan<br/><br/>

                <ul>
                    <li>
                        Hubungi kami atau berpartisipasi dalam survei online, kami mungkin mengumpulkan berbagai informasi, termasuk nama, alamat, nomor telepon, alamat email dan preferensi kontak.
                    </li>
                    <li>
                        Ketika Anda berbagi konten Anda dengan keluarga dan teman-teman menggunakan Produk PT. Tokyu Land Indonesia, atau mengundang orang lain untuk berpartisipasi dalam Layanan PT. Tokyu Land Indonesia atau forum, PT. Tokyu Land Indonesia dapat mengumpulkan informasi yang Anda berikan tentang orang-orang seperti nama, alamat, alamat email, dan nomor telepon. PT. Tokyu Land Indonesia akan menggunakan informasi tersebut untuk memenuhi permintaan Anda, menyediakan produk atau layanan yang relevan, atau untuk tujuan anti-penipuan.
                    </li>
                </ul>

                <strong>Bagaimana kami menggunakan informasi pribadi Anda</strong>

                <ul>
                    <li>
                        Informasi pribadi yang kami kumpulkan memungkinkan kami untuk membuat Anda memperoleh info dari PT. Tokyu Land Indonesia mengenai pengumuman produk terbaru dan acara mendatang.
                    </li>
                    <li>
                        Kami juga menggunakan informasi pribadi untuk membantu kami menciptakan, mengembangkan, mengoperasikan, memberikan, dan meningkatkan produk kami, layanan, konten dan iklan, dan untuk tujuan pencegahan kerugian dan anti-penipuan.
                    </li>
                    <li>
                        Kami dapat menggunakan informasi pribadi Anda, termasuk tanggal lahir, untuk memverifikasi identitas, membantu identifikasi pengguna, dan untuk menentukan layanan yang tepat. Sebagai contoh, kami dapat menggunakan tanggal lahir untuk menentukan usia Anda.
                    </li>
                    <li>
                        Dari waktu ke waktu, kami mungkin menggunakan informasi pribadi Anda untuk mengirim pemberitahuan penting, seperti komunikasi tentang pembelian dan perubahan syarat kami, kondisi, dan kebijakan. Karena informasi ini penting untuk interaksi Anda dengan PT. Tokyu Land Indonesia, Anda tidak bisa memilih untuk tidak menerima komunikasi ini.
                    </li>
                    <li>
                        Kami juga dapat menggunakan informasi pribadi untuk tujuan internal seperti audit, analisis data, dan penelitian untuk meningkatkan Produk PT. Tokyu Land Indonesia, layanan, dan komunikasi pelanggan.
                    </li>
                    <li>
                        Jika Anda masuk ke dalam undian, kontes, atau promosi serupa, kami dapat menggunakan informasi yang Anda berikan untuk mengelola program-program tersebut.
                    </li>
                </ul>

                <strong>Pengumpulan dan Penggunaan Informasi Non-Pribadi</strong><br/><br/>

                Kami juga mengumpulkan data dalam bentuk yang tidak memungkinkan hubungan langsung dengan individu tertentu. Kami dapat mengumpulkan, menggunakan, transfer, dan mengungkapkan informasi non-pribadi untuk tujuan apapun. Berikut ini adalah beberapa contoh informasi non-pribadi yang kami kumpulkan dan bagaimana kami dapat menggunakannya:

                <ul>
                    <li>
                        Kami dapat mengumpulkan informasi seperti pekerjaan, bahasa, kode pos, kode area, pengenal perangkat unik, URL pengarah, lokasi, dan zona waktu di mana Produk PT. Tokyu Land Indonesia digunakan agar kami dapat lebih memahami perilaku pelanggan dan meningkatkan produk kami, layanan, dan iklan.
                    </li>
                    <li>
                        Kami dapat mengumpulkan informasi tentang kegiatan pelanggan di website kami, dan dari produk dan layanan kami yang lain. Informasi ini dikumpulkan dan digunakan untuk membantu kami memberikan informasi yang lebih berguna untuk pelanggan kami dan untuk memahami bagian dari website kami, produk, dan jasa yang paling menarik.
                    </li>
                    <li>
                        Kami dapat mengumpulkan dan menyimpan rincian tentang bagaimana Anda menggunakan layanan kami, termasuk permintaan pencarian. Informasi ini dapat digunakan untuk meningkatkan relevansi hasil yang disediakan oleh layanan kami. Kecuali dalam kasus yang terbatas untuk memastikan kualitas layanan kami melalui Internet, informasi tersebut tidak akan terkait dengan alamat IP Anda.
                    </li>
                </ul>

                Jika kami melakukan menggabungkan informasi non-pribadi dengan informasi pribadi, maka informasi gabungan akan diperlakukan sebagai informasi pribadi selama itu tetap digabungkan.<br/><br/>

                <strong>Cookie dan Teknologi Lain</strong><br/><br/>

                Situs PT. Tokyu Land Indonesia, layanan online, aplikasi interaktif, pesan email, dan iklan dapat menggunakan "cookies" dan teknologi lain seperti tag pixel dan web beacon. Teknologi ini membantu kami lebih memahami perilaku pengguna, memberitahu kami bagian mana dari situs web kami yang telah dikunjungi, dan memfasilitasi dan mengukur efektivitas iklan dan pencarian web. Kami memperlakukan informasi yang dikumpulkan oleh cookie dan teknologi lainnya sebagai informasi non-pribadi. Namun, sejauh alamat Internet Protocol (IP) atau pengenal serupa dianggap informasi pribadi oleh hukum setempat, kami juga memperlakukan pengidentifikasi ini sebagai informasi pribadi. Demikian pula, sejauh bahwa informasi non-pribadi digabungkan dengan informasi pribadi, kami memperlakukan informasi gabungan sebagai informasi pribadi untuk tujuan Kebijakan Privasi ini.<br/><br/>

                PT. Tokyu Land Indonesia dan mitra-mitranya menggunakan cookie dan teknologi lainnya dalam layanan iklan mobile untuk mengontrol berapa kali Anda melihat iklan tertentu, menayangkan iklan yang berhubungan dengan minat Anda, dan mengukur efektivitas kampanye iklan.<br/><br/>

                Seperti halnya sebagian besar layanan internet, kami mengumpulkan beberapa informasi secara otomatis dan menyimpannya dalam file log. Informasi ini mencakup alamat Internet Protocol (IP), jenis browser dan bahasa, penyedia layanan Internet (ISP), mengacu dan website keluar dan aplikasi, sistem operasi, cap tanggal / waktu, dan data clickstream.<br/><br/>

                Kami menggunakan informasi ini untuk memahami dan menganalisis kecenderungan, untuk mengelola situs, untuk belajar tentang perilaku pengguna di situs, untuk meningkatkan produk dan layanan kami, dan untuk mengumpulkan informasi demografis tentang basis pengguna kami secara keseluruhan. PT. Tokyu Land Indonesia dapat menggunakan informasi ini dalam layanan pemasaran dan iklan kami.<br/><br/>

                Dalam beberapa pesan email kami, kami menggunakan "klik-melalui URL" terkait dengan konten pada PT. Situs Tokyu Land Indonesia. Ketika pelanggan menekan klik salah satu URL ini, mereka melewati server web terpisah sebelum tiba di halaman tujuan di website kami. Kami melacak klik-melalui data ini untuk membantu kami menentukan minat pada topik tertentu dan mengukur efektivitas komunikasi pelanggan kami. Jika Anda memilih untuk tidak dilacak dengan cara ini, Anda tidak harus mengklik teks atau link grafis dalam pesan email.<br/><br/>

                Tag Pixel memungkinkan kami untuk mengirim pesan email dalam format dimana pelanggan dapat membaca, dan mereka memberitahu kami apakah email telah dibuka. Kami dapat menggunakan informasi ini untuk mengurangi atau menghilangkan pesan yang dikirim ke pelanggan.<br/><br/>




                <strong>Pengungkapan kepada Pihak Ketiga</strong><br/><br/>

                Setiap saat PT. Tokyu Land Indonesia dapat menyediakan informasi pribadi tertentu kepada mitra strategis yang bekerja sama dengan PT. Tokyu Land Indonesia untuk menyediakan produk dan layanan, atau yang membantu pemasaran PT. Tokyu Land Indonesia kepada pelanggan. Informasi pribadi hanya akan dibagikan oleh PT. Tokyu Land Indonesia untuk memberikan atau meningkatkan produk kami, layanan dan iklan; tidak akan dibagi dengan pihak ketiga untuk tujuan pemasaran mereka.<br/><br/><br/>




                <strong>Penyedia Jasa</strong><br/><br/>

                PT. Tokyu Land Indonesia berbagi informasi pribadi dengan perusahaan yang menyediakan layanan seperti pengolahan informasi, memperluas kredit, memenuhi pesanan pelanggan, memberikan produk kepada Anda, mengelola dan meningkatkan data pelanggan, menyediakan layanan pelanggan, menilai minat Anda pada produk dan layanan kami, dan melakukan pelanggan penelitian atau survei kepuasan. Kami memastikan bahwa Perusahaan-perusahaan ini wajib melindungi informasi Anda.<br/><br/><br/>




                <strong>Lainnya</strong><br/><br/>

                Sepanjang diperlukan - oleh hukum, proses hukum, litigasi, dan / atau permintaan dari otoritas publik dan pemerintah - untuk PT. Tokyu Land Indonesia untuk mengungkapkan informasi pribadi Anda. Kami juga dapat mengungkapkan informasi tentang Anda jika kami menetapkan bahwa untuk tujuan keamanan nasional, penegakan hukum, atau masalah lain kepentingan publik, pengungkapan diperlukan atau tepat.<br/><br/>

                Kami juga dapat mengungkapkan informasi tentang Anda jika kami menentukan bahwa pengungkapan secara wajar diperlukan untuk menegakkan syarat dan ketentuan kami atau melindungi operasi atau pengguna kami. Selain itu, dalam hal reorganisasi, merger, atau penjualan kami dapat mentransfer setiap dan semua informasi pribadi yang kami kumpulkan ke pihak ketiga terkait.<br/><br/><br/>




                <strong>Perlindungan Informasi Pribadi</strong><br/><br/>

                PT. Tokyu Land Indonesia mengamankan informasi pribadi Anda dengan sangat serius. Layanan online PT. Tokyu Land Indonesia seperti http://www.tokyuland-id.com/ melindungi informasi pribadi Anda selama transit menggunakan enkripsi. Ketika data pribadi Anda disimpan oleh PT. Tokyu Land Indonesia, kami menggunakan sistem komputer dengan akses terbatas bertempat di fasilitas yang menggunakan langkah-langkah keamanan fisik.<br/><br/>

                Ketika Anda menggunakan beberapa Produk PT. Tokyu Land Indonesia, layanan, atau layanan jejaring sosial, maka informasi pribadi dan konten Anda yang Anda bagikan akan terlihat oleh pengguna lain dan dapat dibaca, dikumpulkan, atau digunakan oleh mereka. Anda bertanggung jawab untuk informasi pribadi yang Anda bagikan atau kirimkan dalam hal ini. Misalnya, jika Anda daftar nama dan alamat email di forum, maka informasi tersebut menjadi milik umum. Harap berhati-hati saat menggunakan fitur ini.<br/><br/><br/>




                <strong>Integritas dan Retensi Informasi Pribadi</strong><br/><br/>

                PT. Tokyu Land Indonesia membuatnya mudah bagi Anda untuk menjaga informasi pribadi Anda akurat, lengkap, dan up to date. Kami akan menyimpan informasi pribadi Anda untuk periode yang diperlukan untuk memenuhi tujuan yang diuraikan dalam Kebijakan Privasi ini kecuali waktu retensi lebih lama diperlukan atau diizinkan oleh hukum.<br/><br/><br/>




                <strong>Layanan Berbasis Lokasi</strong><br/><br/>

                Untuk menyediakan layanan berbasis lokasi, PT. Tokyu Land Indonesia dan mitra dan pemegang lisensi kami dapat mengumpulkan, menggunakan, dan berbagi data lokasi yang tepat, termasuk real-time lokasi geografis dari komputer atau perangkat Anda. Bila tersedia, layanan berbasis lokasi dapat menggunakan GPS, Bluetooth, dan Alamat IP Anda, bersama dengan sumber hotspot Wi-Fi dan lokasi menara seluler, dan teknologi lainnya untuk menentukan perkiraan lokasi perangkat Anda. Kecuali Anda memberikan persetujuan, data lokasi ini dikumpulkan secara anonim dalam bentuk yang tidak mengidentifikasi Anda secara pribadi dan digunakan oleh PT. Tokyu Land Indonesia dan mitra dan pemegang lisensi untuk menyediakan dan meningkatkan produk dan layanan berbasis lokasi. Sebagai contoh, perangkat Anda dapat berbagi lokasi geografis dengan penyedia aplikasi ketika Anda memilih untuk layanan lokasi mereka.<br/><br/><br/>




                <strong>Situs dan Layanan Pihak Ketiga</strong><br/><br/>

                PT. Tokyu Land Indonesia website, produk, aplikasi, dan layanan dapat berisi link ke situs web pihak ketiga, produk, dan jasa. Kami mendorong Anda untuk belajar tentang praktik privasi dari pihak-pihak ketiga.<br/><br/><br/>




                <strong>Komitmen Seluruh Perusahaan kami untuk Privasi Anda</strong><br/><br/>

                Untuk memastikan informasi pribadi Anda aman, kami telah mengkomunikasikan pedoman privasi dan keamanan kami kepada Karyawan PT. Tokyu Land Indonesia dan secara ketat menegakkan perlindungan privasi dalam perusahaan.<br/><br/><br/>




                <strong>Pertanyaan privasi</strong><br/><br/>

                Jika Anda memiliki pertanyaan atau masalah tentang Kebijakan privasi PT. Tokyu Land Indonesia atau pengolahan data atau jika Anda ingin mengajukan keluhan tentang pelanggaran undang-undang privasi lokal, silahkan hubungi kami. Anda selalu dapat menghubungi kami melalui nomor telepon dukungan pelanggan PT. Tokyu Land Indonesia yang relevan.<br/><br/>

                Semua komunikasi tersebut diperiksa dan tanggapan yang sesuai akan diberikan sesegera mungkin. Jika Anda tidak puas dengan jawaban yang diterima, Anda bisa merujuk keluhan Anda kepada regulator yang relevan di wilayah hukum Anda. Jika Anda bertanya kepada kami, kami akan berusaha untuk memberikan informasi tentang jalan keluar yang relevan yang mungkin berlaku untuk situasi Anda.<br/><br/>

                PT. Tokyu Land Indonesia berhak memperbarui Kebijakan Privasi dari waktu ke waktu. Ketika kami mengubah kebijakan ini secara material, maka akan diberitahukan di website kami bersama dengan Kebijakan Privasi yang telah diperbarui.<br/><br/>',

    'terms' => '

                Situs Web http://www.tokyuland-id.com/ ("Situs") adalah layanan informasi online yang disediakan oleh PT.Tokyu Land Indonesia ("PT. TLID"), tunduk pada kepatuhan Anda dengan syarat dan ketentuan yang tercantum di bawah ini. Bacalah dokumen ini dengan seksama sebelum mengakses atau menggunakan situs. Dengan mengakses atau menggunakan situs, anda setuju untuk terikat oleh syarat dan ketentuan di bawah ini. Jika anda tidak ingin terikat oleh syarat dan ketentuan ini, anda tidak dapat mengakses atau menggunakan situs. PT. TLID dapat mengubah perjanjian ini setiap saat, dan perubahan tersebut akan efektif segera setelah posting dari perjanjian diubah pada situs. Anda setuju untuk meninjau perjanjian secara berkala untuk memperhatikan perubahan tersebut dan akses lanjutan atau penggunaan situs akan dianggap penerimaan konklusif anda terhadap perjanjian modifikasi.<br/><br/><br/>


                <p style="padding-left: 3em;">

                <strong>Hak Cipta, Lisensi dan Kiriman Ide.</strong><br/><br/>

                Seluruh isi dari Situs dilindungi oleh hak cipta dan merek dagang hukum internasional. Pemilik hak cipta dan merek dagang adalah PT. TLID, afiliasinya atau pemberi lisensi pihak ketiga lainnya. Anda tidak diperkenankan mengubah, menyalin, mereproduksi, menerbitkan, mengunggah, meletakkan, mengirimkan, atau mendistribusikan, dalam cara apapun, material pada situs, termasuk tulisan, gambar, kode dan / atau perangkat lunak.  Anda dapat mencetak dan mengunduh bagian dari berbagai wilayah Situs semata-mata untuk tujuan non-komersial Anda sendiri asalkan Anda setuju untuk tidak mengubah atau menghapus setiap hak cipta atau kepemilikan dari bahan. Anda setuju untuk memberikan kepada PT. TLID hak non-eksklusif, bebas royalti, di seluruh dunia, lisensi tak terbatas, dengan hak untuk sub-lisensi, untuk mereproduksi, mendistribusikan, mentransmisikan, membuat karya turunan dari, menampilkan dan melakukan apapun bahan dan informasi lain kepada publik (termasuk, namun tidak terbatas, ide-ide yang terkandung di dalamnya untuk produk dan layanan baru atau yang ditingkatkan) Anda masukkan untuk setiap wilayah publik Situs (seperti papan buletin, forum dan newsgroup) atau melalui e -mail ke Situs dengan segala cara dan di media sekarang dikenal atau selanjutnya dikembangkan. Anda juga memberikan kepada PT. TLID hak untuk menggunakan nama Anda sehubungan dengan materi dan informasi lainnya serta sehubungan dengan semua periklanan, pemasaran dan bahan promosi yang berkaitan dengannya. Anda setuju bahwa Anda tidak akan mengajukan gugatan terhadap PT. TLID untuk setiap dugaan pelanggaran aktual atau penyalahgunaan hak milik apapun dalam komunikasi Anda kepada PT. TLID.</p>

                <p style="padding-left: 1em;">
                    <span style="font-size: 14px; text-transform: uppercase;">MEREK DAGANG.</span><br/><br/>
                    Publikasi, produk, konten atau jasa yang disebut di sini atau di Situs adalah merek dagang eksklusif atau merek layanan dari PT. TLID. Produk dan nama perusahaan lain yang disebutkan di Situs mungkin merupakan merek dagang dari pemiliknya masing-masing.<br/><br/>
                </p>

                <p style="padding-left: 3em;">

                    <strong>Penggunaan Situs.</strong><br/><br/>

                    Anda memahami bahwa, kecuali untuk informasi, produk atau layanan dengan jelas diidentifikasi sebagai disediakan oleh PT. TLID, Situs tidak beroperasi, kontrol atau mendukung informasi, produk atau jasa di internet dengan cara apapun. Kecuali untuk informasi, produk atau jasa Situs diidentifikasi, semua informasi, produk dan jasa yang ditawarkan melalui Situs atau di Internet umumnya ditawarkan oleh pihak ketiga, yang tidak berafiliasi dengan PT. TLID. Anda juga memahami bahwa PT. TLID tidak dapat dan tidak menyatakan atau menjamin bahwa file yang tersedia untuk download melalui Situs akan bebas dari infeksi atau virus, worm, trojan horse atau kode lain yang nyata mengkontaminasi atau bersifat destruktif. Anda bertanggung jawab untuk menerapkan prosedur yang cukup dan pemeriksaan untuk memenuhi persyaratan tertentu untuk akurasi data input dan output, dan untuk menjaga sarana eksternal ke Situs untuk rekonstruksi data yang hilang.<br/><br/>

                    Anda bertanggung jawab terhadap semua risiko untuk penggunaan situs dan internet. PT. TLID menyediakan situs dan informasi terkait "sebagaimana adanya" dan tidak membuat pernyataan atau jaminan tersirat atau tersurat, pernyataan atau dukungan apapun (termasuk tanpa batasan jaminan kepemilikan atau non pelanggaran, atau jaminan diperdagangkan atau kesesuaian untuk tujuan tertentu) sehubungan dengan layanan, informasi barang atau layanan diberikan melalui layanan atau internet umum,                      dan pt. TLID tidak bertanggung jawab atas biaya atau kerusakan yang timbul baik langsung maupun tidak langsung dari transaksi tersebut. Merupakan tanggung jawab anda untuk evaluasi akurasi, kelengkapan dan kegunaan dari semua pendapat, saran, jasa, barang dan informasi lainnya diberikan melalui layanan atau internet secara umum. PT. TLID tidak menjamin bahwa layanan akan terganggu cacat atau bebas kesalahan atau bahwa cacat dalam layanan akan diperbaiki.<br/><br/>

                    Anda memahami lebih lanjut bahwa sifat murni dari internet mengandung bahan tanpa edit atau mungkin ofensif untuk anda. Akses anda untuk bahan tersebut menjadi risiko anda. PT. TLID tidak memiliki kontrol  dan menerima tanggung jawab apapun untuk bahan tersebut.<br/><br/>

                </p>
                <p style="padding-left: 1em;">
                    <span style="font-size: 14px; text-transform: uppercase;">PEMBATASAN TANGGUNG JAWAB</span><br/><br/>
                    PT. TLID tidak bertanggung jawab atas (i), kerusakan insidental, akibat, atau tidak langsung (termasuk, tapi tidak terbatas pada, kerugian akibat hilangnya laba, gangguan bisnis, hilangnya program atau informasi, dan sejenisnya) yang timbul dari penggunaan atau ketidakmampuan untuk menggunakan layanan, atau informasi apapun, atau transaksi diberikan pada layanan, atau download dari layanan, atau penundaan dari informasi atau layanan. Meskipun pt. TLID atau perwakilan perusahaan resmi telah diberitahu tentang kemungkinan kerusakan tersebut, atau (ii) klaim disebabkan kesalahan, kelalaian atau ketidakakuratan lainnya di layanan dan / atau bahan informasi atau download melalui layanan. Karena beberapa negara tidak mengizinkan pengecualian atau pembatasan tanggung jawab atas kerusakan akibat atau insidental, batasan di atas mungkin tidak berlaku untuk anda. Dalam hal ini kewajiban pt. TLID  adalah terbatas sejauh diizinkan oleh hukum.<br/><br/>

                    PT. TLID tidak membuat pernyataan apapun tentang situs web lain yang mungkin anda akses melalui Situs ini atau yang dapat membuat link ke Situs ini. Ketika Anda mengakses situs web lain, harap dipahami bahwa itu adalah independen dari Situs ini, dan bahwa PT. TLID tidak memiliki kontrol atas konten di situs web tersebut. Selain itu, link ke situs web PT. TLID tidak berarti bahwa PT. TLID mendukung atau menerima tanggung jawab untuk konten, atau penggunaan, situs web tersebut.<br/><br/>
                </p>

                <p style="padding-left: 3em;">
                    <strong>Ganti Rugi.</strong><br/><br/>

                    Anda setuju untuk mengganti kerugian, membela dan melepaskan PT. TLID termasuk, pejabat, direktur, karyawan, agen, pemberi lisensi, pemasok dan setiap pihak ketiga penyedia informasi ke Situs dari dan terhadap semua kerugian, ongkos , kerusakan dan biaya, termasuk biaya pengacara yang wajar, akibat dari setiap pelanggaran terhadap Perjanjian ini (termasuk tindakan kelalaian atau kesalahan) oleh Anda atau orang lain mengakses Situs.<br/><br/><br/>

                    <strong>Hak Pihak Ketiga.</strong><br/><br/>

                    Ketentuan-ketentuan ayat 2 (Penggunaan Layanan), dan 3 (Ganti Rugi) adalah untuk kepentingan PT. TLID dan pejabat, direktur, karyawan, agen, pemberi lisensi, pemasok, dan setiap pihak ketiga penyedia informasi ke Situs. Masing-masing individu atau entitas memiliki hak untuk menyatakan dan menegakkan ketentuan-ketentuan secara langsung terhadap Anda atas nama sendiri.<br/><br/><br/>

                    <strong>Jangka waktu.</strong><br/><br/>

                    Perjanjian ini berlaku efektif sepanjang Anda masih mengakses dan menggunakan Situs.<br/><br/><br/>

                    <strong>Lain-Lain.</strong><br/><br/>

                    Perjanjian ini harus diatur dan ditafsirkan sesuai dengan hukum Indonesia yang berlaku dan akan dilakukan di Indonesia. Anda setuju bahwa setiap tindakan hukum atau persidangan antara PT. TLID dan Anda untuk tujuan apapun mengenai Perjanjian ini atau kewajiban para pihak berdasarkan Perjanjian ini akan diselesaikan secara eksklusif di pengadilan negeri yang berwenang di Indonesia.<br/><br/><br/>
                </p>',

);